import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { RefrigeratedPayment } from 'src/app/models/refrigeratedpayment';
import { Refrigerator } from 'src/app/models/refrigerator';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RefrigeratorPaymentService } from 'src/app/services/refrigerator-payment.service';
import { RefrigeratorService } from 'src/app/services/refrigerator.service';

@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.component.html',
  styleUrls: ['./add-payment.component.css']
})
export class AddPaymentComponent implements OnInit {

  paymentForm: FormGroup = new FormGroup({
    'idRefrigerator': new FormControl(''),
    'idPlantation': new FormControl(''),
    'kindOfRaspberry': new FormControl(''),
    'date' : new FormControl(''),
    'price': new FormControl('')
  });
  refrigerators: Refrigerator[] = [];
  plantations: RaspberryPlantation[] = [];
  idGrower: number;

  constructor(
    private refrigeratorService: RefrigeratorService,
    private plantationService: RaspberryPlantationService,
    private router: Router,
    private paymentService: RefrigeratorPaymentService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.refrigeratorService.findAll().subscribe(
      data => {
        this.refrigerators = data;
      }
    );
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onAdd();
    }
  }

  onAdd(){
    this.plantationService.getPlantation(this.paymentForm.value['idPlantation']).subscribe(
      plantation => {
        this.refrigeratorService.getById( this.paymentForm.value['idRefrigerator']).subscribe(
          refrigerator => {
            const payment = new RefrigeratedPayment(
              this.paymentForm.value['kindOfRaspberry'],
              new Date(this.paymentForm.value['date']),
              this.paymentForm.value['price'],
              plantation,
              refrigerator
              );
            this.paymentService.addPayment(payment).subscribe(
              data => {
                this.router.navigate(['payment']);
              }
            )
          }
        )
      }
    )
  }

}
