import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { RefrigeratedPayment } from 'src/app/models/refrigeratedpayment';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RefrigeratorPaymentService } from 'src/app/services/refrigerator-payment.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  paymentForm: FormGroup = new FormGroup({
    'year': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  paymentShow = false;
  payments: RefrigeratedPayment[] = [];
  plantations: RaspberryPlantation[] = [];
  idGrower: number;

  constructor(
    private paymentService: RefrigeratorPaymentService,
    private authService: AuthService,
    private plantationService: RaspberryPlantationService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  onFind() {
    this.paymentService.findPayments(this.paymentForm.value['year'], this.paymentForm.value['idPlantation']).subscribe(
      data => {
        this.payments = data;
        this.paymentShow = true;
      }, error => {
        if (error.status === 404) {
          window.alert("No payments in year which you input!");
          this.paymentShow = false;
          this.paymentForm = new FormGroup({
            'year': new FormControl(''),
            'idPlantation': new FormControl('')
          });
        }
      }
    )
  }

  onClose() {
    this.paymentShow = false;
    this.paymentForm = new FormGroup({
      'year': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }
}
