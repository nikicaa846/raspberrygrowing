import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { RefrigeratedCrates } from 'src/app/models/refrigeratedcrates';
import { RefrigeratorCratesService } from 'src/app/services/refrigerator-crates.service';
import { RefrigeratorRemovalService } from 'src/app/services/refrigerator-removal.service';

@Component({
  selector: 'app-add-crates',
  templateUrl: './add-crates.component.html',
  styleUrls: ['./add-crates.component.css']
})
export class AddCratesComponent implements OnInit {

  cratesForm: FormGroup = new FormGroup({
    'submittedcrates': new FormControl(''),
    'takencrates': new FormControl('')
  });
  
  constructor(
    private cratesService: RefrigeratorCratesService,
    private removalService: RefrigeratorRemovalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onAdd();
    }
  }
  

  onAdd() {
    const param = this.route.snapshot.queryParamMap;
    const kind = param.get('kind');
    const date = param.get('date');
    const idRefrigerator = +param.get('idRefrigerator');
    const idPlantation = +param.get('idPlantation');
    this.removalService.findByKindOfRaspberryAndDate( idPlantation, kind, date  ).subscribe(
      value => {
        const removal = value.find( el => idRefrigerator === el.refrigerator.id);
          const crates = new RefrigeratedCrates(
            this.cratesForm.value['takencrates'],
            this.cratesForm.value['submittedcrates'],
            removal
          );
          this.cratesService.addCrates(crates).subscribe(
            data => {
              this.router.navigate(['/removal']);
            }
          )
      }
    );
  }
}
