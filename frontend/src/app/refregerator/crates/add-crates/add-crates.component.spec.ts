import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCratesComponent } from './add-crates.component';

describe('AddCratesComponent', () => {
  let component: AddCratesComponent;
  let fixture: ComponentFixture<AddCratesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCratesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCratesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
