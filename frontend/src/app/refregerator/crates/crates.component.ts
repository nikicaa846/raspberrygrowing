import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { RefrigeratedCrates } from 'src/app/models/refrigeratedcrates';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RefrigeratorCratesService } from 'src/app/services/refrigerator-crates.service';

@Component({
  selector: 'app-crates',
  templateUrl: './crates.component.html',
  styleUrls: ['./crates.component.css']
})
export class CratesComponent implements OnInit {

  cratesForm: FormGroup = new FormGroup({
    'date' : new FormControl(''),
    'idPlantation' : new FormControl('')
  });
  showCrates = false;
  crates: RefrigeratedCrates [] = [];
  plantations: RaspberryPlantation[] = [];
  idGrower: number;

  constructor(
    private cratesService: RefrigeratorCratesService,
    private authService: AuthService,
    private plantationService: RaspberryPlantationService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onFind();
    }
  }

  onFind(){
    this.cratesService.findCrates(this.cratesForm.value['date'], this.cratesForm.value['idPlantation']).subscribe(
      data => {
        this.crates = data;
        this.showCrates = true;
      }, error =>{
        if ( error.status === 404 ){
          window.alert("No removal for date which you input!");
           this.showCrates = false;
           this.cratesForm = new FormGroup({
            'date' : new FormControl(''),
            'idPlantation': new FormControl('')
          });
        }
      }
    )
  }

  onClose(){
    this.showCrates = false;
    this.cratesForm = new FormGroup({
      'date' : new FormControl(''),
      'idPlantation' : new FormControl('')
    });
  }
}
