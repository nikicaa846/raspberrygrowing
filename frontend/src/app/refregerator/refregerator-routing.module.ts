import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGaurdService } from '../services/authguard.service';
import { AddRefregeratorComponent } from './add-refregerator/add-refregerator.component';
import { AddCratesComponent } from './crates/add-crates/add-crates.component';
import { CratesComponent } from './crates/crates.component';
import { InformationComponent } from './information/information.component';
import { AddPaymentComponent } from './payment/add-payment/add-payment.component';
import { PaymentComponent } from './payment/payment.component';
import { RefregeratorComponent } from './refregerator.component';
import { AddRemovalComponent } from './removal/add-removal/add-removal.component';
import { RemovalComponent } from './removal/removal.component';
import { UpdateRefregeratorComponent } from './update-refregerator/update-refregerator.component';


const routes: Routes = [
    { path: '', component: RefregeratorComponent, canActivate:[AuthGaurdService], children:[
        {path: 'refrigerator', component: InformationComponent},
        {path: 'addrefrigerator', component: AddRefregeratorComponent},
        {path: 'updaterefrigerator/:id', component: UpdateRefregeratorComponent},
        {path: 'crates', component: CratesComponent},
        {path: 'addcrates', component: AddCratesComponent},
        {path: 'payment', component: PaymentComponent},
        {path: 'addpayment', component: AddPaymentComponent},
        {path: 'removal', component: RemovalComponent},
        {path: 'addremoval', component: AddRemovalComponent}
      ]}
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RefregeratorRoutingModule{}