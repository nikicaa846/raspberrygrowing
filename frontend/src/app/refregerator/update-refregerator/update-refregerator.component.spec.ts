import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRefregeratorComponent } from './update-refregerator.component';

describe('UpdateRefregeratorComponent', () => {
  let component: UpdateRefregeratorComponent;
  let fixture: ComponentFixture<UpdateRefregeratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateRefregeratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRefregeratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
