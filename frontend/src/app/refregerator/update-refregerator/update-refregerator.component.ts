import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { RefrigeratorService } from 'src/app/services/refrigerator.service';

@Component({
  selector: 'app-update-refregerator',
  templateUrl: './update-refregerator.component.html',
  styleUrls: ['./update-refregerator.component.css']
})
export class UpdateRefregeratorComponent implements OnInit {

  refrigeratorForm: FormGroup = new FormGroup({
    'name': new FormControl(''),
    'address': new FormControl(''),
    'phone': new FormControl('')
  });
  id: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private refrigeratorService: RefrigeratorService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.refrigeratorService.getById(this.id).subscribe(
      data => {
        this.refrigeratorForm = new FormGroup({
          'name': new FormControl(data.name),
          'address': new FormControl(data.address),
          'phone': new FormControl(data.phone)
        });
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onUpdate();
    }
  }

  onUpdate(){
    this.refrigeratorService.updateRefrigerator(this.id, this.refrigeratorForm.value).subscribe(
      data => {
        this.router.navigate(['/refrigerator']);
      }
    )
  }
}
