import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefregeratorComponent } from './refregerator.component';

describe('RefregeratorComponent', () => {
  let component: RefregeratorComponent;
  let fixture: ComponentFixture<RefregeratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefregeratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefregeratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
