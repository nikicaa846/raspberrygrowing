import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RefrigeratorService } from 'src/app/services/refrigerator.service';

@Component({
  selector: 'app-add-refregerator',
  templateUrl: './add-refregerator.component.html',
  styleUrls: ['./add-refregerator.component.css']
})
export class AddRefregeratorComponent implements OnInit {

  refrigeratorForm: FormGroup = new FormGroup({
    'name': new FormControl(''),
    'address': new FormControl(''),
    'phone': new FormControl('')
  });

  constructor(
    private refrigeratorService: RefrigeratorService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.refrigeratorService.addRefrigerator(this.refrigeratorForm.value).subscribe(
      data => {
        this.router.navigate(['/refrigerator']);
      }
    )
  }

}
