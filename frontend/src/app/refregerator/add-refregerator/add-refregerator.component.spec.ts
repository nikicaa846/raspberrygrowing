import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRefregeratorComponent } from './add-refregerator.component';

describe('AddRefregeratorComponent', () => {
  let component: AddRefregeratorComponent;
  let fixture: ComponentFixture<AddRefregeratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddRefregeratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRefregeratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
