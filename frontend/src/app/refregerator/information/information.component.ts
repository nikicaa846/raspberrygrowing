import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Refrigerator } from 'src/app/models/refrigerator';
import { AuthService } from 'src/app/services/auth.service';
import { RefrigeratorService } from 'src/app/services/refrigerator.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

  isAdmin: boolean;
  refrigerators: Refrigerator[] = [];

  constructor( 
    private authService: AuthService, 
    private refrigeratorService: RefrigeratorService,
    private router: Router 
    ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.refrigeratorService.findAll().subscribe(
      data => {
        this.refrigerators = data;
      }
    );
  }

  onUpdate(id: number){
    this.router.navigate(['/updaterefrigerator', id]);
  }

  onDelete(id: number){
    this.refrigeratorService.deleteRefrigerator(id).subscribe(
      data => {
        window.location.reload();
      }
    )
  }
}
