import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { RefrigeratedRemoval } from 'src/app/models/refrigeratedremoval';
import { Refrigerator } from 'src/app/models/refrigerator';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RefrigeratorRemovalService } from 'src/app/services/refrigerator-removal.service';
import { RefrigeratorService } from 'src/app/services/refrigerator.service';

@Component({
  selector: 'app-add-removal',
  templateUrl: './add-removal.component.html',
  styleUrls: ['./add-removal.component.css']
})
export class AddRemovalComponent implements OnInit {

  removalForm: FormGroup = new FormGroup({
    'kindOfRaspberry': new FormControl(''),
    'date': new FormControl(''),
    'weight': new FormControl(''),
    'idRefrigerator': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  refrigerators: Refrigerator[] = [];
  plantations: RaspberryPlantation[] = [];
  idGrower: number;

  constructor(
    private plantationService: RaspberryPlantationService,
    private refregeratorService: RefrigeratorService,
    private removalService: RefrigeratorRemovalService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    );
    this.refregeratorService.findAll().subscribe(
      data => {
        this.refrigerators = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.refregeratorService.getById(this.removalForm.value['idRefrigerator']).subscribe(
      refrigerator => {
        this.plantationService.getPlantation(this.removalForm.value['idPlantation']).subscribe(
          plantation => {
            const removal = new RefrigeratedRemoval(
              this.removalForm.value['kindOfRaspberry'],
              this.removalForm.value['weight'],
              new Date(this.removalForm.value['date']),
              plantation,
              refrigerator
            );
            this.removalService.addRemoval(removal).subscribe(
              data => {
                this.router.navigate(['/addcrates'],
                  {
                    queryParams: {
                      kind: this.removalForm.value['kindOfRaspberry'],
                      date: this.removalForm.value['date'],
                      idRefrigerator: this.removalForm.value['idRefrigerator'],
                      idPlantation: this.removalForm.value['idPlantation']
                    }
                  });
              }
            );
          }
        )
      }
    );
  }
}
