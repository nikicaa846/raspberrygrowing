import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRemovalComponent } from './add-removal.component';

describe('AddRemovalComponent', () => {
  let component: AddRemovalComponent;
  let fixture: ComponentFixture<AddRemovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddRemovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRemovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
