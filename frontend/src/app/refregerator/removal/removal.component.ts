import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { RefrigeratedRemoval } from 'src/app/models/refrigeratedremoval';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RefrigeratorRemovalService } from 'src/app/services/refrigerator-removal.service';

@Component({
  selector: 'app-removal',
  templateUrl: './removal.component.html',
  styleUrls: ['./removal.component.css']
})
export class RemovalComponent implements OnInit {

  removalForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'kindOfRaspberry': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  showRemoval = false;
  removals: RefrigeratedRemoval[] = [];
  plantations: RaspberryPlantation[] = [];
  idGrower: number;

  constructor(
    private removalService: RefrigeratorRemovalService,
    private authService: AuthService,
    private plantationService: RaspberryPlantationService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  onFind() {
    this.removalService.findByKindOfRaspberryAndDate(this.removalForm.value['idPlantation'], this.removalForm.value['kindOfRaspberry'], this.removalForm.value['date']).subscribe(
      value => {
        this.removals = value;
        this.showRemoval = true;
      }, error => {
        if (error.status === 404) {
          window.alert("No removal for date which you input!");
          this.showRemoval = false;
          this.removalForm = new FormGroup({
            'date': new FormControl(''),
            'kindOfRaspberry': new FormControl(''),
            'idPlantation': new FormControl('')
          });
        }
      }
    )
  }

  onClose() {
    this.showRemoval = false;
    this.removalForm = new FormGroup({
      'date': new FormControl(''),
      'kindOfRaspberry': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }
}
