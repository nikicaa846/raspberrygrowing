import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RefregeratorComponent } from './refregerator.component';
import { CratesComponent } from './crates/crates.component';
import { RemovalComponent } from './removal/removal.component';
import { PaymentComponent } from './payment/payment.component';
import { InformationComponent } from './information/information.component';
import { RefregeratorRoutingModule } from './refregerator-routing.module';
import { AddRefregeratorComponent } from './add-refregerator/add-refregerator.component';
import { UpdateRefregeratorComponent } from './update-refregerator/update-refregerator.component';
import { AddCratesComponent } from './crates/add-crates/add-crates.component';
import { AddRemovalComponent } from './removal/add-removal/add-removal.component';
import { AddPaymentComponent } from './payment/add-payment/add-payment.component';




@NgModule({
    declarations:[
        RefregeratorComponent,
        CratesComponent,
        RemovalComponent,
        PaymentComponent,
        InformationComponent,
        AddRefregeratorComponent,
        UpdateRefregeratorComponent,
        AddCratesComponent,
        AddRemovalComponent,
        AddPaymentComponent
    ],
    imports:[
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule,
        RefregeratorRoutingModule
    ]
})
export class RefregeratorModule {}