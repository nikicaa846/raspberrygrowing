import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RaspberryGrowerModule } from './raspberry-grower/raspberry-grower.module';
import { AuthModule } from './auth/auth.module';
import { RefregeratorModule } from './refregerator/refregerator.module';
import { PurchaseModule } from './purchase/purchase.module';
import { WorkerModule } from './worker/worker.module';
import { RasbperryProcesingModule } from './raspberry-processing/raspberry-procesing.module';
import { InvestmentModule } from './investment/investment.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    RaspberryGrowerModule,
    AuthModule,
    RefregeratorModule,
    PurchaseModule,
    WorkerModule,
    RasbperryProcesingModule,
    InvestmentModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
