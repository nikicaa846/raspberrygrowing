import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-update-worker',
  templateUrl: './update-worker.component.html',
  styleUrls: ['./update-worker.component.css']
})
export class UpdateWorkerComponent implements OnInit {

  workerForm: FormGroup = new FormGroup({
    'name' : new FormControl(''),
    'surname': new FormControl(''),
    'phone': new FormControl('')
  });
  id: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private workerService: WorkerService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.workerService.getById(this.id).subscribe(
      data => {
        this.workerForm = new FormGroup({
          'name' : new FormControl(data.name),
          'surname': new FormControl(data.surname),
          'phone': new FormControl(data.phone)
        });
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.update();
    }
  }

  update(){
    this.workerService.updateWorker(this.id, this.workerForm.value).subscribe(
      data => {
        this.router.navigate(['/worker']).then(
          () => window.location.reload()
        )
      }
    )
  }
}
