import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WorkerComponent } from './worker.component';
import { WorkerInformationComponent } from './worker-information/worker-information.component';
import { AddWorkerComponent } from './add-worker/add-worker.component';
import { UpdateWorkerComponent } from './update-worker/update-worker.component';
import { WorkerRoutingModule } from './worker-routing.module';
import { WorkerDefrayalComponent } from './worker-defrayal/worker-defrayal.component';
import { AddDefrayalComponent } from './worker-defrayal/add-defrayal/add-defrayal.component';
import { WorkerDetailsComponent } from './worker-details/worker-details.component';

@NgModule({
    declarations:[
        WorkerComponent,
        WorkerInformationComponent,
        AddWorkerComponent,
        UpdateWorkerComponent,
        WorkerDefrayalComponent,
        AddDefrayalComponent,
        WorkerDetailsComponent,
    ],
    imports:[
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        CommonModule,
        WorkerRoutingModule
    ]
})
export class WorkerModule{}