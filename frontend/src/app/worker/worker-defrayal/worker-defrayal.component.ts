import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { WorkerDefrayal } from 'src/app/models/worker-defrayal';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { WorkerDefrayalService } from 'src/app/services/worker-defrayal.service';

@Component({
  selector: 'app-worker-defrayal',
  templateUrl: './worker-defrayal.component.html',
  styleUrls: ['./worker-defrayal.component.css']
})
export class WorkerDefrayalComponent implements OnInit {

  defrayalForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  showDefrayal = false;
  defrayals: WorkerDefrayal[] = [];
  plantations: RaspberryPlantation[] = [];
  idGrower: number;

  constructor(
    private workerdefrayalService: WorkerDefrayalService,
    private plantationService: RaspberryPlantationService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  onFind() {
    this.workerdefrayalService.findDefrayalByDateAndPlantation(this.defrayalForm.value['date'], this.defrayalForm.value['idPlantation']).subscribe(
      data => {
        this.defrayals = data;
        this.showDefrayal = true;
      }, error => {
        if (error.status === 404) {
          window.alert("No defrayal with this date!");
          this.showDefrayal = false;
          this.defrayalForm = new FormGroup({
            'date': new FormControl(''),
            'idPlantation': new FormControl('')
          });
        }
      }
    )
  }

  onClose() {
    this.showDefrayal = false;
    this.defrayalForm = new FormGroup({
      'date': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }

}
