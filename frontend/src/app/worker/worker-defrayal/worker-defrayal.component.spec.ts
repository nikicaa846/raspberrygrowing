import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerDefrayalComponent } from './worker-defrayal.component';

describe('WorkerDefrayalComponent', () => {
  let component: WorkerDefrayalComponent;
  let fixture: ComponentFixture<WorkerDefrayalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerDefrayalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerDefrayalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
