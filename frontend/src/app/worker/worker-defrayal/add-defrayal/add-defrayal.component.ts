import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { Worker } from 'src/app/models/worker';
import { WorkerDefrayal } from 'src/app/models/worker-defrayal';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { WorkerDefrayalService } from 'src/app/services/worker-defrayal.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-add-defrayal',
  templateUrl: './add-defrayal.component.html',
  styleUrls: ['./add-defrayal.component.css']
})
export class AddDefrayalComponent implements OnInit {

  defrayalForm : FormGroup = new FormGroup({
    'date': new FormControl(''),
    'idPlantation': new FormControl(''),
    'price': new FormControl(''),
    'idWorker': new FormControl(''),
    'kind' : new FormControl(''),
    'month': new FormControl('')
  });
  workers: Worker[] = [];
  plantations: RaspberryPlantation[] = [];
  idGrower: number;

  constructor(
    private workerService: WorkerService,
    private workerdefrayalService: WorkerDefrayalService,
    private plantationService: RaspberryPlantationService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.workerService.findWorkers(this.idGrower).subscribe(
      data => {
        this.workers = data;
      }
    );
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onAdd();
    }
  }

  onAdd(){
    this.workerService.getById(this.defrayalForm.value['idWorker']).subscribe(
      worker => {
        this.plantationService.getPlantation(this.defrayalForm.value['idPlantation']).subscribe(
          plantation => {
            const defrayal = new WorkerDefrayal(
              this.defrayalForm.value['date'],
              this.defrayalForm.value['month'],
              this.defrayalForm.value['kind'],
              plantation,
              worker,
              this.defrayalForm.value['price']
            );
            this.workerdefrayalService.addDefrayal(defrayal).subscribe(
              data => {
                window.location.reload();
              }
            )
          }
        )
      }
    )
  }
}
