import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDefrayalComponent } from './add-defrayal.component';

describe('AddDefrayalComponent', () => {
  let component: AddDefrayalComponent;
  let fixture: ComponentFixture<AddDefrayalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDefrayalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDefrayalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
