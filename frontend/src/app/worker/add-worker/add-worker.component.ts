import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Worker } from 'src/app/models/worker';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-add-worker',
  templateUrl: './add-worker.component.html',
  styleUrls: ['./add-worker.component.css']
})
export class AddWorkerComponent implements OnInit {

  workerForm: FormGroup = new FormGroup({
    'name': new FormControl(''),
    'surname': new FormControl(''),
    'phone': new FormControl(''),
    'idGrower': new FormControl('')
  });
  idGrower: number;

  constructor(
    private authService: AuthService,
    private router: Router,
    private workerService: WorkerService,
    private growerService: RaspberryGrowerService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    if (this.idGrower !== 0) {
      this.workerForm = new FormGroup({
        'name': new FormControl(''),
        'surname': new FormControl(''),
        'phone': new FormControl(''),
        'idGrower': new FormControl(this.idGrower)
      });
    }
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.growerService.getById(this.workerForm.value['idGrower']).subscribe(
      grower => {
        const worker = new Worker(
          this.workerForm.value['name'],
          this.workerForm.value['surname'],
          this.workerForm.value['phone'],
          grower
        );
        this.workerService.addWorker(worker).subscribe(
          data => {
            this.router.navigate(['/worker']);
          }
        )
      }
    )
  }
}
