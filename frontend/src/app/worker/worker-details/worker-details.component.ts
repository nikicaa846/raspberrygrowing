import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { Worker } from 'src/app/models/worker';
import { WorkerDefrayal } from 'src/app/models/worker-defrayal';
import { RaspberryDiggingService } from 'src/app/services/raspberry-digging.service';
import { RaspberryFertilizationWorkerService } from 'src/app/services/raspberry-fertilization-worker.service';
import { RaspberryHaircutService } from 'src/app/services/raspberry-haircut.service';
import { RasbperryHarvestService } from 'src/app/services/raspberry-harvest.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RaspberrySprayingWorkerService } from 'src/app/services/raspberry-spraying-worker.service';
import { WorkerDefrayalService } from 'src/app/services/worker-defrayal.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-worker-details',
  templateUrl: './worker-details.component.html',
  styleUrls: ['./worker-details.component.css']
})
export class WorkerDetailsComponent implements OnInit {

  workerForm: FormGroup = new FormGroup({
    'year': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  workerDefrayals: WorkerDefrayal[] = [];
  plantations: RaspberryPlantation[] = [];
  id: number;
  showDefrayal = false;
  worker: Worker = new Worker(null, null, null, null);

  constructor(
    private defraylService: WorkerDefrayalService,
    private plantationService: RaspberryPlantationService,
    private harvestService: RasbperryHarvestService,
    private digginService: RaspberryDiggingService,
    private haircutService: RaspberryHaircutService,
    private fertilizationWorkerService: RaspberryFertilizationWorkerService,
    private sprayingWorkerService: RaspberrySprayingWorkerService,
    private workerService: WorkerService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.plantationService.findPlantations(this.id).subscribe(
      data => {
        this.plantations = data;
      }
    );
    this.workerService.getById(this.id).subscribe(
      data => {
        this.worker = data;
      }
    );
    this.getHarvestHoursPerMonth();
    this.getHaircutHorusPerMonth();
    this.getDiggingHorusPerMonth();
    this.getFertilizationHoursPerMonth();
    this.getSprayingHoursPerMonth();
  }

  harvestHours: number[] = [];
  haircutHours: number[] = [];
  diggingHours: number[] = [];
  fertilizationHours: number[] = [];
  sprayingHours: number[] = [];
  months: string[] = ['January', 'February', 'March', 'April', 'May', 'Jun', 'July',
                      'Avgust', 'September' ,'October', 'November', 'December'];

  getHarvestHoursPerMonth() {
    for ( let i = 0; i < this.months.length; i++ ){
      this.harvestService.getTotalWorkerHourPerMonth(this.id, this.months[i]).subscribe(
        data => {
           this.harvestHours.push(data)
        }
      );
    }
  }

  getDiggingHorusPerMonth() {
    for ( let i = 0; i < this.months.length; i++ ){
      this.digginService.getTotalWorkerHourPerMonth(this.id, this.months[i]).subscribe(
        data => {
          this.diggingHours.push(data);
        }
      )
    }
  }

  getHaircutHorusPerMonth() {
    for ( let i = 0; i < this.months.length; i++ ){
      this.haircutService.getTotalWorkerHourPerMonth(this.id, this.months[i]).subscribe(
        data => {
          this.haircutHours.push(data);
        }
      )
    }
  }

  getFertilizationHoursPerMonth() {
    for ( let i = 0; i < this.months.length; i++ ){
      this.fertilizationWorkerService.getTotalWorkerHourPerMonth(this.id, this.months[i]).subscribe(
        data => {
           this.fertilizationHours.push(data)
        }
      );
    }
  }

  getSprayingHoursPerMonth() {
    for ( let i = 0; i < this.months.length; i++ ){
      this.sprayingWorkerService.getTotalWorkerHourPerMonth(this.id, this.months[i]).subscribe(
        data => {
           this.sprayingHours.push(data)
        }
      );
    }
  }

  onFind(){
    this.showDefrayal = true;
    this.defraylService.findDefrayalsWorkerPlantationYear(this.id, this.workerForm.value['idPlantation'], this.workerForm.value['year']).
    subscribe( 
      data => {
        this.workerDefrayals = data;
      }
    )
  }

  onClose(){
    this.showDefrayal = false;
    this.workerForm = new FormGroup({
      'year': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }
}
