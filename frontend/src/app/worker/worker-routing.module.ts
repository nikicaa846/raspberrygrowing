import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGaurdService } from '../services/authguard.service';
import { AddWorkerComponent } from './add-worker/add-worker.component';
import { UpdateWorkerComponent } from './update-worker/update-worker.component';
import { AddDefrayalComponent } from './worker-defrayal/add-defrayal/add-defrayal.component';
import { WorkerDefrayalComponent } from './worker-defrayal/worker-defrayal.component';
import { WorkerDetailsComponent } from './worker-details/worker-details.component';
import { WorkerInformationComponent } from './worker-information/worker-information.component';

import { WorkerComponent } from './worker.component';

const routes: Routes = [
    { path: '', component: WorkerComponent, canActivate: [AuthGaurdService], children:[
        { path: 'worker', component: WorkerInformationComponent },
        { path: 'addworker', component: AddWorkerComponent },
        { path: 'updateworker/:id', component: UpdateWorkerComponent},
        { path: 'workerdefrayal', component: WorkerDefrayalComponent},
        { path: 'adddefrayal', component: AddDefrayalComponent},
        { path: 'workerdetails/:id', component: WorkerDetailsComponent}
    ]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WorkerRoutingModule{}