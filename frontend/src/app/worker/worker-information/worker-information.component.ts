import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Worker } from 'src/app/models/worker';
import { AuthService } from 'src/app/services/auth.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-worker-information',
  templateUrl: './worker-information.component.html',
  styleUrls: ['./worker-information.component.css']
})
export class WorkerInformationComponent implements OnInit {

  isAdmin: boolean;
  workers: Worker[] = [];
  idGrower: number;

  constructor(
    private authService: AuthService,
    private workerService: WorkerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.idGrower = + this.authService.getGrower();
    this.workerService.findWorkers(this.idGrower).subscribe(
      data => {
        this.workers = data;
      }
    )
  }

  onUpdate(id: number) {
    this.router.navigate(['/updateworker', id]);
  }

  onDelete(id: number) {
    this.workerService.deleteWorker(id).subscribe(
      data => {
        window.location.reload();
      }
    )
  }

  onDetails(id: number) {
    this.router.navigate(['/workerdetails', id]);
  }
}
