import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Refrigerator } from "../models/refrigerator";

@Injectable({providedIn: 'root'})
export class RefrigeratorService{

    url = 'http://localhost:8081/api/v1/refrigerator';

    constructor( private http: HttpClient){}

    public getById(id: number): Observable<Refrigerator> {
        return this.http.get<Refrigerator>(this.url + '/' + id);
    }

    public findAll(): Observable<Refrigerator[]> {
        return this.http.get<Refrigerator[]>(this.url);
    }

    public addRefrigerator(refrigerator: Refrigerator): Observable<Object> {
        return this.http.post(this.url, refrigerator);
    }

    public updateRefrigerator(id: number, refrigerator: Refrigerator): Observable<Object> {
        return this.http.put(this.url + '/' + id, refrigerator);
    }

    public deleteRefrigerator(id: number){
        return this.http.delete(this.url + '/' + id);
    }
}