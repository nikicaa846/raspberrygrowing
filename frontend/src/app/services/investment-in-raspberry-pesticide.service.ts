import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { InvestmentInRaspberryPesticide } from "../models/investment-in-raspberry-pesticide";

@Injectable({providedIn:'root'})
export class InvestmentInRaspberryPesticideService{

    url = 'http://localhost:8081/api/v1/raspberryinvestmentinpesticide';

    constructor( private http: HttpClient ){}

    addInvestmentInPesticide(investment: InvestmentInRaspberryPesticide){
        return this.http.post(this.url, investment);
    }

    public findInvestmentsByDateAndGrower(date: number, idGrower: number): Observable<InvestmentInRaspberryPesticide[]>{
        return this.http.get<InvestmentInRaspberryPesticide[]>(this.url + '?date=' + date + '&idGrower=' + idGrower);
    }

    public getTotalInvestmentForYear(year: number, idGrower: number): Observable<number>{
        return this.http.get<number>(this.url + '/total?year=' + year + '&idGrower=' + idGrower);
    }
}