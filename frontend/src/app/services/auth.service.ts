import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user';

import { UserService } from './user.service';

@Injectable({ providedIn: 'root' })
export class AuthService {

    user: User;
    role: string;
    idGrower: string;

    constructor(private userService: UserService, private router: Router) { }

    login(username: string, password: string) {
        this.userService.getByUsername(username).subscribe(
            data => {
                this.user = data;
                this.role = data.role;
                if (data.raspberryGrower === null) {
                    this.idGrower = "0";
                } else {
                    this.idGrower = '' + data.raspberryGrower.id;
                }
                if (this.user.password !== password) {
                    window.alert("Incorrect password!");
                } else {
                    sessionStorage.setItem('username', username);
                    sessionStorage.setItem('role', this.role);
                    sessionStorage.setItem('grower', this.idGrower);
                    this.router.navigate(['/homepage']).then(() => {
                        window.location.reload();
                    });
                }
            },
            error => {
                if (error.status === 404) {
                    window.alert("This user doesn't exist!");
                }
            }
        );
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem('username')
        return !(user === null)
    }

    isAdmin() {
        let user = sessionStorage.getItem('role');
        return (user === "admin");
    }

    getGrower() {
        let grower = sessionStorage.getItem('grower');
        return grower;
    }

    logOut() {
        sessionStorage.removeItem('username');
        sessionStorage.removeItem('role');
        this.router.navigate(['/homepage']).then(() => {
            window.location.reload();
        });
    }
}