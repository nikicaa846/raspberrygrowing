import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { RefrigeratedPayment } from "../models/refrigeratedpayment";

@Injectable({providedIn: 'root'})
export class RefrigeratorPaymentService{

    url = 'http://localhost:8081/api/v1/payment';

    constructor( private http: HttpClient){}

    public addPayment(payment : RefrigeratedPayment ): Observable< Object> {
        return this.http.post(this.url, payment);
    } 

    public findPayments(year: number, idPlantation: number): Observable<RefrigeratedPayment[]> {
        return this.http.get<RefrigeratedPayment[]>(this.url + '?year=' + year + '&idPlantation=' + idPlantation);
    }

    public getTotalPaymentForPlantation(id: number, year: number ): Observable<number>{
        return this.http.get<number>(this.url + '/plantation?idPlantation=' + id + '&year=' + year);
    }
}