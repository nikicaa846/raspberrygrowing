import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RaspberryDigging } from "../models/raspberrydigging";

@Injectable({providedIn: 'root'})
export class RaspberryDiggingService{

    url = 'http://localhost:8081/api/v1/digging';

    constructor(private http: HttpClient){}

    public findDiggingByDateAndPlantation(date: number, idplantation: number): Observable<RaspberryDigging[]> {
        return this.http.get<RaspberryDigging[]>(this.url + '?date=' + date + '&idPlantation=' + idplantation);
    }

    public getTotalWorkerHourPerMonth(idWorker: number, monthName: string ): Observable<number>{
        return this.http.get<number>(this.url + '/worker?idWorker=' + idWorker + '&monthName=' + monthName);
    }

    public addDigging(digging: RaspberryDigging ){
        return this.http.post(this.url, digging);
    }
}