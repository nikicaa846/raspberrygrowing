import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { InvestmentInRaspberryFertilizer } from "../models/investment-in-raspberry-fertilizer";

@Injectable({providedIn:'root'})
export class InvestmentInRaspberryFertilizerService{

    url = 'http://localhost:8081/api/v1/raspberryinvestmentinfertilizer';

    constructor( private http: HttpClient ){}

    addInvestmentInFertilizer(investment: InvestmentInRaspberryFertilizer){
        return this.http.post(this.url, investment);
    }

    public findInvestmentsByDateAndGrower(date: number, idGrower): Observable<InvestmentInRaspberryFertilizer[]>{
        return this.http.get<InvestmentInRaspberryFertilizer[]>(this.url + '?date=' + date + '&idGrower=' + idGrower);
    }

    public getTotalInvestmentForYear(year: number, idGrower: number): Observable<number>{
        return this.http.get<number>(this.url + '/total?year=' + year + '&idGrower=' + idGrower);
    }
}