import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RaspberryFertilizer } from "../models/raspberry-fertilizer";

@Injectable({providedIn: 'root'})
export class RaspberryFertilizerService{

    url = 'http://localhost:8081/api/v1/fertilizer';

    constructor( private http: HttpClient){}
    
    public findFertilizers( idgrower : number ): Observable<RaspberryFertilizer[]>{
        return this.http.get<RaspberryFertilizer[]>(this.url + '?idGrower=' + idgrower);
    }

    public getFertilizer(id: number): Observable<RaspberryFertilizer> {
        return this.http.get<RaspberryFertilizer>(this.url + '/' + id);
    }

    public addFertilizer(fertilizer: RaspberryFertilizer){
        return this.http.post(this.url, fertilizer);
    }

    public updateFertilizer(id: number, fertilizer: RaspberryFertilizer){
        return this.http.put(this.url + '/' + id, fertilizer);
    }

    public deleteFertilizer(id: number){
        return this.http.delete(this.url + '/' + id);
    }
}