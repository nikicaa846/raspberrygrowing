import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RaspberryInvestment } from "../models/raspberry-investment";

@Injectable({providedIn: 'root'})
export class RaspberryInvestmentService{

    url = 'http://localhost:8081/api/v1/investment';

    constructor( private http: HttpClient ){}

    public addInvestment(investment: RaspberryInvestment){
        return this.http.post(this.url, investment);
    }
    
    public findInvestmentsByDateAndGrower(date: number, idGrower: number): Observable<RaspberryInvestment[]>{
        return this.http.get<RaspberryInvestment[]>(this.url + '?date=' + date + '&idGrower=' + idGrower);
    }

    public getTotalInvestmentForYear(year: number, idGrower: number): Observable<number>{
        return this.http.get<number>(this.url + '/total?year=' + year + '&idGrower=' + idGrower);
    }
}