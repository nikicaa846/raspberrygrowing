import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RaspberrySpraying } from "../models/raspberry-spraying";

@Injectable({providedIn: 'root'})
export class RaspberrySprayingService{

    url = 'http://localhost:8081/api/v1/spraying';

    constructor( private http: HttpClient){}

    public getById(id: number): Observable<RaspberrySpraying>{
        return this.http.get<RaspberrySpraying>(this.url + '/' + id);
    }

    public findSprayingByDatePlantationPesticide(date: string, idPlantation: number, idPesticide: number): Observable<RaspberrySpraying> {
        return this.http.get<RaspberrySpraying>(this.url + '/sprayingworker?date=' + date + '&idPlantation=' + idPlantation + '&idPesticide=' + idPesticide);
    }

    public findSprayingByDateAndPlantation(date: number, id: number): Observable<RaspberrySpraying[]> {
        return this.http.get<RaspberrySpraying[]>(this.url + '?date=' + date + '&idPlantation=' + id);
    }

    public addSpraying(spraying: RaspberrySpraying){
        return this.http.post(this.url, spraying);
    }
}