import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RaspberryFertilizationWorker } from "../models/raspberry-fertilization-worker";

@Injectable({providedIn: 'root'})
export class RaspberryFertilizationWorkerService{

    url = 'http://localhost:8081/api/v1/fertilizationworker';

    constructor( private http: HttpClient ){}

    public findByFertilization(id: number): Observable<RaspberryFertilizationWorker[]> {
        return this.http.get<RaspberryFertilizationWorker[]>(this.url + '?id=' + id);
    }

    public getTotalWorkerHourPerMonth(idWorker: number, monthName: string ): Observable<number>{
        return this.http.get<number>(this.url + '/worker?idWorker=' + idWorker + '&monthName=' + monthName);
    }

    public addFertilizationWorker(fertilizationWorker: RaspberryFertilizationWorker){
        return this.http.post(this.url, fertilizationWorker);
    }
}