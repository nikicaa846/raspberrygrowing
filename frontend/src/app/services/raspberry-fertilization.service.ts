import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { RaspberryFertilization } from "../models/raspberry-fertilization";

@Injectable({providedIn: 'root'})
export class RaspberryFertilizationService{

    url = 'http://localhost:8081/api/v1/fertilization';

    constructor( private http: HttpClient ){}

    public getById(id: number):  Observable<RaspberryFertilization>{
        return this.http.get<RaspberryFertilization>(this.url + '/' + id);
    }
    
    public findFertilizationByDateAndPlantationAndFertilizer(date: string, idPlantation: number, idFertilizer: number): Observable<RaspberryFertilization> {
        return this.http.get<RaspberryFertilization>(this.url + '/fertilizationworker?date=' + date + '&idPlantation=' + idPlantation + '&idFertilizer=' + idFertilizer);
    }

    public findFertilizationByDateAndPlantation(date: number, idPlantation: number): Observable<RaspberryFertilization[]> {
        return this.http.get<RaspberryFertilization[]>(this.url + '?date=' + date + '&idPlantation=' + idPlantation);
    }

    public addFertilization( fertilization: RaspberryFertilization){
        return this.http.post(this.url, fertilization);
    }
}