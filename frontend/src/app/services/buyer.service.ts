import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Buyer } from "../models/buyer";

@Injectable({providedIn:'root'})
export class BuyerService{

    url = 'http://localhost:8081/api/v1/buyer';

    constructor(private http: HttpClient){}

    public getBuyer(id: number): Observable<Buyer> {
        return this.http.get<Buyer>(this.url + '/' + id);
    }

    public findBuyers(idGrower: number): Observable<Buyer[]> {
        return this.http.get<Buyer[]>(this.url + '?idGrower=' + idGrower);
    }

    public addBuyer(buyer: Buyer): Observable<Object> {
        return this.http.post(this.url, buyer);
    }

    public updateBuyer(id: number, buyer: Buyer): Observable<Object> {
        return this.http.put(this.url + '/' + id, buyer);
    }

    public deleteBuyer(id: number){
        return this.http.delete(this.url + '/' + id);
    }
}