import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { RaspberryPesticides } from 'src/app/models/raspberry-pesticides';
@Injectable({providedIn: 'root'})
export class RaspberryPesticidesService{

    url = 'http://localhost:8081/api/v1/pesticide';

    constructor( private http: HttpClient ){}

    public getPesticide(id: number): Observable<RaspberryPesticides>{
        return this.http.get<RaspberryPesticides>(this.url + '/' + id);
    }

    public findPesticides(idgrower: number): Observable<RaspberryPesticides[]>{
        return this.http.get<RaspberryPesticides[]>(this.url + '?idGrower=' + idgrower);
    }

    public addPesticide(pesticide: RaspberryPesticides){
        return this.http.post(this.url, pesticide);
    }

    public updatePesticide(id: number, pesticide: RaspberryPesticides){
        return this.http.put( this.url + '/' + id, pesticide);
    }

    public deletePesticide(id: number){
        return this.http.delete(this.url + '/' + id);
    }
}