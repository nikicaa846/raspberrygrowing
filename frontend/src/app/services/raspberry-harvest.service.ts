import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RaspberryHarvest } from "../models/raspberryharvest";

@Injectable({providedIn: 'root'})
export class RasbperryHarvestService{

    url = 'http://localhost:8081/api/v1/harvest';

    constructor(private http: HttpClient){}

    public findHarvestByDateAndPlantation(date: Date, id: number): Observable<RaspberryHarvest[]> {
        return this.http.get<RaspberryHarvest[]>(this.url + '?date=' + date + '&idPlantation=' + id );
    }

    public getTotalWorkerHourPerMonth(idWorker: number, monthName: string ): Observable<number>{
        return this.http.get<number>(this.url + '/worker?idWorker=' + idWorker + '&monthName=' + monthName);
    }

    public addHarvest(harvest: RaspberryHarvest ){
        return this.http.post(this.url , harvest);
    }
}