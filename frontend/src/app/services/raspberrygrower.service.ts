import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { RaspberryGrower } from '../models/raspberrygrower';

@Injectable({providedIn: 'root'})
export class RaspberryGrowerService{

    url = 'http://localhost:8081/api/v1/grower';

    constructor(private http: HttpClient){}

    public getById(id: number): Observable<RaspberryGrower> {
        return this.http.get<RaspberryGrower>(this.url + '/' + id);
    }

    public getProfit(id: number, year: number): Observable<number>{
        return this.http.get<number>(this.url + '/profit?id=' + id + '&year=' + year);
    }

    public addGrower(grower : RaspberryGrower): Observable<Object> {
        return this.http.post(this.url, grower);
    }

    public updateGrower(id: number, grower: RaspberryGrower): Observable<Object>{
        return this.http.put(this.url + '/' + id, grower);
    }

    public deleteGrower(id: number){
        return this.http.delete(this.url + '/' + id);
    }
}