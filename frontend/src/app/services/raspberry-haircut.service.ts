import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { RaspberryHaircut } from "../models/raspberryhaircut";

@Injectable({providedIn: 'root'})
export class RaspberryHaircutService{
    
    url = 'http://localhost:8081/api/v1/haircut';

    constructor(private http: HttpClient){}

    public findHaircutByDateAndPlantation(date: Date, id: number): Observable<RaspberryHaircut[]>{
        return this.http.get<RaspberryHaircut[]>(this.url + '?date=' + date + '&idPlantation=' + id );
    }

    public getTotalWorkerHourPerMonth(idWorker: number, monthName: string ): Observable<number>{
        return this.http.get<number>(this.url + '/worker?idWorker=' + idWorker + '&monthName=' + monthName);
    }

    public addHaircut( haircut: RaspberryHaircut ){
        return this.http.post(this.url, haircut);
    }
}