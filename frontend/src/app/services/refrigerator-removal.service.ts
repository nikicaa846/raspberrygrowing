import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { RefrigeratedRemoval } from "../models/refrigeratedremoval";

@Injectable({providedIn: 'root'})
export class RefrigeratorRemovalService{

    url = 'http://localhost:8081/api/v1/removal';

    constructor(private http: HttpClient){}

    public findByKindOfRaspberryAndDate(idPlantation: number, kindOfRaspberry: string, date: string): Observable<RefrigeratedRemoval[]> {
        return this.http.get<RefrigeratedRemoval[]>(this.url + '?idPlantation=' + idPlantation +'&raspberry=' + kindOfRaspberry + "&date=" + date);
     }

    public addRemoval(removal : RefrigeratedRemoval ): Observable< Object> {
        return this.http.post(this.url, removal);
    }   

    public getTotalWeight( id: number, year: number, kind: string): Observable<number>{
        return this.http.get<number>(this.url + '/weight?idPlantation=' + id + '&year=' + year + '&kind=' + kind);
    }
}