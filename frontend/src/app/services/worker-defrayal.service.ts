import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { WorkerDefrayal } from "../models/worker-defrayal";

@Injectable({providedIn: 'root'})
export class WorkerDefrayalService{

    url = 'http://localhost:8081/api/v1/workerdefrayal';

    constructor( private http: HttpClient ){}

    public findDefrayalByDateAndPlantation(date: Date, idPlantation: number): Observable<WorkerDefrayal[]> {
        return this.http.get<WorkerDefrayal[]>(this.url + '?date=' + date + '&idPlantation=' + idPlantation);
    }

    public addDefrayal(defrayal: WorkerDefrayal){
        return this.http.post(this.url, defrayal);
    }

    public getTotalDefrayalForPlantation(id: number, year: number): Observable<number>{
        return this.http.get<number>(this.url + '/plantation?idPlantation=' + id + '&year=' + year);
    }

    public findDefrayalsWorkerPlantationYear(idWorker: number, idPlantation: number, year: number): Observable<WorkerDefrayal[]>{
        return this.http.get<WorkerDefrayal[]>
        (this.url + '/worker?idWorker=' + idWorker + '&idPlantation=' + idPlantation + '&year=' + year);
    }
}