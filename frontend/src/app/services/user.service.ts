import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({providedIn: 'root'})
export class UserService{

    url = "http://localhost:8081/api/v1/users";

    constructor(private http: HttpClient){}

    public getById(id: number): Observable<User> {
        return this.http.get<User>(this.url + '/' + id);
    }

    public getByUsername(username: string): Observable<User> {
        return this.http.get<User>(this.url + "?username=" + username);
    }

    public addUser(user: User): Observable<Object> {
        return this.http.post(this.url, user);
    }

    public updateUser(id: number, user: User): Observable<Object> {
        return this.http.put(this.url + '/' + id, user);
    }

    public deleteUser(id: number){
        return this.http.delete(this.url + '/' + id);
    }

}