import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { RefrigeratedCrates } from "../models/refrigeratedcrates";

@Injectable({providedIn: 'root'})
export class RefrigeratorCratesService{

    url = "http://localhost:8081/api/v1/crates";

    constructor(private http: HttpClient){}

    public addCrates(crates : RefrigeratedCrates ): Observable< Object> {
        return this.http.post(this.url, crates);
    }   

    public findCrates( date: number , idPlantation: number): Observable<RefrigeratedCrates[]> {
        return this.http.get<RefrigeratedCrates[]>(this.url + '?date=' + date + '&idPlantation=' + idPlantation);
    }
}