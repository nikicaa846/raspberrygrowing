import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RaspberryPlantation } from '../models/raspberryplantation';

@Injectable({providedIn: 'root'})
export class RaspberryPlantationService{

    url = 'http://localhost:8081/api/v1/plantation';

    constructor(private http: HttpClient){}

    public getPlantation(id: number): Observable<RaspberryPlantation> {
        return this.http.get<RaspberryPlantation>(this.url + '/' + id)
    }

    public findPlantations(idGrower: number): Observable<RaspberryPlantation[]> {
        return this.http.get<RaspberryPlantation[]>(this.url + '?idGrower=' + idGrower);
    }

    public addPlantation(plantation: RaspberryPlantation): Observable<Object> {
        return this.http.post(this.url, plantation);
    }

    public updatePlantation(id: number, plantation: RaspberryPlantation): Observable<Object> {
        return this.http.put(this.url + '/' + id, plantation);
    }

    public deletePlantation(id: number){
        return this.http.delete(this.url + '/' + id);
    }
}