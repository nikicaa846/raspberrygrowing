import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RaspberrySprayingWorker } from "../models/raspberry-spraying-worker";

@Injectable({providedIn:'root'})
export class RaspberrySprayingWorkerService{
    
    url = 'http://localhost:8081/api/v1/sprayingworker';

    constructor( private http: HttpClient){}

    public findBySpraying(id: number): Observable<RaspberrySprayingWorker[]>{
        return this.http.get<RaspberrySprayingWorker[]>(this.url + '?id=' + id);
    }

    public getTotalWorkerHourPerMonth(idWorker: number, monthName: string ): Observable<number>{
        return this.http.get<number>(this.url + '/worker?idWorker=' + idWorker + '&monthName=' + monthName);
    }

    public addSprayingWorker(sprayingWorker: RaspberrySprayingWorker){
        return this.http.post(this.url, sprayingWorker);
    }
}