import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Purchase } from "../models/purchase";

@Injectable({providedIn:'root'})
export class PurchaseService{

    url ='http://localhost:8081/api/v1/purchase';

    constructor(private http: HttpClient){}

    public findByDateAndPlantation(date: number, idPlantation: number): Observable<Purchase[]>{
        return this.http.get<Purchase[]>(this.url + "?date=" + date + "&idPlantation=" + idPlantation);
    }

    public getTotalEarnings(year: number, id: number ): Observable<number>{
        return this.http.get<number>(this.url + '/earnings?year=' + year + '&idPlantation=' + id);
    }

    public getTotalPurchaseWeight(year: number, id: number, kind: String): Observable<number>{
        return this.http.get<number>(this.url + '/weight?year=' + year + '&idPlantation=' + id + '&kind=' + kind);
    }

    public addPurchase(purchase: Purchase){
        return this.http.post(this.url, purchase);
    }
}