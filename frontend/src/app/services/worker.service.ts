import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Worker } from "../models/worker";

@Injectable({providedIn: 'root'})
export class WorkerService{

    url = 'http://localhost:8081/api/v1/worker';

    constructor( private http: HttpClient ){}

    public getById(id: number): Observable<Worker> {
        return this.http.get<Worker>(this.url + '/' + id);
    }

    public findWorkers(idgrower: number): Observable<Worker[]>{
        return this.http.get<Worker[]>(this.url + '?idGrower=' + idgrower);
    }

    public addWorker(worker: Worker): Observable<Object>{
        return this.http.post(this.url, worker);
    }

    public updateWorker( id: number, worker: Worker){
        return this.http.put(this.url + '/' + id, worker);
    }

    public deleteWorker(id: number){
        return this.http.delete(this.url + '/' + id);
    }
}