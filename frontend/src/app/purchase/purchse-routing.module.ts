import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGaurdService } from "../services/authguard.service";
import { AddPurchaseComponent } from "./add-purchase/add-purchase.component";
import { AddBuyerComponent } from "./buyer/add-buyer/add-buyer.component";
import { BuyerComponent } from "./buyer/buyer.component";
import { UpdateBuyerComponent } from "./buyer/update-buyer/update-buyer.component";
import { InformationComponent } from "./information/information.component";
import { PurchaseComponent } from "./purchase.component";

const routes: Routes = [
    { path: '', component: PurchaseComponent, canActivate: [AuthGaurdService],
     children:[
         { path: 'purchase', component: InformationComponent },
         { path: 'addpurchase', component: AddPurchaseComponent },
         { path: 'buyer', component: BuyerComponent },
         { path: 'addbuyer', component: AddBuyerComponent },
         { path: 'updatebuyer/:id', component: UpdateBuyerComponent }
     ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PurchaseRoutingModule{}