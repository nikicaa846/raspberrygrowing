import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Buyer } from 'src/app/models/buyer';
import { AuthService } from 'src/app/services/auth.service';
import { BuyerService } from 'src/app/services/buyer.service';

@Component({
  selector: 'app-buyer',
  templateUrl: './buyer.component.html',
  styleUrls: ['./buyer.component.css']
})
export class BuyerComponent implements OnInit {

  buyers: Buyer[] = [];
  isAdmin: boolean;
  idGrower: number;

  constructor(
    private router: Router,
    private buyerService: BuyerService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.idGrower = + this.authService.getGrower();
    this.buyerService.findBuyers(this.idGrower).subscribe(
      data => {
        this.buyers = data;
      }
    )
  }

  onUpdate(id: number){
    this.router.navigate(['updatebuyer', id]);
  }

  onDelete(id: number){
    this.buyerService.deleteBuyer(id).subscribe(
      data=> {
        window.location.reload();
      }
    )
  }
}
