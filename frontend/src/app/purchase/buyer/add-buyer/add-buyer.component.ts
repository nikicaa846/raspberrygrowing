import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Buyer } from 'src/app/models/buyer';
import { AuthService } from 'src/app/services/auth.service';
import { BuyerService } from 'src/app/services/buyer.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-add-buyer',
  templateUrl: './add-buyer.component.html',
  styleUrls: ['./add-buyer.component.css']
})
export class AddBuyerComponent implements OnInit {

  buyerForm: FormGroup = new FormGroup({
    'name': new FormControl(''),
    'surname': new FormControl(''),
    'phone': new FormControl(''),
    'idGrower': new FormControl('')
  });
  idGrower: number;

  constructor(
    private router: Router,
    private buyerService: BuyerService,
    private authService: AuthService,
    private growerService: RaspberryGrowerService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    if (this.idGrower !== 0) {
      this.buyerForm = new FormGroup({
        'name': new FormControl(''),
        'surname': new FormControl(''),
        'phone': new FormControl(''),
        'idGrower': new FormControl(this.idGrower)
      });
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.growerService.getById(this.buyerForm.value['idGrower']).subscribe(
      grower => {
        const buyer = new Buyer(
          this.buyerForm.value['name'],
          this.buyerForm.value['surname'],
          this.buyerForm.value['phone'],
          grower
        );
        this.buyerService.addBuyer(buyer).subscribe(
          data => {
            this.router.navigate(['buyer']);
          }
        )
      }
    )
  }
}
