import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { BuyerService } from 'src/app/services/buyer.service';

@Component({
  selector: 'app-update-buyer',
  templateUrl: './update-buyer.component.html',
  styleUrls: ['./update-buyer.component.css']
})
export class UpdateBuyerComponent implements OnInit {

  buyerForm: FormGroup = new FormGroup({
    'name': new FormControl(''),
    'surname': new FormControl(''),
    'phone': new FormControl('')
  });
  id: number;

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.update();
    }
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private buyerService: BuyerService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.buyerService.getBuyer(this.id).subscribe(
      data => {
        this.buyerForm = new FormGroup({
          'name': new FormControl(data.name),
          'surname': new FormControl(data.surname),
          'phone': new FormControl(data.phone)
        })
      }
    )
  }

  update() {
    this.buyerService.updateBuyer(this.id, this.buyerForm.value).subscribe(
      data => {
        this.router.navigate(['/buyer']);
      },
      error => window.alert(error)
    )
  }
}
