import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { PurchaseComponent } from './purchase.component';
import { BuyerComponent } from './buyer/buyer.component';
import { AddBuyerComponent } from './buyer/add-buyer/add-buyer.component';
import { AddPurchaseComponent } from './add-purchase/add-purchase.component';
import { UpdateBuyerComponent } from './buyer/update-buyer/update-buyer.component';
import { InformationComponent } from './information/information.component';
import { PurchaseRoutingModule } from './purchse-routing.module';

@NgModule({
    declarations:[
        PurchaseComponent,
        BuyerComponent,
        AddBuyerComponent,
        AddPurchaseComponent,
        UpdateBuyerComponent,
        InformationComponent
    ],
    imports:[
        BrowserAnimationsModule,
        RouterModule,
        NgbModule,
        ReactiveFormsModule,
        BsDropdownModule.forRoot(),
        CommonModule,
        PurchaseRoutingModule
    ]
})
export class PurchaseModule{}