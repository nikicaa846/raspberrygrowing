import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Purchase } from 'src/app/models/purchase';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { PurchaseService } from 'src/app/services/purchase.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

  purchaseForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  clickForFind = false;
  purchases: Purchase[] = [];
  idGrower: number;
  plantations: RaspberryPlantation[] = [];

  constructor(
    private purchaseService: PurchaseService,
    private authService: AuthService,
    private plantationService: RaspberryPlantationService
  ) { }

  ngOnInit(): void {
    this.idGrower = +this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  onFind() {
    this.purchaseService.findByDateAndPlantation(this.purchaseForm.value['date'], this.purchaseForm.value['idPlantation']).subscribe(
      data => {
        this.purchases = data;
        this.clickForFind = true;
      }, error => {
        if (error.status === 404) {
          window.alert("No purchases with date you inputed!");
        }
      }
    )
  }

  onClose() {
    this.clickForFind = false;
    this.purchaseForm = new FormGroup({
      'date': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }

}
