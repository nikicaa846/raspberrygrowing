import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Buyer } from 'src/app/models/buyer';
import { Purchase } from 'src/app/models/purchase';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { BuyerService } from 'src/app/services/buyer.service';
import { PurchaseService } from 'src/app/services/purchase.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';

@Component({
  selector: 'app-add-purchase',
  templateUrl: './add-purchase.component.html',
  styleUrls: ['./add-purchase.component.css']
})
export class AddPurchaseComponent implements OnInit {

  purchaseForm: FormGroup = new FormGroup({
    'idPlantation': new FormControl(''),
    'idBuyer': new FormControl(''),
    'date': new FormControl(''),
    'kindOfRaspberry': new FormControl(''),
    'weight': new FormControl(''),
    'charged': new FormControl(''),
    'price': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  buyers: Buyer[] = [];
  idGrower: number;

  constructor(
    private buyerService: BuyerService,
    private purchaseService: PurchaseService,
    private plantationService: RaspberryPlantationService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    );
    this.buyerService.findBuyers(this.idGrower).subscribe(
      data => {
        this.buyers = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.plantationService.getPlantation(this.purchaseForm.value['idPlantation']).subscribe(
      plantation => {
        this.buyerService.getBuyer(this.purchaseForm.value['idBuyer']).subscribe(
          buyer => {
           const purchase = new Purchase(
              plantation,
              buyer,
              this.purchaseForm.value['weight'],
              this.purchaseForm.value['kindOfRaspberry'],
              this.purchaseForm.value['charged'],
              new Date(this.purchaseForm.value['date']),
              this.purchaseForm.value['price']
            );
            this.purchaseService.addPurchase(purchase).subscribe(
              data => {
                this.router.navigate(['/purchase']);
              }
            )
          }
        )
      }
    );
  }
}
