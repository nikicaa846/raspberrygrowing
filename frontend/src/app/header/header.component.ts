import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  isLogin: boolean;
  isAdmin: boolean;
  idGrower: number;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.isLogin = this.authService.isUserLoggedIn();
    this.isAdmin = this.authService.isAdmin();
    this.idGrower = + this.authService.getGrower();
  }

  onLogout(){
    this.authService.logOut();
    this.router.navigate(['/homepage']);
  }
}