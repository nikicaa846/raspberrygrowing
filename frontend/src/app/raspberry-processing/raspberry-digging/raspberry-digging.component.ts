import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryDigging } from 'src/app/models/raspberrydigging';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryDiggingService } from 'src/app/services/raspberry-digging.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';

@Component({
  selector: 'app-raspberry-digging',
  templateUrl: './raspberry-digging.component.html',
  styleUrls: ['./raspberry-digging.component.css']
})
export class RaspberryDiggingComponent implements OnInit {

  diggingForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  showDigging = false;
  plantations: RaspberryPlantation[] = [];
  digginges: RaspberryDigging[] = [];
  idGrower: number;

  constructor(
    private authService: AuthService,
    private plantationService: RaspberryPlantationService,
    private diggingService: RaspberryDiggingService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  onFind() {
    this.diggingService.findDiggingByDateAndPlantation(this.diggingForm.value['date'], this.diggingForm.value['idPlantation'])
      .subscribe(
        data => {
          this.digginges = data;
          this.showDigging = true;
        }, error => {
          if (error.status === 404) {
            this.showDigging = false;
            window.alert("No digginges with this date");
            this.diggingForm = new FormGroup({
              'date': new FormControl(''),
              'idPlantation': new FormControl('')
            });
          }
        }
      );
  }

  onClose() {
    this.showDigging = false;
    this.diggingForm = new FormGroup({
      'date': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }
}
