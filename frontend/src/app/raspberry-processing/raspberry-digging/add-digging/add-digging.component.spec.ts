import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDiggingComponent } from './add-digging.component';

describe('AddDiggingComponent', () => {
  let component: AddDiggingComponent;
  let fixture: ComponentFixture<AddDiggingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDiggingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDiggingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
