import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryDigging } from 'src/app/models/raspberrydigging';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { Worker } from 'src/app/models/worker';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryDiggingService } from 'src/app/services/raspberry-digging.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-add-digging',
  templateUrl: './add-digging.component.html',
  styleUrls: ['./add-digging.component.css']
})
export class AddDiggingComponent implements OnInit {

  diggingForm: FormGroup = new FormGroup({
    'idWorker': new FormControl(''),
    'idPlantation': new FormControl(''),
    'date': new FormControl(''),
    'startHour': new FormControl(''),
    'endHour': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  workers: Worker[] = [];
  idGrower: number;

  constructor(
    private plantationService: RaspberryPlantationService,
    private workerService: WorkerService,
    private diggingService: RaspberryDiggingService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    );
    this.workerService.findWorkers(this.idGrower).subscribe(
      data => {
        this.workers = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onAdd();
    }
  }

  onAdd(){
    this.plantationService.getPlantation(this.diggingForm.value['idPlantation']).subscribe(
      plantation => {
        this.workerService.getById(this.diggingForm.value['idWorker']).subscribe(
          worker => {
            const digging = new RaspberryDigging(
              new Date(this.diggingForm.value['date']),
              this.diggingForm.value['startHour'],
              this.diggingForm.value['endHour'],
              plantation,
              worker
            );
            this.diggingService.addDigging(digging).subscribe(
              data => {
                window.location.reload();
              }
            );
          }
        )
      }
    )
  }
}
