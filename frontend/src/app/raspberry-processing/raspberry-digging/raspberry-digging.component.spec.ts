import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberryDiggingComponent } from './raspberry-digging.component';

describe('RaspberryDiggingComponent', () => {
  let component: RaspberryDiggingComponent;
  let fixture: ComponentFixture<RaspberryDiggingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberryDiggingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberryDiggingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
