import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberryPesticidesComponent } from './raspberry-pesticides.component';

describe('RaspberryPesticidesComponent', () => {
  let component: RaspberryPesticidesComponent;
  let fixture: ComponentFixture<RaspberryPesticidesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberryPesticidesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberryPesticidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
