import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { RaspberryPesticidesService } from 'src/app/services/raspberry-pesticides.service';

@Component({
  selector: 'app-update-pesticides',
  templateUrl: './update-pesticides.component.html',
  styleUrls: ['./update-pesticides.component.css']
})
export class UpdatePesticidesComponent implements OnInit {

  pesticideForm: FormGroup = new FormGroup({
    'kindOfPesticide': new FormControl(''),
    'description': new FormControl(''),
    'unitOfMeasurementOfQuantity': new FormControl(''),
    'quantityInStore': new FormControl('')
  });
  id: number;
  isAdmin: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private pesticidesService: RaspberryPesticidesService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.pesticidesService.getPesticide(this.id).subscribe(
      data => {
        this.pesticideForm = new FormGroup({
          'kindOfPesticide': new FormControl(data.kindOfPesticide),
          'description': new FormControl(data.description),
          'unitOfMeasurementOfQuantity': new FormControl(data.unitOfMeasurementOfQuantity),
          'quantityInStore': new FormControl(data.quantityInStore)
        });
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.update();
    }
  }

  update() {
    this.pesticidesService.updatePesticide(this.id, this.pesticideForm.value).subscribe(
      data => {
        this.router.navigate(['/pesticides']);
      }
    )
  }
}
