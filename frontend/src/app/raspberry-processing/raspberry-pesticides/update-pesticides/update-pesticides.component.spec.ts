import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePesticidesComponent } from './update-pesticides.component';

describe('UpdatePesticidesComponent', () => {
  let component: UpdatePesticidesComponent;
  let fixture: ComponentFixture<UpdatePesticidesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatePesticidesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePesticidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
