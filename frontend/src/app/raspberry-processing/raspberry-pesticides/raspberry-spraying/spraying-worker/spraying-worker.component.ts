import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { RaspberrySprayingWorker } from "src/app/models/raspberry-spraying-worker";
import { RaspberrySprayingWorkerService } from "src/app/services/raspberry-spraying-worker.service";
import { RaspberrySprayingService } from "src/app/services/raspberry-spraying.service";

@Component({
    selector: 'app-spraying-worker',
    templateUrl: './spraying-worker.component.html',
    styleUrls: ['./spraying-worker.component.css'],
})
export class SprayingWorkerComponent implements OnInit{

    id: number;
    sprayingWorkers: RaspberrySprayingWorker[] = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sprayingService: RaspberrySprayingService,
        private sprayingWorkerService: RaspberrySprayingWorkerService
    ){}

    ngOnInit(){
        this.id = this.route.snapshot.params['id'];
        this.sprayingWorkerService.findBySpraying(this.id).subscribe(
            data => {
                this.sprayingWorkers = data;
            }
        )
    }

    onAdd(){
        this.sprayingService.getById(this.id).subscribe(
            data => {
                this.router.navigate(['/addsprayingworker'],
                {queryParams: {
                    date: data.dateOfSpraying,
                    idPlantation: data.plantation.id,
                    idPesticide: data.pesticide.id
                }})
            }
        )
    }
}