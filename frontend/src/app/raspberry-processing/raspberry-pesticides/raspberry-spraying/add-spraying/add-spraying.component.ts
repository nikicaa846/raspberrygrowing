import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RaspberryPesticides } from 'src/app/models/raspberry-pesticides';
import { RaspberrySpraying } from 'src/app/models/raspberry-spraying';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPesticidesService } from 'src/app/services/raspberry-pesticides.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RaspberrySprayingService } from 'src/app/services/raspberry-spraying.service';


@Component({
  selector: 'app-add-spraying',
  templateUrl: './add-spraying.component.html',
  styleUrls: ['./add-spraying.component.css']
})
export class AddSprayingComponent implements OnInit {

  sprayingForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'quantityOfPesticide': new FormControl(''),
    'idPlantation': new FormControl(''),
    'idPesticide': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  pesticides: RaspberryPesticides[] = [];
  idGrower: number;

  constructor(
    private plantationService: RaspberryPlantationService,
    private pesticidesService: RaspberryPesticidesService,
    private sprayingService: RaspberrySprayingService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.pesticidesService.findPesticides(this.idGrower).subscribe(
      data => {
        this.pesticides = data;
      }
    );
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    );
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.plantationService.getPlantation(this.sprayingForm.value['idPlantation']).subscribe(
      plantation => {
        this.pesticidesService.getPesticide(this.sprayingForm.value['idPesticide']).subscribe(
          pesticide => {
            var quantity = this.sprayingForm.value['quantityOfPesticide'];
            if (pesticide.quantityInStore === 0) {
              window.alert("No pesticide in store.");
            } else if (pesticide.quantityInStore < quantity) {
              window.alert("No enough pesticide in store");
            } else {
              const spraying = new RaspberrySpraying(
                new Date(this.sprayingForm.value['date']),
                this.sprayingForm.value['quantityOfPesticide'],
                pesticide,
                plantation
              );
              this.sprayingService.addSpraying(spraying).subscribe(
                data => {
                  this.router.navigate(['/addsprayingworker'],
                    {
                      queryParams: {
                        date: this.sprayingForm.value['date'],
                        idPlantation: this.sprayingForm.value['idPlantation'],
                        idPesticide: this.sprayingForm.value['idPesticide']
                      }
                    });
                }
              );
            }
          }
        )
      }
    )
  }
}
