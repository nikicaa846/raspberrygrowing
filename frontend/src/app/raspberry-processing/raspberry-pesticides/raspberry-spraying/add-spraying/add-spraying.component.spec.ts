import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSprayingComponent } from './add-spraying.component';

describe('AddSprayingComponent', () => {
  let component: AddSprayingComponent;
  let fixture: ComponentFixture<AddSprayingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSprayingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSprayingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
