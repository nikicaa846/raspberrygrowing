import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberrySpraying } from 'src/app/models/raspberry-spraying';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RaspberrySprayingService } from 'src/app/services/raspberry-spraying.service';

@Component({
  selector: 'app-raspberry-spraying',
  templateUrl: './raspberry-spraying.component.html',
  styleUrls: ['./raspberry-spraying.component.css']
})
export class RaspberrySprayingComponent implements OnInit {

  sprayingForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  sprayings: RaspberrySpraying[] = [];
  idGrower: number;
  showSprayings = false;
  isAdmin: boolean;

  constructor(
    private authService: AuthService,
    private plantationService: RaspberryPlantationService,
    private sprayingService: RaspberrySprayingService
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  onClose() {
    this.showSprayings = false;
    this.sprayingForm = new FormGroup({
      'date': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }

  onFind() {
    this.sprayingService.findSprayingByDateAndPlantation(this.sprayingForm.value['date'], this.sprayingForm.value['idPlantation'])
      .subscribe(
        data => {
          this.sprayings = data;
          this.showSprayings = true;
        }, error => {
          if (error.status === 404) {
            this.showSprayings = false;
            window.alert("No sprayings with this date!");
            this.sprayingForm = new FormGroup({
              'date': new FormControl(''),
              'idPlantation': new FormControl('')
            });
          }
        }
      )
  }

}
