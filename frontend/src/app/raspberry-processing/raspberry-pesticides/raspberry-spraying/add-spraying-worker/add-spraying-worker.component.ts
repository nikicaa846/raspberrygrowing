import { Component, HostListener, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { RaspberrySprayingWorker } from "src/app/models/raspberry-spraying-worker";
import { Worker } from "src/app/models/worker";
import { AuthService } from "src/app/services/auth.service";
import { RaspberrySprayingWorkerService } from "src/app/services/raspberry-spraying-worker.service";
import { RaspberrySprayingService } from "src/app/services/raspberry-spraying.service";
import { WorkerService } from "src/app/services/worker.service";

@Component({
    selector: 'app-add-spraying-worker',
    templateUrl: './add-spraying-worker.component.html',
    styleUrls: ['./add-spraying-worker.component.css']
})
export class AddSprayingWorkerComponent implements OnInit {

    sprayingWorkerForm: FormGroup = new FormGroup({
        'startHour': new FormControl(''),
        'endHour': new FormControl(''),
        'idWorker': new FormControl(''),
    });
    workers: Worker[] = [];
    idGrower: number;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private workerService: WorkerService,
        private sprayingService: RaspberrySprayingService,
        private sprayingWorkerService: RaspberrySprayingWorkerService,
        private authService: AuthService
    ) { }

    ngOnInit() {
        this.idGrower = + this.authService.getGrower();
        this.workerService.findWorkers(this.idGrower).subscribe(
            data => {
                this.workers = data;
            }
        )
    }

    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        if (event.key == "+") {
            this.onAdd();
        } else if (event.key == "Enter") {
            this.onFinish();
        }
    }

    onAdd() {
        const param = this.route.snapshot.queryParamMap;
        this.workerService.getById(this.sprayingWorkerForm.value['idWorker']).subscribe(
            worker => {
                this.sprayingService.findSprayingByDatePlantationPesticide(param.get('date'), + param.get('idPlantation'), +param.get('idPesticide')).subscribe(
                    spraying => {
                        const sprayingWorker = new RaspberrySprayingWorker(
                            this.sprayingWorkerForm.value['startHour'],
                            this.sprayingWorkerForm.value['endHour'],
                            worker,
                            spraying
                        );
                        this.sprayingWorkerService.addSprayingWorker(sprayingWorker).subscribe(
                            data => {
                                this.sprayingWorkerForm = new FormGroup({
                                    'startHour': new FormControl(''),
                                    'endHour': new FormControl(''),
                                    'idWorker': new FormControl(''),
                                });
                            }
                        )
                    }
                );
            }
        );
    }

    onFinish() {
        this.router.navigate(['/spraying']);
    }
}