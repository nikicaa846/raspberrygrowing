import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberrySprayingComponent } from './raspberry-spraying.component';

describe('RaspberrySprayingComponent', () => {
  let component: RaspberrySprayingComponent;
  let fixture: ComponentFixture<RaspberrySprayingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberrySprayingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberrySprayingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
