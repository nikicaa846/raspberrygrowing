import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPesticidesComponent } from './add-pesticides.component';

describe('AddPesticidesComponent', () => {
  let component: AddPesticidesComponent;
  let fixture: ComponentFixture<AddPesticidesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPesticidesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPesticidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
