import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RaspberryPesticides } from 'src/app/models/raspberry-pesticides';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPesticidesService } from 'src/app/services/raspberry-pesticides.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-add-pesticides',
  templateUrl: './add-pesticides.component.html',
  styleUrls: ['./add-pesticides.component.css']
})
export class AddPesticidesComponent implements OnInit {

  pesticideForm: FormGroup = new FormGroup({
    'kindOfPesticide': new FormControl(''),
    'description': new FormControl(''),
    'unitOfMeasurementOfQuantity': new FormControl(''),
    'quantityInStore': new FormControl(''),
    'idGrower': new FormControl('')
  });
  idGrower: number;

  constructor(
    private pesticidesService: RaspberryPesticidesService,
    private router: Router,
    private authService: AuthService,
    private growerService: RaspberryGrowerService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    if ( this.idGrower !== 0 ){
      this.pesticideForm = new FormGroup({
        'kindOfPesticide': new FormControl(''),
        'description': new FormControl(''),
        'unitOfMeasurementOfQuantity': new FormControl(''),
        'quantityInStore': new FormControl(''),
        'idGrower': new FormControl(this.idGrower)
      });
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.growerService.getById(this.pesticideForm.value['idGrower']).subscribe(
      grower => {
        const pesticide = new RaspberryPesticides(
          this.pesticideForm.value['kindOfPesticide'],
          this.pesticideForm.value['description'],
          this.pesticideForm.value['unitOfMeasurementOfQuantity'],
          this.pesticideForm.value['quantityInStore'],
          grower
        );
        this.pesticidesService.addPesticide(pesticide).subscribe(
          data => {
            this.router.navigate(['/pesticides']);
          }
        )
      }
    );
  }
}
