import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RaspberryPesticides } from 'src/app/models/raspberry-pesticides';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPesticidesService } from 'src/app/services/raspberry-pesticides.service';

@Component({
  selector: 'app-raspberry-pesticides',
  templateUrl: './raspberry-pesticides.component.html',
  styleUrls: ['./raspberry-pesticides.component.css']
})
export class RaspberryPesticidesComponent implements OnInit {

  isAdmin: boolean;
  pesticides: RaspberryPesticides[] = [];
  idGrower: number;

  constructor(
    private authService: AuthService,
    private router: Router,
    private pesticidesService: RaspberryPesticidesService
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.idGrower = + this.authService.getGrower();
    this.pesticidesService.findPesticides(this.idGrower).subscribe(
      data => {
        this.pesticides = data;
      }
    )
  }

  onUpdate(id: number) {
    this.router.navigate(['updatepesticide/', id]);
  }

  onDelete(id: number) {
    this.pesticidesService.deletePesticide(id).subscribe(
      data => {
        window.location.reload();
      }
    )
  }
}
