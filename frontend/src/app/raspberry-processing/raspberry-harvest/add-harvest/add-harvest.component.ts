import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryHarvest } from 'src/app/models/raspberryharvest';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { Worker } from 'src/app/models/worker';
import { AuthService } from 'src/app/services/auth.service';
import { RasbperryHarvestService } from 'src/app/services/raspberry-harvest.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-add-harvest',
  templateUrl: './add-harvest.component.html',
  styleUrls: ['./add-harvest.component.css']
})
export class AddHarvestComponent implements OnInit {

  harvestForm: FormGroup = new FormGroup({
    'idWorker': new FormControl(''),
    'idPlantation': new FormControl(''),
    'date': new FormControl(''),
    'startHour': new FormControl(''),
    'endHour': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  workers: Worker[] = [];
  idGrower: number;

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onAdd();
    }
  }

  constructor(
    private workerService: WorkerService,
    private plantationService: RaspberryPlantationService,
    private harvestService: RasbperryHarvestService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    );
    this.workerService.findWorkers(this.idGrower).subscribe(
      data => {
        this.workers = data;
      }
    )
  }

  onAdd(){
    this.plantationService.getPlantation(this.harvestForm.value['idPlantation']).subscribe(
      plantation => {
        this.workerService.getById(this.harvestForm.value['idWorker']).subscribe(
          worker => {
            const harvest = new RaspberryHarvest(
              new Date(this.harvestForm.value['date']),
              this.harvestForm.value['startHour'],
              this.harvestForm.value['endHour'],
              plantation,
              worker
            );
            this.harvestService.addHarvest(harvest).subscribe(
              data => {
                window.location.reload();
              }
            )
          }
        )
      }
    );
  }
}
