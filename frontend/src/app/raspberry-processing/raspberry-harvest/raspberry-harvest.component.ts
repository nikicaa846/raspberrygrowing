import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryHarvest } from 'src/app/models/raspberryharvest';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { RasbperryHarvestService } from 'src/app/services/raspberry-harvest.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';

@Component({
  selector: 'app-raspberry-harvest',
  templateUrl: './raspberry-harvest.component.html',
  styleUrls: ['./raspberry-harvest.component.css']
})
export class RaspberryHarvestComponent implements OnInit {

  harvestForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  harvestes: RaspberryHarvest[] = [];
  showHarvest = false;
  grower: number;
  isAdmin: boolean;

  constructor(
    private authService: AuthService,
    private plantationService: RaspberryPlantationService,
    private harvestService: RasbperryHarvestService
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.grower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.grower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onFind();
    }
  }

  onFind(){
    this.harvestService.findHarvestByDateAndPlantation(this.harvestForm.value['date'], this.harvestForm.value['idPlantation']).subscribe(
      data => {
        this.harvestes = data;
        this.showHarvest = true;
      }, error => {
        if ( error.status === 404 ){
          this.showHarvest = false;
          window.alert("No harvest with this date");
          this.harvestForm = new FormGroup({
            'date': new FormControl(''),
            'idPlantation': new FormControl('')
          });
        }
      }
    )
  }

  onClose(){
    this.showHarvest = false;
    this.harvestForm = new FormGroup({
      'date': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }
}
