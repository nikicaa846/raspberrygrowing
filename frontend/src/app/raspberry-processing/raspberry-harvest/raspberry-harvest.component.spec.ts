import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberryHarvestComponent } from './raspberry-harvest.component';

describe('RaspberryHarvestComponent', () => {
  let component: RaspberryHarvestComponent;
  let fixture: ComponentFixture<RaspberryHarvestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberryHarvestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberryHarvestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
