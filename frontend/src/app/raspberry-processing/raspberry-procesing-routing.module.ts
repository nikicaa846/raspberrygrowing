import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGaurdService } from '../services/authguard.service';
import { AddDiggingComponent } from './raspberry-digging/add-digging/add-digging.component';
import { RaspberryDiggingComponent } from './raspberry-digging/raspberry-digging.component';
import { AddFertilizerComponent } from './raspberry-fertilizer/add-fertilizer/add-fertilizer.component';
import { AddFertilizationWorkerComponent } from './raspberry-fertilizer/raspberry-fertilization/add-fertilization-worker/add-fertilization-worker.component';
import { AddFertilizationComponent } from './raspberry-fertilizer/raspberry-fertilization/add-fertilization/add-fertilization.component';
import { FertiloizationWorkerComponent } from './raspberry-fertilizer/raspberry-fertilization/fertilization-worker/fertilization-worker.component';
import { RaspberryFertilizationComponent } from './raspberry-fertilizer/raspberry-fertilization/raspberry-fertilization.component';
import { RaspberryFertilizerComponent } from './raspberry-fertilizer/raspberry-fertilizer.component';
import { UpdateFertilizerComponent } from './raspberry-fertilizer/update-fertilizer/update-fertilizer.component';
import { AddHaircutComponent } from './raspberry-haircut/add-haircut/add-haircut.component';
import { RaspberryHaircutComponent } from './raspberry-haircut/raspberry-haircut.component';
import { AddHarvestComponent } from './raspberry-harvest/add-harvest/add-harvest.component';
import { RaspberryHarvestComponent } from './raspberry-harvest/raspberry-harvest.component';
import { AddPesticidesComponent } from './raspberry-pesticides/add-pesticides/add-pesticides.component';
import { RaspberryPesticidesComponent } from './raspberry-pesticides/raspberry-pesticides.component';
import { AddSprayingWorkerComponent } from './raspberry-pesticides/raspberry-spraying/add-spraying-worker/add-spraying-worker.component';
import { AddSprayingComponent } from './raspberry-pesticides/raspberry-spraying/add-spraying/add-spraying.component';
import { RaspberrySprayingComponent } from './raspberry-pesticides/raspberry-spraying/raspberry-spraying.component';
import { SprayingWorkerComponent } from './raspberry-pesticides/raspberry-spraying/spraying-worker/spraying-worker.component';
import { UpdatePesticidesComponent } from './raspberry-pesticides/update-pesticides/update-pesticides.component';
import { RaspberryProcessingComponent } from './raspberry-processing.component';

const routes: Routes = [
    { path: '', component: RaspberryProcessingComponent, canActivate:[AuthGaurdService], children:[
        { path: 'harvest', component: RaspberryHarvestComponent},
        { path: 'digging', component: RaspberryDiggingComponent},
        { path: 'addharvest', component: AddHarvestComponent},
        { path: 'adddigging', component: AddDiggingComponent},
        { path: 'haircut', component: RaspberryHaircutComponent},
        { path: 'addhaircut', component: AddHaircutComponent},
        { path: 'fertilizer', component: RaspberryFertilizerComponent},
        { path: 'addfertilizer', component: AddFertilizerComponent},
        { path: 'updatefertilizer/:id', component: UpdateFertilizerComponent},
        { path: 'fertilization', component: RaspberryFertilizationComponent},
        { path: 'addfertilization', component: AddFertilizationComponent},
        { path: 'addfertilizationworker', component: AddFertilizationWorkerComponent},
        { path: 'fertilizationworker/:id', component: FertiloizationWorkerComponent},
        { path: 'pesticides', component: RaspberryPesticidesComponent},
        { path: 'addpesticide', component: AddPesticidesComponent},
        { path: 'updatepesticide/:id', component: UpdatePesticidesComponent},
        { path: 'spraying', component: RaspberrySprayingComponent},
        { path: 'addspraying', component: AddSprayingComponent},
        { path: 'addsprayingworker', component: AddSprayingWorkerComponent},
        { path: 'sprayingworker/:id', component: SprayingWorkerComponent}
    ]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RaspberryProcessingRoutingModule{}