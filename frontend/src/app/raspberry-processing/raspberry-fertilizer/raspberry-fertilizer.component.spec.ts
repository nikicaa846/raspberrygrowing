import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberryFertilizerComponent } from './raspberry-fertilizer.component';

describe('RaspberryFertilizerComponent', () => {
  let component: RaspberryFertilizerComponent;
  let fixture: ComponentFixture<RaspberryFertilizerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberryFertilizerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberryFertilizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
