import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { RaspberryFertilizerService } from 'src/app/services/raspberry-fertilizer.service';

@Component({
  selector: 'app-update-fertilizer',
  templateUrl: './update-fertilizer.component.html',
  styleUrls: ['./update-fertilizer.component.css']
})
export class UpdateFertilizerComponent implements OnInit {

  fertilizerForm: FormGroup = new FormGroup({
    'kindOfFertilizer': new FormControl(''),
    'description': new FormControl(''),
    'unitOfMeasurementOfQuantity': new FormControl(''),
    'quantityInStore': new FormControl('')
  });
  id: number;
  isAdmin: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fertilizerService: RaspberryFertilizerService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.fertilizerService.getFertilizer(this.id).subscribe(
      data => {
        this.fertilizerForm = new FormGroup({
          'kindOfFertilizer': new FormControl(data.kindOfFertilizer),
          'description': new FormControl(data.description),
          'unitOfMeasurementOfQuantity': new FormControl(data.unitOfMeasurementOfQuantity),
          'quantityInStore': new FormControl(data.quantityInStore)
        });
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.update();
    }
  }

  update() {
    this.fertilizerService.updateFertilizer(this.id, this.fertilizerForm.value).subscribe(
      data => {
        this.router.navigate(['/fertilizer']);
      }
    )
  }
}
