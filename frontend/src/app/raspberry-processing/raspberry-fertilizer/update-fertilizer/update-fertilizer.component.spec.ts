import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFertilizerComponent } from './update-fertilizer.component';

describe('UpdateFertilizerComponent', () => {
  let component: UpdateFertilizerComponent;
  let fixture: ComponentFixture<UpdateFertilizerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateFertilizerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFertilizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
