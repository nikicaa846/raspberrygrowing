import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { RaspberryFertilizationWorker } from "src/app/models/raspberry-fertilization-worker";
import { RaspberryFertilizationWorkerService } from "src/app/services/raspberry-fertilization-worker.service";
import { RaspberryFertilizationService } from "src/app/services/raspberry-fertilization.service";

@Component({
    selector: 'app-fertilization-worker',
    templateUrl: './fertilization-worker.component.html',
    styleUrls: ['./fertilization-worker.component.css'],
  })
export class FertiloizationWorkerComponent implements OnInit{

  fertilizationWorkers: RaspberryFertilizationWorker[] = [];
  id: number;

  constructor(
    private fertilizationWorkerService: RaspberryFertilizationWorkerService,
    private fertilizationService: RaspberryFertilizationService,
    private route: ActivatedRoute,
    private router: Router
  ){}

  ngOnInit(){
    this.id = this.route.snapshot.params['id'];
    this.fertilizationWorkerService.findByFertilization(this.id).subscribe(
      data => {
        this.fertilizationWorkers = data;
      }
    )
  }

  onAdd(){
    this.fertilizationService.getById(this.id).subscribe(
      data => {
        this.router.navigate(['/addfertilizationworker'], 
        {queryParams: { 
          date: data.dateOfFertilization,
          plantation: data.plantation.id,
          fertilizer: data.fertilizer.id
        } });
      }
    );

  }
}