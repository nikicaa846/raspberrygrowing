import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberryFertilizationComponent } from './raspberry-fertilization.component';

describe('RaspberryFertilizationComponent', () => {
  let component: RaspberryFertilizationComponent;
  let fixture: ComponentFixture<RaspberryFertilizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberryFertilizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberryFertilizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
