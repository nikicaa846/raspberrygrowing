import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryFertilization } from 'src/app/models/raspberry-fertilization';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryFertilizationService } from 'src/app/services/raspberry-fertilization.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';

@Component({
  selector: 'app-raspberry-fertilization',
  templateUrl: './raspberry-fertilization.component.html',
  styleUrls: ['./raspberry-fertilization.component.css']
})
export class RaspberryFertilizationComponent implements OnInit {

  fertilizationForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  fertilizations: RaspberryFertilization[] = [];
  idGrower: number;
  showFertilization = false;
  isAdmin: boolean;

  constructor(
    private authService: AuthService,
    private plantationsService: RaspberryPlantationService,
    private fertilizationService: RaspberryFertilizationService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.isAdmin = this.authService.isAdmin();
    this.plantationsService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    );
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  onFind() {
    this.fertilizationService.findFertilizationByDateAndPlantation(this.fertilizationForm.value['date'], this.fertilizationForm.value['idPlantation'])
      .subscribe(
        data => {
          this.fertilizations = data;
          this.showFertilization = true;
        }, error => {
          if (error.status === 404) {
            window.alert("No fertilization with this date!");
            this.fertilizationForm = new FormGroup({
              'date': new FormControl(''),
              'idPlantation': new FormControl('')
            });
          }
        }
      )
  }

  onClose() {
    this.showFertilization = false;
    this.fertilizationForm = new FormGroup({
      'date': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }
}
