import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { RaspberryFertilizationWorker } from 'src/app/models/raspberry-fertilization-worker';
import { Worker } from 'src/app/models/worker';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryFertilizationWorkerService } from 'src/app/services/raspberry-fertilization-worker.service';
import { RaspberryFertilizationService } from 'src/app/services/raspberry-fertilization.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-add-fertilization-worker',
  templateUrl: './add-fertilization-worker.component.html',
  styleUrls: ['./add-fertilization-worker.component.css']
})
export class AddFertilizationWorkerComponent implements OnInit {

  fertilizationWorkerForm: FormGroup = new FormGroup({
    'startHour': new FormControl(''),
    'endHour': new FormControl(''),
    'idWorker': new FormControl(''),
  });
  workers: Worker[] = [];
  idGrower: number;

  constructor(
    private workerService: WorkerService,
    private router: Router,
    private fertilizationWorkerService: RaspberryFertilizationWorkerService,
    private fertilizationService: RaspberryFertilizationService,
    private route: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.workerService.findWorkers(this.idGrower).subscribe(
      data => {
        this.workers = data;
      }
    )
  }

  onFinish() {
    this.router.navigate(['/fertilization']);
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "+"){
      this.onAdd();
    } else if ( event.key == "Enter" ){
        this.onFinish();
    }
  }

  onAdd() {
    const param = this.route.snapshot.queryParamMap;
    this.workerService.getById(this.fertilizationWorkerForm.value['idWorker']).subscribe(
      worker => {
        this.fertilizationService.findFertilizationByDateAndPlantationAndFertilizer(param.get('date'), + param.get('idPlantation'), +param.get('idFertilizer')).subscribe(
          fertilization => {
            const fertilizationWorker = new RaspberryFertilizationWorker(
              this.fertilizationWorkerForm.value['startHour'],
              this.fertilizationWorkerForm.value['endHour'],
              fertilization,
              worker,
            );
            this.fertilizationWorkerService.addFertilizationWorker(fertilizationWorker).subscribe(
              data => {
                this.fertilizationWorkerForm = new FormGroup({
                  'startHour': new FormControl(''),
                  'endHour': new FormControl(''),
                  'idWorker': new FormControl(''),
                });
              }
            )
          }
        )
      }
    )
  }
}
