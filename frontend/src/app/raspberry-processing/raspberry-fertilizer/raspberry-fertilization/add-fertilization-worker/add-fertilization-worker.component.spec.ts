import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFertilizationWorkerComponent } from './add-fertilization-worker.component';

describe('AddFertilizationWorkerComponent', () => {
  let component: AddFertilizationWorkerComponent;
  let fixture: ComponentFixture<AddFertilizationWorkerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFertilizationWorkerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFertilizationWorkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
