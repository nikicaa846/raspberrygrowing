import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryFertilization } from 'src/app/models/raspberry-fertilization';
import { RaspberryFertilizer } from 'src/app/models/raspberry-fertilizer';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { RaspberryFertilizerService } from 'src/app/services/raspberry-fertilizer.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RaspberryFertilizationService } from 'src/app/services/raspberry-fertilization.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-add-fertilization',
  templateUrl: './add-fertilization.component.html',
  styleUrls: ['./add-fertilization.component.css']
})
export class AddFertilizationComponent implements OnInit {

  fertilizationForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'quantityOfFertilizer': new FormControl(''),
    'idPlantation': new FormControl(''),
    'idFertilizer': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  fertilizers: RaspberryFertilizer[] = [];
  idGrower: number;

  constructor(
    private authService: AuthService,
    private plantationService: RaspberryPlantationService,
    private fertilizerService: RaspberryFertilizerService,
    private fertilizationService: RaspberryFertilizationService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    );
    this.fertilizerService.findFertilizers(this.idGrower).subscribe(
      data => {
        this.fertilizers = data;
      }
    );
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.plantationService.getPlantation(this.fertilizationForm.value['idPlantation']).subscribe(
      plantation => {
        this.fertilizerService.getFertilizer(this.fertilizationForm.value['idFertilizer']).subscribe(
          fertilizer => {
            var quantity = this.fertilizationForm.value['quantityOfFertilizer'];
            if (fertilizer.quantityInStore === 0) {
              window.alert("No fertilizer in store!");
            } else if (fertilizer.quantityInStore < quantity) {
              window.alert("No enough fertilizer in store!");
            } else {
              const fertilization = new RaspberryFertilization(
                new Date(this.fertilizationForm.value['date']),
                this.fertilizationForm.value['quantityOfFertilizer'],
                plantation,
                fertilizer
              );
              this.fertilizationService.addFertilization(fertilization).subscribe(
                data => {
                  this.router.navigate(['/addfertilizationworker'],
                    {
                      queryParams: {
                        date: this.fertilizationForm.value['date'],
                        idPlantation: this.fertilizationForm.value['idPlantation'],
                        idFertilizer: this.fertilizationForm.value['idFertilizer']
                      }
                    });
                }
              );
            }
          }
        )
      }
    )
  }

}
