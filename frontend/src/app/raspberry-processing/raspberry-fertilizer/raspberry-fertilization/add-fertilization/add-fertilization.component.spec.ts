import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFertilizationComponent } from './add-fertilization.component';

describe('AddFertilizationComponent', () => {
  let component: AddFertilizationComponent;
  let fixture: ComponentFixture<AddFertilizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFertilizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFertilizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
