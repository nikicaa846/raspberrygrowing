import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RaspberryFertilizer } from 'src/app/models/raspberry-fertilizer';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryFertilizerService } from 'src/app/services/raspberry-fertilizer.service';

@Component({
  selector: 'app-raspberry-fertilizer',
  templateUrl: './raspberry-fertilizer.component.html',
  styleUrls: ['./raspberry-fertilizer.component.css']
})
export class RaspberryFertilizerComponent implements OnInit {

  isAdmin: boolean;
  fertilizers: RaspberryFertilizer[] = [];
  idGrower: number;

  constructor(
    private authService: AuthService,
    private fertilizerService: RaspberryFertilizerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.idGrower = + this.authService.getGrower();
    this.fertilizerService.findFertilizers(this.idGrower).subscribe(
      data => {
        this.fertilizers = data;
      }
    )
  }

  onUpdate(id: number) {
    this.router.navigate(['/updatefertilizer', id]);
  }

  onDelete(id: number) {
    this.fertilizerService.deleteFertilizer(id).subscribe(
      data => {
        window.location.reload();
      }
    )
  }
}
