import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RaspberryFertilizer } from 'src/app/models/raspberry-fertilizer';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryFertilizerService } from 'src/app/services/raspberry-fertilizer.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-add-fertilizer',
  templateUrl: './add-fertilizer.component.html',
  styleUrls: ['./add-fertilizer.component.css']
})
export class AddFertilizerComponent implements OnInit {

  fertilizerForm: FormGroup = new FormGroup({
    'kindOfFertilizer': new FormControl(''),
    'description': new FormControl(''),
    'unitOfMeasurementOfQuantity': new FormControl(''),
    'quantityInStore': new FormControl(''),
    'idGrower': new FormControl('')
  });
  idGrower: number;

  constructor(
    private authService: AuthService,
    private fertilizerService: RaspberryFertilizerService,
    private growerService: RaspberryGrowerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    if (this.idGrower !== 0) {
      this.fertilizerForm = new FormGroup({
        'kindOfFertilizer': new FormControl(''),
        'description': new FormControl(''),
        'unitOfMeasurementOfQuantity': new FormControl(''),
        'quantityInStore': new FormControl(''),
        'idGrower': new FormControl(this.idGrower)
      });
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.growerService.getById(this.fertilizerForm.value['idGrower']).subscribe(
      grower => {
        const fertilizer = new RaspberryFertilizer(
          this.fertilizerForm.value['kindOfFertilizer'],
          this.fertilizerForm.value['description'],
          this.fertilizerForm.value['unitOfMeasurementOfQuantity'],
          this.fertilizerForm.value['quantityInStore'],
          grower
        );
        this.fertilizerService.addFertilizer(fertilizer).subscribe(
          data => {
            this.router.navigate(['/fertilizer']);
          }
        );
      }
    );
  }
}
