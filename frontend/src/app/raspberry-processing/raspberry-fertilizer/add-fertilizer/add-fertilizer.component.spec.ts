import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFertilizerComponent } from './add-fertilizer.component';

describe('AddFertilizerComponent', () => {
  let component: AddFertilizerComponent;
  let fixture: ComponentFixture<AddFertilizerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFertilizerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFertilizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
