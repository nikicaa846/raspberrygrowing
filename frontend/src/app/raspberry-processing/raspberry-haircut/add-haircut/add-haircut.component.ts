import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryHaircut } from 'src/app/models/raspberryhaircut';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { Worker } from 'src/app/models/worker';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryHaircutService } from 'src/app/services/raspberry-haircut.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-add-haircut',
  templateUrl: './add-haircut.component.html',
  styleUrls: ['./add-haircut.component.css']
})
export class AddHaircutComponent implements OnInit {

  haircutForm: FormGroup = new FormGroup({
    'idWorker': new FormControl(''),
    'idPlantation': new FormControl(''),
    'date': new FormControl(''),
    'startHour': new FormControl(''),
    'endHour': new FormControl('')
  });
  workers: Worker[] = [];
  plantations: RaspberryPlantation[] = [];
  idGrower: number;

  constructor(
    private plantationService: RaspberryPlantationService,
    private workerService: WorkerService,
    private haircutService: RaspberryHaircutService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    );
    this.workerService.findWorkers(this.idGrower).subscribe(
      data => {
        this.workers = data;
      }
    );
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.plantationService.getPlantation(this.haircutForm.value['idPlantation']).subscribe(
      plantation => {
        this.workerService.getById(this.haircutForm.value['idWorker']).subscribe(
          worker => {
            const haircut = new RaspberryHaircut(
              new Date(this.haircutForm.value['date']),
              this.haircutForm.value['startHour'],
              this.haircutForm.value['endHour'],
              plantation,
              worker
            );
            this.haircutService.addHaircut(haircut).subscribe(
              data => {
                window.location.reload();
              }
            );
          }
        )
      }
    );
  }
}
