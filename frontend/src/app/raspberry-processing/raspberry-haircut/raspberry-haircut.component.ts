import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryHaircut } from 'src/app/models/raspberryhaircut';
import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryHaircutService } from 'src/app/services/raspberry-haircut.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';

@Component({
  selector: 'app-raspberry-haircut',
  templateUrl: './raspberry-haircut.component.html',
  styleUrls: ['./raspberry-haircut.component.css']
})
export class RaspberryHaircutComponent implements OnInit {

  haircutForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'idPlantation': new FormControl('')
  });
  plantations: RaspberryPlantation[] = [];
  haircuts: RaspberryHaircut[] = [];
  showHaircut = false;
  idGrower: number;
  isAdmin: boolean;

  constructor(
    private authService: AuthService,
    private plantationService: RaspberryPlantationService,
    private haircutService: RaspberryHaircutService
  ) { }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.idGrower = + this.authService.getGrower();
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  onFind() {
    this.haircutService.findHaircutByDateAndPlantation(this.haircutForm.value['date'], this.haircutForm.value['idPlantation'])
      .subscribe(
        data => {
          this.haircuts = data;
          this.showHaircut = true;
        }, error => {
          if (error.status === 404) {
            this.showHaircut = false;
            window.alert("No haircut with this date!");
            this.haircutForm = new FormGroup({
              'date': new FormControl(''),
              'idPlantation': new FormControl('')
            });
          }
        }
      )
  }

  onClose() {
    this.showHaircut = false;
    this.haircutForm = new FormGroup({
      'date': new FormControl(''),
      'idPlantation': new FormControl('')
    });
  }
}
