import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberryHaircutComponent } from './raspberry-haircut.component';

describe('RaspberryHaircutComponent', () => {
  let component: RaspberryHaircutComponent;
  let fixture: ComponentFixture<RaspberryHaircutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberryHaircutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberryHaircutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
