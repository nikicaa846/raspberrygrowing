import { NgModule } from "@angular/core";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RaspberryProcessingComponent } from "./raspberry-processing.component";
import { RaspberryHarvestComponent } from "./raspberry-harvest/raspberry-harvest.component";
import { RaspberryDiggingComponent } from "./raspberry-digging/raspberry-digging.component";
import { RaspberryProcessingRoutingModule } from "./raspberry-procesing-routing.module";
import { AddHarvestComponent } from './raspberry-harvest/add-harvest/add-harvest.component';
import { AddDiggingComponent } from './raspberry-digging/add-digging/add-digging.component';
import { RaspberryHaircutComponent } from './raspberry-haircut/raspberry-haircut.component';
import { AddHaircutComponent } from './raspberry-haircut/add-haircut/add-haircut.component';
import { RaspberryPesticidesComponent } from './raspberry-pesticides/raspberry-pesticides.component';
import { AddPesticidesComponent } from './raspberry-pesticides/add-pesticides/add-pesticides.component';
import { RaspberrySprayingComponent } from './raspberry-pesticides/raspberry-spraying/raspberry-spraying.component';
import { AddSprayingComponent } from './raspberry-pesticides/raspberry-spraying/add-spraying/add-spraying.component';
import { UpdatePesticidesComponent } from './raspberry-pesticides/update-pesticides/update-pesticides.component';
import { RaspberryFertilizerComponent } from './raspberry-fertilizer/raspberry-fertilizer.component';
import { AddFertilizerComponent } from './raspberry-fertilizer/add-fertilizer/add-fertilizer.component';
import { UpdateFertilizerComponent } from './raspberry-fertilizer/update-fertilizer/update-fertilizer.component';
import { RaspberryFertilizationComponent } from './raspberry-fertilizer/raspberry-fertilization/raspberry-fertilization.component';
import { AddFertilizationComponent } from './raspberry-fertilizer/raspberry-fertilization/add-fertilization/add-fertilization.component';
import { AddFertilizationWorkerComponent } from "./raspberry-fertilizer/raspberry-fertilization/add-fertilization-worker/add-fertilization-worker.component";
import { FertiloizationWorkerComponent } from "./raspberry-fertilizer/raspberry-fertilization/fertilization-worker/fertilization-worker.component";
import { AddSprayingWorkerComponent } from "./raspberry-pesticides/raspberry-spraying/add-spraying-worker/add-spraying-worker.component";
import { SprayingWorkerComponent } from "./raspberry-pesticides/raspberry-spraying/spraying-worker/spraying-worker.component";

@NgModule({
    declarations: [
        RaspberryProcessingComponent,
        RaspberryHarvestComponent,
        RaspberryDiggingComponent,
        AddHarvestComponent,
        AddDiggingComponent,
        RaspberryHaircutComponent,
        AddHaircutComponent,
        RaspberryPesticidesComponent,
        AddPesticidesComponent,
        RaspberrySprayingComponent,
        AddSprayingComponent,
        UpdatePesticidesComponent,
        RaspberryFertilizerComponent,
        AddFertilizerComponent,
        UpdateFertilizerComponent,
        RaspberryFertilizationComponent,
        AddFertilizationComponent,
        AddFertilizationWorkerComponent,
        FertiloizationWorkerComponent,
        AddSprayingWorkerComponent,
        SprayingWorkerComponent
    ], 
    imports: [
        BrowserAnimationsModule,
        RouterModule,
        NgbModule,
        ReactiveFormsModule,
        BsDropdownModule.forRoot(),
        CommonModule,
        RaspberryProcessingRoutingModule
    ]
})
export class RasbperryProcesingModule{}