import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberryProcessingComponent } from './raspberry-processing.component';

describe('RaspberryProcessingComponent', () => {
  let component: RaspberryProcessingComponent;
  let fixture: ComponentFixture<RaspberryProcessingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberryProcessingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberryProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
