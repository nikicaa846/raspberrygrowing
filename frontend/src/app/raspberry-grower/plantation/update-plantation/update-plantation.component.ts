import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-update-plantation',
  templateUrl: './update-plantation.component.html',
  styleUrls: ['./update-plantation.component.css']
})
export class UpdatePlantationComponent implements OnInit {

  plantationForm: FormGroup = new FormGroup({
    'size': new FormControl(''),
    'idGrower': new FormControl(''),
    'address': new FormControl('')
  });
  id: number;

  constructor(
    private plantationService: RaspberryPlantationService,
    private router: Router,
    private route: ActivatedRoute,
    private growerService: RaspberryGrowerService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.plantationService.getPlantation(this.id).subscribe(
      data => {
        this.plantationForm = new FormGroup({
          'size': new FormControl(data.size),
          'idGrower': new FormControl(data.raspberryGrower.id),
          'address': new FormControl(data.address)
        });
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onUpdate();
    }
  }

  onUpdate() {
    this.growerService.getById(this.plantationForm.value['idGrower']).subscribe(
      grower => {
        const plantation =
          new RaspberryPlantation(this.plantationForm.value['size'], this.plantationForm.value['address'], grower);
        this.plantationService.updatePlantation(this.id, plantation).subscribe(
          data => {
            this.router.navigate(['/plantation']);
          }
        )
      }
    )
  }

}
