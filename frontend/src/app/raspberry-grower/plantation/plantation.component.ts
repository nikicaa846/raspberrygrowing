import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';

@Component({
  selector: 'app-plantation',
  templateUrl: './plantation.component.html',
  styleUrls: ['./plantation.component.css'],
})
export class PlantationComponent implements OnInit {

  plantations: RaspberryPlantation[] = [];
  isAdmin: boolean;
  idGrower: number;

  constructor(
    private plantationService: RaspberryPlantationService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
    this.idGrower = + this.authService.getGrower();
    this.findPlantations();
  }

  private findPlantations() {
    this.plantationService.findPlantations(this.idGrower).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  onUpdate(id: number) {
    this.router.navigate(['/updateplantation', id]);
  }

  onDelete(id: number) {
    this.plantationService.deletePlantation(id).subscribe(
      data => {
        window.location.reload();
      }
    )
  }
}
