import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-add-plantation',
  templateUrl: './add-plantation.component.html',
  styleUrls: ['./add-plantation.component.css']
})
export class AddPlantationComponent implements OnInit {

  plantationForm: FormGroup = new FormGroup({
    'size': new FormControl(''),
    'idGrower': new FormControl(''),
    'address': new FormControl('')
  });

  constructor(
    private plantationService: RaspberryPlantationService,
    private router: Router,
    private growerService: RaspberryGrowerService
  ) { }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  ngOnInit(): void {
  }

  onAdd() {
    this.growerService.getById(this.plantationForm.value['idGrower']).subscribe(
      grower => {
        const plantation =
          new RaspberryPlantation(this.plantationForm.value['size'], this.plantationForm.value['address'], grower);
        this.plantationService.addPlantation(plantation).subscribe(
          data => {
            this.router.navigate(['/plantation']);
          }
        )
      }
    )

  }

}
