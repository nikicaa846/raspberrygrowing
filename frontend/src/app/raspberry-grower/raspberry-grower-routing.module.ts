import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGaurdService } from '../services/authguard.service';
import { AddGrowerComponent } from './grower/add-grower/add-grower.component';
import { GrowerComponent } from './grower/grower.component';
import { UpdateGrowerComponent } from './grower/update-grower/update-grower.component';
import { AddPlantationComponent } from './plantation/add-plantation/add-plantation.component';
import { PlantationComponent } from './plantation/plantation.component';
import { UpdatePlantationComponent } from './plantation/update-plantation/update-plantation.component';
import { RaspberryGrowerComponent } from './raspberry-grower.component';
import { GrowerDetailsComponent } from './grower/grower-details/grower-details.component';

const routes: Routes = [
    { path: '', component: RaspberryGrowerComponent,
     canActivate: [AuthGaurdService],
    children:[
        {path: 'raspberrygrower', component: GrowerComponent},
        {path: 'growerdetails/:id', component: GrowerDetailsComponent},
        {path: 'plantation', component: PlantationComponent},
        {path: 'addgrower', component: AddGrowerComponent},
        {path: 'updategrower/:id', component: UpdateGrowerComponent},
        {path: 'addplantation', component: AddPlantationComponent},
        {path: 'updateplantation/:id', component: UpdatePlantationComponent},
      ]}, 
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RaspberryGrowerRoutingModule{}