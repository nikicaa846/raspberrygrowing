import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RaspberryGrowerComponent } from './raspberry-grower.component';

describe('RaspberryGrowerComponent', () => {
  let component: RaspberryGrowerComponent;
  let fixture: ComponentFixture<RaspberryGrowerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RaspberryGrowerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RaspberryGrowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
