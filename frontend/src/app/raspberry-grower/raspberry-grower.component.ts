import { Component, OnInit } from '@angular/core';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-raspberry-grower',
  templateUrl: './raspberry-grower.component.html',
  styleUrls: ['./raspberry-grower.component.css']
})
export class RaspberryGrowerComponent implements OnInit {

  isAdmin: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
  }

}
