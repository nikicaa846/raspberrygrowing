import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGrowerComponent } from './update-grower.component';

describe('UpdateGrowerComponent', () => {
  let component: UpdateGrowerComponent;
  let fixture: ComponentFixture<UpdateGrowerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateGrowerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGrowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
