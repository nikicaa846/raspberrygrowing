import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-update-grower',
  templateUrl: './update-grower.component.html',
  styleUrls: ['./update-grower.component.css']
})
export class UpdateGrowerComponent implements OnInit {

  growerForm: FormGroup = new FormGroup({
    'id': new FormControl(''),
    'name': new FormControl(''),
    'surname': new FormControl(''),
    'phone': new FormControl(''),
    'address': new FormControl(''),
    'birthday': new FormControl('')
  });
  id: number;

  constructor(
    private growerService: RaspberryGrowerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.growerService.getById(this.id).subscribe(
      data => {
        this.growerForm = new FormGroup({
          'id': new FormControl(data.id),
          'name': new FormControl(data.name),
          'surname': new FormControl(data.surname),
          'phone': new FormControl(data.phone),
          'address': new FormControl(data.address),
          'birthday': new FormControl(data.birthday)
        });
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onUpdate();
    }
  }

  onUpdate() {
    this.growerService.updateGrower(this.id, this.growerForm.value).subscribe(
      data => {
        this.router.navigate(['/raspberrygrower']);
      }
    )
  }
}
