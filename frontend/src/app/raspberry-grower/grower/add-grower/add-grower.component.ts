import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-add-grower',
  templateUrl: './add-grower.component.html',
  styleUrls: ['./add-grower.component.css']
})
export class AddGrowerComponent implements OnInit {

  growerForm: FormGroup = new FormGroup({
    'id': new FormControl(''),
    'name': new FormControl(''),
    'surname': new FormControl(''),
    'address': new FormControl(''),
    'phone': new FormControl(''),
    'birthday': new FormControl('')
  });
  constructor(private growerService: RaspberryGrowerService, private router: Router) { }

  ngOnInit(): void {
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.growerService.addGrower(this.growerForm.value).subscribe(
      data => {
        this.router.navigate(['/raspberrygrower']);
      }
    )
  }
}
