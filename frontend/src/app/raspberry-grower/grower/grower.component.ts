import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RaspberryGrower } from 'src/app/models/raspberrygrower';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-grower',
  templateUrl: './grower.component.html',
  styleUrls: ['./grower.component.css']
})
export class GrowerComponent implements OnInit {

  growerForm: FormGroup = new FormGroup({
    'id': new FormControl('')
  });
  grower: RaspberryGrower = new RaspberryGrower();
  showGrower = false;
  isAdmin: boolean;

  constructor(
    private growerService: RaspberryGrowerService, 
    private router: Router,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if ( event.key == "Enter"){
      this.onFind();
    }
  }

  onFind(){
    this.growerService.getById(this.growerForm.value['id']).subscribe(
      data => {
        this.grower = data;
        this.showGrower = true;
      }, error => {
        if(error.status === 404 ){
          window.alert("Grower with jmbg you input doesn't exist!");
          this.showGrower = false;
          this.growerForm = new FormGroup({
            'id': new FormControl('')
          });
        }
      }
    )
  }

  onUpdate(id: number){
    this.router.navigate(['/updategrower', id]);
  }

  onDelete(id: number){
    this.growerService.deleteGrower(id).subscribe(
      data => {
        this.showGrower = false;
        this.growerForm = new FormGroup({
          'id': new FormControl('')
        });
      }
    )
  }

  onClose(){
    this.showGrower = false;
    this.growerForm = new FormGroup({
      'id': new FormControl('')
    });
  }

  onDetails(id: number){
    this.router.navigate(['/growerdetails', id]);
  }
}
