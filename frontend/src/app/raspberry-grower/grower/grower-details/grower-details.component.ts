import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { RaspberryPlantation } from 'src/app/models/raspberryplantation';
import { InvestmentInRaspberryPesticideService } from 'src/app/services/investment-in-raspberry-pesticide.service';
import { InvestmentInRaspberryFertilizerService } from 'src/app/services/investment-in-raspbrry-fertilizer.service';
import { PurchaseService } from 'src/app/services/purchase.service';
import { RaspberryInvestmentService } from 'src/app/services/raspberry-investment.service';
import { RaspberryPlantationService } from 'src/app/services/raspberry-plantation.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';
import { RefrigeratorPaymentService } from 'src/app/services/refrigerator-payment.service';
import { RefrigeratorRemovalService } from 'src/app/services/refrigerator-removal.service';
import { WorkerDefrayalService } from 'src/app/services/worker-defrayal.service';

@Component({
  selector: 'app-grower-details',
  templateUrl: './grower-details.component.html',
  styleUrls: ['./grower-details.component.css']
})
export class GrowerDetailsComponent implements OnInit {

  growerForm: FormGroup = new FormGroup({
    'year': new FormControl('')
  });
  id: number;
  plantations: RaspberryPlantation[] = [];
  showDetails = false;
  totalWeightOfMiker: number;
  totalWeightOfVilamet: number;
  totalEarnFromBuyer: number;
  totalPaymentOfRefrigerators: number;
  totalDefrayalForWorkers: number;
  totalInvestmentInPesticides: number;
  totalInvestmentInFertilizer: number;
  totalInvestmentInOther: number;
  profit: number;

  constructor(
    private route: ActivatedRoute,
    private plantationService: RaspberryPlantationService,
    private removalService: RefrigeratorRemovalService,
    private paymentService: RefrigeratorPaymentService,
    private purchaseService: PurchaseService,
    private defrayalService: WorkerDefrayalService,
    private investmentService: RaspberryInvestmentService,
    private investmentInFertilizerService: InvestmentInRaspberryFertilizerService,
    private investmentInPesticideService: InvestmentInRaspberryPesticideService,
    private growerService: RaspberryGrowerService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.plantationService.findPlantations(this.id).subscribe(
      data => {
        this.plantations = data;
      }
    )
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  getTotalInvestments() {
    this.investmentService.getTotalInvestmentForYear(this.growerForm.value['year'], this.id).subscribe(
      data => {
        this.totalInvestmentInOther = data;
      }
    );
    this.investmentInFertilizerService.getTotalInvestmentForYear(this.growerForm.value['year'], this.id).subscribe(
      data => {
        this.totalInvestmentInFertilizer = data;
      }
    );
    this.investmentInPesticideService.getTotalInvestmentForYear(this.growerForm.value['year'], this.id).subscribe(
      data => {
        this.totalInvestmentInPesticides = data;
      }
    )
  }

  getProfit() {
    this.growerService.getProfit(this.id, this.growerForm.value['year']).subscribe(
      data => {
        this.profit = data;
      }
    )
  }

  onFind() {
    this.getTotalInvestments();
    this.getProfit();
    this.showDetails = true;
    for (let i = 0; i < this.plantations.length; i++) {
      this.removalService.getTotalWeight(this.plantations[i].id, this.growerForm.value['year'], "MIKER").subscribe(
        data => {
          const totalWeightOfRemoval = data;
          this.purchaseService.getTotalPurchaseWeight
            (this.growerForm.value['year'], this.plantations[i].id, "MIKER").subscribe(
              data => {
                const totalWeightOfPurchase = data;
                this.totalWeightOfMiker = totalWeightOfRemoval + totalWeightOfPurchase;
              })
        }
      );
      this.removalService.getTotalWeight(this.plantations[i].id, this.growerForm.value['year'], "VILAMET").subscribe(
        data => {
          const totalWeightOfRemoval = data;
          this.purchaseService.getTotalPurchaseWeight
            (this.growerForm.value['year'], this.plantations[i].id, "VILAMET").subscribe(
              data => {
                const totalWeightOfPurchase = data;
                this.totalWeightOfVilamet = totalWeightOfRemoval + totalWeightOfPurchase;
              })
        }
      );
      this.purchaseService.getTotalEarnings(this.growerForm.value['year'], this.plantations[i].id).subscribe(
        data => {
          this.totalEarnFromBuyer = data;
        }
      );
      this.paymentService.getTotalPaymentForPlantation(this.plantations[i].id, this.growerForm.value['year']).subscribe(
        data => {
          this.totalPaymentOfRefrigerators = data;
        }
      );
      this.defrayalService.getTotalDefrayalForPlantation(this.plantations[i].id, this.growerForm.value['year']).subscribe(
        data => {
          this.totalDefrayalForWorkers = data;
        }
      );
    }
  }

  onClose() {
    this.showDetails = false;
    this.growerForm = new FormGroup({
      'year': new FormControl('')
    });
  }

}
