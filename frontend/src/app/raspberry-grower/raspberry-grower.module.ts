import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RaspberryGrowerComponent } from './raspberry-grower.component';
import { PlantationComponent } from './plantation/plantation.component';
import { AddGrowerComponent } from './grower/add-grower/add-grower.component';
import { UpdateGrowerComponent } from './grower/update-grower/update-grower.component';
import { AddPlantationComponent } from './plantation/add-plantation/add-plantation.component';
import { UpdatePlantationComponent } from './plantation/update-plantation/update-plantation.component';
import { GrowerComponent } from './grower/grower.component';
import { RaspberryGrowerRoutingModule } from './raspberry-grower-routing.module';
import { GrowerDetailsComponent } from './grower/grower-details/grower-details.component';



@NgModule({
    declarations:[
    RaspberryGrowerComponent,
    PlantationComponent,
    AddGrowerComponent,
    UpdateGrowerComponent,
    AddPlantationComponent,
    UpdatePlantationComponent,
    GrowerComponent,
    GrowerDetailsComponent
    ],
    imports:[
        BrowserAnimationsModule,
        RouterModule,
        NgbModule,
        ReactiveFormsModule,
        BsDropdownModule.forRoot(),
        CommonModule,
        RaspberryGrowerRoutingModule
    ]
})
export class RaspberryGrowerModule {}