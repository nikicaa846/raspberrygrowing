import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddUserComponent } from './add-user/add-user.component';
import { AuthComponent } from './auth.component';
import { FindUserComponent } from './find-user/find-user.component';
import { LoginComponent } from './login/login.component';
import { UpdateUserComponent } from './update-user/update-user.component';

const routes: Routes = [
    { path: '', component: AuthComponent, children:[
        { path: 'login', component: LoginComponent},
        { path: 'users', component: FindUserComponent},
        { path: 'adduser', component: AddUserComponent},
        { path: 'updateuser/:id', component: UpdateUserComponent}
      ]},
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule{}