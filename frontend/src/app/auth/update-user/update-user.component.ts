import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  userForm: FormGroup = new FormGroup({
    'username': new FormControl(''),
    'password': new FormControl(''),
    'role': new FormControl('')
  });
  id: number;

  constructor
    (
      private userService: UserService,
      private route: ActivatedRoute,
      private router: Router
    ) { }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.update();
    }
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.userService.getById(this.id).subscribe(
      data => {
        this.userForm = new FormGroup({
          'username': new FormControl(data.username),
          'password': new FormControl(data.password),
          'role': new FormControl(data.role)
        });
      }
    );
  }

  update() {
    const user = this.userForm.value;
    this.userService.updateUser(this.id, user).subscribe(
      data => {
        window.location.reload();
      },
      error => window.alert(error)
    );
    this.router.navigate(["/users"]);
  }
}
