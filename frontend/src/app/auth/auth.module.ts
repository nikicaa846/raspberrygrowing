import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from "@angular/common";

import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { FindUserComponent } from './find-user/find-user.component';
import { AuthRoutingModule } from './auth-routing.module';

@NgModule({
    declarations:[
        AuthComponent,
        LoginComponent,
        AddUserComponent,
        UpdateUserComponent,
        FindUserComponent
    ],
    imports: [
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        AuthRoutingModule
    ]
})
export class AuthModule{}