import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { User } from 'src/app/models/user';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  userForm: FormGroup = new FormGroup({
    'username': new FormControl(''),
    'password': new FormControl(''),
    'role': new FormControl(''),
    'idGrower': new FormControl('')
  });

  constructor(
    private userService: UserService,
    private growerService: RaspberryGrowerService,
    private router: Router
  ) { }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  ngOnInit(): void {
  }

  onAdd() {
    if (this.userForm.value['idGrower'] === "0") {
      const user = new User(
        this.userForm.value['username'],
        this.userForm.value['password'],
        this.userForm.value['role'],
        null);
      this.userService.addUser(user).subscribe(
        data => {
          this.router.navigate(['/users']);
        }, error => {
          if (error.status === 409) {
            window.alert("User with username = " + this.userForm.value['username'] + " exist in base!");
          }
        }
      )
    } else {
      this.growerService.getById(this.userForm.value['idGrower']).subscribe(
        grower => {
          const user = new User(
            this.userForm.value['username'],
            this.userForm.value['password'],
            this.userForm.value['role'],
            grower);
          this.userService.addUser(user).subscribe(
            data => {
              this.router.navigate(['/users']);
            }, error => {
              if (error.status === 409) {
                window.alert("User with username = " + this.userForm.value['username'] + " exist in base!");
              }
            }
          );
        }
      )
    }

  }
}
