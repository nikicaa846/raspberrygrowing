import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-find-user',
  templateUrl: './find-user.component.html',
  styleUrls: ['./find-user.component.css']
})
export class FindUserComponent implements OnInit {

  userForm: FormGroup = new FormGroup({
    'username': new FormControl('')
  });
  showUser = false;
  user: User;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  ngOnInit(): void {
  }

  onFind() {
    this.userService.getByUsername(this.userForm.value['username']).subscribe(
      data => {
        this.user = data;
        this.showUser = true;
      }, error => {
        if (error.status === 404) {
          window.alert("User with username " + this.userForm.value['username'] + " doesn't exist!");
           this.showUser = false;
           this.userForm = new FormGroup({
             username: new FormControl('')
           });
        }
      }
    )
  }

  onClose() {
    this.showUser = false;
    this.userForm = new FormGroup({
      username: new FormControl('')
    });

  }

  onUpdate(id: number) {
    this.router.navigate(['updateuser', id]);
  }

  onDelete(id: number) {
    this.userService.deleteUser(id).subscribe(
      data => {
        this.showUser = false;
        window.location.reload();
      }
    )
  }
}
