import { RaspberryFertilization } from "./raspberry-fertilization";
import { Worker } from "./worker";

export class RaspberryFertilizationWorker{
    id: number;
    startHour: number;
    endHour: number;
    fertilization: RaspberryFertilization;
    worker: Worker;

    constructor( startH: number, endH: number, fertilization: RaspberryFertilization, worker: Worker){
        this.startHour = startH;
        this.endHour = endH;
        this.fertilization = fertilization;
        this.worker = worker;
    }
}