import { RaspberryGrower } from "./raspberrygrower";

export class User {
    id: number;
    username: string;
    password: string;
    role: string;
    raspberryGrower: RaspberryGrower;

    constructor( username: string, password: string, role: string, raspberryGrower: RaspberryGrower){
        this.username = username;
        this.password = password;
        this.role = role;
        this.raspberryGrower = raspberryGrower;
    }  
}