import { RaspberryPlantation } from "./raspberryplantation";
import { Worker } from "./worker";

export class RaspberryDigging{
    id: number;
    dateOfDigging: Date;
    startHour: number;
    endHour: number;
    plantation: RaspberryPlantation;
    worker: Worker;

    constructor( date: Date, startHour: number, endHour: number, plantation: RaspberryPlantation, worker: Worker){
        this.dateOfDigging = date;
        this.startHour = startHour;
        this.endHour = endHour;
        this.plantation = plantation;
        this.worker = worker;
    }
}