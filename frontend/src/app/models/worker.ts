import { RaspberryGrower } from "./raspberrygrower";

export class Worker{
    id: number;
    name: string;
    surname: string;
    phone: string;
    raspberryGrower: RaspberryGrower;

    constructor(name: string, surname: string, phone: string, raspberryGrower: RaspberryGrower){
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.raspberryGrower = raspberryGrower;
    }
}