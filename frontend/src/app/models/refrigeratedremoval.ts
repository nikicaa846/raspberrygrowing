import { RaspberryPlantation } from './raspberryplantation';
import { Refrigerator } from './refrigerator';

export class RefrigeratedRemoval{
    id: number;
	kindOfRaspberry: string;
	weight: number;
    dateOfRemoval: Date;
    plantation: RaspberryPlantation;
    refrigerator: Refrigerator;
    
    constructor( kindOfRaspberry: string, weight: number, date: Date, plantation: RaspberryPlantation, refrigerator: Refrigerator){
        this.dateOfRemoval = date;
        this.kindOfRaspberry = kindOfRaspberry;
        this.weight = weight;
        this.plantation = plantation;
        this.refrigerator = refrigerator;
    }
}