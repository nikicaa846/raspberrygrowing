import { RaspberryFertilizer } from "./raspberry-fertilizer";
import { RaspberryGrower } from "./raspberrygrower";

export class InvestmentInRaspberryFertilizer{
    id: number;
    dateOfInvestment: Date;
    pricePerKgOrMl: number;
    paid: number;
    purchasedQuantity: number;
    fertilizer: RaspberryFertilizer;
    raspberryGrower: RaspberryGrower;

    constructor(date: Date, quantity: number, paid: number, fertilizer: RaspberryFertilizer, grower: RaspberryGrower){
        this.dateOfInvestment = date;
        this.paid = paid;
        this.purchasedQuantity = quantity;
        this.fertilizer = fertilizer;
        this.raspberryGrower = grower;
    }
}