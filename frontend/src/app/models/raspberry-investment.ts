import { RaspberryGrower } from "./raspberrygrower";

export class RaspberryInvestment{
    id: number;
    kindOfInvestment: string;
    dateOfInvestment: Date;
    paid: number;
    raspberryGrower: RaspberryGrower;

    constructor(kindOfInvestment: string, date: Date, paid: number, grower: RaspberryGrower){
             this.kindOfInvestment = kindOfInvestment;
             this.dateOfInvestment = date;
             this.paid = paid;
             this.raspberryGrower = grower;
         }
}