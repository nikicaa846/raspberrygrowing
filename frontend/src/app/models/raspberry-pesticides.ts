import { RaspberryGrower } from "./raspberrygrower";

export class RaspberryPesticides{
    id: number;
    kindOfPesticide: string;
    description: string;
    unitOfMeasurementOfQuantity: string;
    quantityInStore: number;
    raspberryGrower: RaspberryGrower;

    constructor( kindOfPesticide: string, description: string, 
        unitOfMeasurmentOfQuantity: string, quantityInStore: number, grower: RaspberryGrower){
            this.kindOfPesticide = kindOfPesticide;
            this.description = description;
            this.unitOfMeasurementOfQuantity = unitOfMeasurmentOfQuantity;
            this.quantityInStore = quantityInStore;
            this.raspberryGrower = grower;
    }
}