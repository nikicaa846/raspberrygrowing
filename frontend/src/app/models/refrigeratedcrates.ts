import { RefrigeratedRemoval } from './refrigeratedremoval';

export class RefrigeratedCrates{
    id: number;
    numberTakenCrates: number;
    numberSubmittedCrates: number;
    removal: RefrigeratedRemoval;

    constructor(takenCrates: number, submittedCrates: number, removal: RefrigeratedRemoval){
        this.numberTakenCrates = takenCrates;
        this.numberSubmittedCrates = submittedCrates;
        this.removal = removal;
    }
}