export class Refrigerator{
    id: number;
    name: string;
    phone: string;
    address: string;
}