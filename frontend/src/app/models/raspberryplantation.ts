import { RaspberryGrower } from './raspberrygrower';

export class RaspberryPlantation{
    id: number;
    address: string;
    size: number;
    raspberryGrower: RaspberryGrower;

    constructor( size: number, address: string, raspberryGrower: RaspberryGrower){
        this.size = size;
        this.address = address;
        this.raspberryGrower = raspberryGrower;
    }
}