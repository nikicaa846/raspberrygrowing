import { RaspberryPlantation } from './raspberryplantation';
import { Refrigerator } from './refrigerator'

export class RefrigeratedPayment{
    idPayment: number;
    weight: number;
    kindOfRaspberry: string;
    dateOfPayment: Date;
    price: number;
    plantation: RaspberryPlantation;
    refrigerator: Refrigerator;

    constructor( kindOfRaspberry: string, date: Date, price: number, plantation: RaspberryPlantation, refrigerator: Refrigerator){
        this.kindOfRaspberry = kindOfRaspberry;
        this.dateOfPayment = date;
        this.price = price;
        this.plantation = plantation;
        this.refrigerator = refrigerator;
    }
}