import { Buyer } from "./buyer";
import { RaspberryPlantation } from "./raspberryplantation";

export class Purchase{
    id: number;
    weight: number;
    kindOfRaspberry: string;
    dateOfPurchase: Date;
    price: number;
    plantation: RaspberryPlantation;
    buyer: Buyer;
    charged: string;

    constructor( plantation: RaspberryPlantation, buyer: Buyer, weight: number, kindOfRaspberry: string, charged: string, date: Date, price: number ){
        this.plantation = plantation;
        this.buyer = buyer;
        this.weight = weight;
        this.kindOfRaspberry = kindOfRaspberry;
        this.charged = charged;
        this.dateOfPurchase = date;
        this.price = price;
    }
}