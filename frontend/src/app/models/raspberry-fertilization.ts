import { RaspberryFertilizer } from "./raspberry-fertilizer";
import { RaspberryPlantation } from "./raspberryplantation";

export class RaspberryFertilization{
    id: number;
    dateOfFertilization: Date;
    quantityOfFertilizer: number;
    plantation: RaspberryPlantation;
    fertilizer: RaspberryFertilizer;

    constructor(date: Date, quantityOfFertilizer: number, plantation: RaspberryPlantation, fertilizer: RaspberryFertilizer){
        this.dateOfFertilization = date;
        this.quantityOfFertilizer = quantityOfFertilizer;
        this.plantation = plantation;
        this.fertilizer = fertilizer;
    }
}