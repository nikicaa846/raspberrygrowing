import { RaspberrySpraying } from "./raspberry-spraying";
import { Worker } from "./worker";

export class RaspberrySprayingWorker{
    id: number;
    startHour: number;
    endHour: number;
    worker: Worker;
    spraying: RaspberrySpraying;

    constructor(startHour: number, endHour: number, worker: Worker, spraying: RaspberrySpraying){
        this.startHour = startHour;
        this.endHour = endHour;
        this.worker = worker;
        this.spraying = spraying;
    }
}