import { RaspberryGrower } from "./raspberrygrower";

export class Buyer{
    id: number;
    name: string;
    surname: string;
    phone: string;
    raspberryGrower: RaspberryGrower;

    constructor(name: string, surname: string, phone: string, grower: RaspberryGrower){
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.raspberryGrower = grower;
    }
}