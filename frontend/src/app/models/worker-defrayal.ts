import { RaspberryPlantation } from "./raspberryplantation";
import { Worker } from "./worker";

export class WorkerDefrayal{
    id: number;
    dateOfDefrayal: Date;
    defrayalPerMonth: string;
    salaryForTheMonth: number;
    kindOfWork: string;
    plantation: RaspberryPlantation;
    worker: Worker;
    totalHour: number;
    pricePerHour: number;

    constructor( dateOfDefrayal: Date, defroyalPerMonth: string, kindOfWork: string, 
        plantation: RaspberryPlantation, worker: Worker, pricePerHour: number ){
            this.dateOfDefrayal = dateOfDefrayal;
            this.defrayalPerMonth = defroyalPerMonth;
            this.kindOfWork = kindOfWork;
            this.plantation = plantation;
            this.worker = worker;
            this.pricePerHour = pricePerHour;
    }
}