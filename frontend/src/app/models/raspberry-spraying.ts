import { RaspberryPesticides } from "./raspberry-pesticides";
import { RaspberryPlantation } from "./raspberryplantation";

export class RaspberrySpraying{
    id: number;
    dateOfSpraying: Date;
    quantityOfPesticide: number;
    plantation: RaspberryPlantation;
    pesticide: RaspberryPesticides;

    constructor(date: Date, quantityOfPesticide: number, pesticide: RaspberryPesticides, plantation: RaspberryPlantation){
        this.dateOfSpraying = date;
        this.quantityOfPesticide = quantityOfPesticide;
        this.pesticide = pesticide;
        this.plantation = plantation;
    }
}