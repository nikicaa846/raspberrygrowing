import { RaspberryPlantation } from "./raspberryplantation";
import { Worker } from "./worker";

export class RaspberryHarvest{
    id: number;
    dateOfHarvest: Date;
    startHour: number;
    endHour: number;
    plantation: RaspberryPlantation;
    worker: Worker;

    constructor( date: Date, startHour: number, endHour: number, plantation: RaspberryPlantation, worker: Worker){
        this.dateOfHarvest = date;
        this.startHour = startHour;
        this.endHour = endHour;
        this.plantation = plantation;
        this.worker = worker;
    }
}