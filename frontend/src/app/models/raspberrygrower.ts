export class RaspberryGrower{
    id: number;
    name: string;
    surname: string;
    address: string;
    phone: string;
    birthday: Date;
}