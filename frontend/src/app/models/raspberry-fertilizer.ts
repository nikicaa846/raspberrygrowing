import { RaspberryGrower } from "./raspberrygrower";

export class RaspberryFertilizer{
    id: number;
    kindOfFertilizer: string;
    description: string;
    unitOfMeasurementOfQuantity: string;
    quantityInStore: number;
    raspberryGrower: RaspberryGrower;

    constructor( kindOfFertilizer: string, description: string, 
        unitOfMeasurmentOfQuantity: string, quantityInStore: number, grower: RaspberryGrower){
            this.kindOfFertilizer = kindOfFertilizer;
            this.description = description;
            this.unitOfMeasurementOfQuantity = unitOfMeasurmentOfQuantity;
            this.quantityInStore = quantityInStore;
            this.raspberryGrower = grower;
    }
}