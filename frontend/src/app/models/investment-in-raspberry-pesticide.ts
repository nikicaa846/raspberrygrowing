import { RaspberryPesticides } from "./raspberry-pesticides";
import { RaspberryGrower } from "./raspberrygrower";

export class InvestmentInRaspberryPesticide{
    id: number;
    dateOfInvestment: Date;
    pricePerKgOrMl: number;
    paid: number;
    purchasedQuantity: number;
    pesticide: RaspberryPesticides;
    raspberryGrower: RaspberryGrower;

    constructor(date: Date, quantity: number, paid: number, pesticide: RaspberryPesticides, grower: RaspberryGrower){
        this.dateOfInvestment = date;
        this.paid = paid;
        this.purchasedQuantity = quantity;
        this.pesticide = pesticide;
        this.raspberryGrower = grower;
    }
}