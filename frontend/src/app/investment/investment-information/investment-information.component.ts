import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { InvestmentInRaspberryFertilizer } from 'src/app/models/investment-in-raspberry-fertilizer';
import { InvestmentInRaspberryPesticide } from 'src/app/models/investment-in-raspberry-pesticide';
import { RaspberryInvestment } from 'src/app/models/raspberry-investment';
import { AuthService } from 'src/app/services/auth.service';
import { InvestmentInRaspberryPesticideService } from 'src/app/services/investment-in-raspberry-pesticide.service';
import { InvestmentInRaspberryFertilizerService } from 'src/app/services/investment-in-raspbrry-fertilizer.service';
import { RaspberryInvestmentService } from 'src/app/services/raspberry-investment.service';

@Component({
  selector: 'app-investment-information',
  templateUrl: './investment-information.component.html',
  styleUrls: ['./investment-information.component.css']
})
export class InvestmentInformationComponent implements OnInit {

  investmentForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'kindOfInvestment': new FormControl(''),
    'idGrower': new FormControl('')
  });
  showInvestmentFertilizer = false;
  showInvestmentOther = false;
  showInvestmentPesticide = false;
  fertilizerInvestments: InvestmentInRaspberryFertilizer[] = [];
  pesticideInvestments: InvestmentInRaspberryPesticide[] = [];
  otherInvestments: RaspberryInvestment[] = [];
  idGrower: number;
  isAdmin: boolean;

  constructor(
    private investmentService: RaspberryInvestmentService,
    private investmentFertilizerService: InvestmentInRaspberryFertilizerService,
    private investmentPesticideService: InvestmentInRaspberryPesticideService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.isAdmin = this.authService.isAdmin();
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onFind();
    }
  }

  onFind() {
    switch (this.investmentForm.value['kindOfInvestment']) {
      case 'OTHER':
        if (this.idGrower === 0) {
          this.investmentService.findInvestmentsByDateAndGrower(this.investmentForm.value['date'], this.investmentForm.value['idGrower']).subscribe(
            data => {
              this.otherInvestments = data;
              this.showInvestmentOther = true;
              this.showInvestmentFertilizer = false;
              this.showInvestmentPesticide = false;
            }
          )
        } else {
          this.investmentService.findInvestmentsByDateAndGrower(this.investmentForm.value['date'], this.idGrower).subscribe(
            data => {
              this.otherInvestments = data;
              this.showInvestmentOther = true;
              this.showInvestmentFertilizer = false;
              this.showInvestmentPesticide = false;
            }
          )
        }
        break;
      case 'FERTILIZER':
        if (this.idGrower === 0) {
          this.investmentFertilizerService.
            findInvestmentsByDateAndGrower(this.investmentForm.value['date'], this.investmentForm.value['idGrower']).subscribe(
              data => {
                this.fertilizerInvestments = data;
                this.showInvestmentFertilizer = true;
                this.showInvestmentPesticide = false;
                this.showInvestmentOther = false;
              }
            )
        } else {
          this.investmentFertilizerService.findInvestmentsByDateAndGrower(this.investmentForm.value['date'], this.idGrower).subscribe(
            data => {
              this.fertilizerInvestments = data;
              this.showInvestmentFertilizer = true;
              this.showInvestmentPesticide = false;
              this.showInvestmentOther = false;
            }
          )
        }
        break;
      case 'PESTICIDE':
        if (this.idGrower === 0) {
          this.investmentPesticideService.findInvestmentsByDateAndGrower(this.investmentForm.value['date'], this.investmentForm.value['idGrower']).subscribe(
            data => {
              this.pesticideInvestments = data;
              this.showInvestmentPesticide = true;
              this.showInvestmentOther = false;
              this.showInvestmentFertilizer = false;
            }
          )
        } else {
          this.investmentPesticideService.findInvestmentsByDateAndGrower(this.investmentForm.value['date'], this.idGrower).subscribe(
            data => {
              this.pesticideInvestments = data;
              this.showInvestmentPesticide = true;
              this.showInvestmentOther = false;
              this.showInvestmentFertilizer = false;
            }
          )
        }
        break;
    }
  }

  onClose() {
    this.showInvestmentPesticide = false;
    this.showInvestmentOther = false;
    this.showInvestmentFertilizer = false;
    this.investmentForm = new FormGroup({
      'date': new FormControl(''),
      'kindOfInvestment': new FormControl(''),
      'idGrower': new FormControl('')
    });
  }

}
