import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInvestmentInRaspberryPesticideComponent } from './add-investment-in-raspberry-pesticide.component';

describe('AddInvestmentInRaspberryPesticideComponent', () => {
  let component: AddInvestmentInRaspberryPesticideComponent;
  let fixture: ComponentFixture<AddInvestmentInRaspberryPesticideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddInvestmentInRaspberryPesticideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInvestmentInRaspberryPesticideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
