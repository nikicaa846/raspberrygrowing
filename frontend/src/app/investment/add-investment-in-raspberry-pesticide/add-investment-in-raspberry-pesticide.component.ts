import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { InvestmentInRaspberryPesticide } from 'src/app/models/investment-in-raspberry-pesticide';
import { RaspberryPesticides } from 'src/app/models/raspberry-pesticides';
import { AuthService } from 'src/app/services/auth.service';
import { InvestmentInRaspberryPesticideService } from 'src/app/services/investment-in-raspberry-pesticide.service';
import { RaspberryPesticidesService } from 'src/app/services/raspberry-pesticides.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-add-investment-in-raspberry-pesticide',
  templateUrl: './add-investment-in-raspberry-pesticide.component.html',
  styleUrls: ['./add-investment-in-raspberry-pesticide.component.css']
})
export class AddInvestmentInRaspberryPesticideComponent implements OnInit {

  investmentForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'paid': new FormControl(''),
    'purchasedQuantity': new FormControl(''),
    'idPesticide': new FormControl(''),
    'idGrower': new FormControl('')
  });
  pesticides: RaspberryPesticides[] = [];
  idGrower: number;

  constructor(
    private pesticideService: RaspberryPesticidesService,
    private growerService: RaspberryGrowerService,
    private investmentService: InvestmentInRaspberryPesticideService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.pesticideService.findPesticides(this.idGrower).subscribe(
      data => {
        this.pesticides = data;
      }
    );
    this.idGrower = + this.authService.getGrower();
    if (this.idGrower !== 0) {
      this.investmentForm = new FormGroup({
        'date': new FormControl(''),
        'paid': new FormControl(''),
        'purchasedQuantity': new FormControl(''),
        'idPesticide': new FormControl(''),
        'idGrower': new FormControl(this.idGrower)
      });
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    } else if (event.key == "+") {
      this.router.navigate(['/addpesticide']);
    }
  }

  onAdd() {
    this.growerService.getById(this.investmentForm.value['idGrower']).subscribe(
      grower => {
        this.pesticideService.getPesticide(this.investmentForm.value['idPesticide']).subscribe(
          pesticide => {
            const investment = new InvestmentInRaspberryPesticide(
              new Date(this.investmentForm.value['date']),
              this.investmentForm.value['purchasedQuantity'],
              this.investmentForm.value['paid'],
              pesticide,
              grower
            );
            this.investmentService.addInvestmentInPesticide(investment).subscribe(
              data => {
                window.location.reload();
              }
            )
          }
        )
      }
    )
  }
}
