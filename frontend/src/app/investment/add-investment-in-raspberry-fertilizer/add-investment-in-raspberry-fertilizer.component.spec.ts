import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInvestmentInRaspberryFertilizerComponent } from './add-investment-in-raspberry-fertilizer.component';

describe('AddInvestmentInRaspberryFertilizerComponent', () => {
  let component: AddInvestmentInRaspberryFertilizerComponent;
  let fixture: ComponentFixture<AddInvestmentInRaspberryFertilizerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddInvestmentInRaspberryFertilizerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInvestmentInRaspberryFertilizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
