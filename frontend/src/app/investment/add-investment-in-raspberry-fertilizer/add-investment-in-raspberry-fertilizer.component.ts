import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { InvestmentInRaspberryFertilizer } from 'src/app/models/investment-in-raspberry-fertilizer';
import { RaspberryFertilizer } from 'src/app/models/raspberry-fertilizer';
import { AuthService } from 'src/app/services/auth.service';
import { InvestmentInRaspberryFertilizerService } from 'src/app/services/investment-in-raspbrry-fertilizer.service';
import { RaspberryFertilizerService } from 'src/app/services/raspberry-fertilizer.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-add-investment-in-raspberry-fertilizer',
  templateUrl: './add-investment-in-raspberry-fertilizer.component.html',
  styleUrls: ['./add-investment-in-raspberry-fertilizer.component.css']
})
export class AddInvestmentInRaspberryFertilizerComponent implements OnInit {

  investmentForm: FormGroup = new FormGroup({
    'date': new FormControl(''),
    'paid': new FormControl(''),
    'purchasedQuantity': new FormControl(''),
    'idFertilizer': new FormControl(''),
    'idGrower': new FormControl('')
  });
  fertilizers: RaspberryFertilizer[] = [];
  idGrower: number;

  constructor(
    private fertilizerService: RaspberryFertilizerService,
    private growerService: RaspberryGrowerService,
    private investmentService: InvestmentInRaspberryFertilizerService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    this.fertilizerService.findFertilizers(this.idGrower).subscribe(
      data => {
        this.fertilizers = data;
      }
    );
    this.idGrower = + this.authService.getGrower();
    if (this.idGrower !== 0) {
      this.investmentForm = new FormGroup({
        'date': new FormControl(''),
        'paid': new FormControl(''),
        'purchasedQuantity': new FormControl(''),
        'idFertilizer': new FormControl(''),
        'idGrower': new FormControl(this.idGrower)
      });
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    } else if (event.key == "+") {
      this.router.navigate(['/addfertilizer']);
    }
  }

  onAdd() {
    this.growerService.getById(this.investmentForm.value['idGrower']).subscribe(
      grower => {
        this.fertilizerService.getFertilizer(this.investmentForm.value['idFertilizer']).subscribe(
          fertilizer => {
            const investment = new InvestmentInRaspberryFertilizer(
              new Date(this.investmentForm.value['date']),
              this.investmentForm.value['purchasedQuantity'],
              this.investmentForm.value['paid'],
              fertilizer,
              grower
            );
            this.investmentService.addInvestmentInFertilizer(investment).subscribe(
              data => {
                window.location.reload();
              }
            )
          }
        )
      }
    )
  }
}
