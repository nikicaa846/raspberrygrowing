import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGaurdService } from "../services/authguard.service";
import { AddInvestmentComponent } from "./add-investment/add-investment.component";
import { AddInvestmentInRaspberryFertilizerComponent } from "./add-investment-in-raspberry-fertilizer/add-investment-in-raspberry-fertilizer.component";
import { AddInvestmentInRaspberryPesticideComponent } from "./add-investment-in-raspberry-pesticide/add-investment-in-raspberry-pesticide.component";
import { InvestmentInformationComponent } from "./investment-information/investment-information.component";
import { InvestmentComponent } from "./investment.component";

const routes: Routes = [
    { path: '', component: InvestmentComponent, canActivate:[AuthGaurdService], 
    children: [
        { path: 'investment', component: InvestmentInformationComponent},
        { path: 'addinvestment', component: AddInvestmentComponent},
        { path: 'addinvestmentinfertilizer', component: AddInvestmentInRaspberryFertilizerComponent},
        { path: 'addinvestmentinpesticide', component: AddInvestmentInRaspberryPesticideComponent}
    ]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InvestmentRoutingModule{}