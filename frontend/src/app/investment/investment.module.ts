import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";

import { AddInvestmentComponent } from "./add-investment/add-investment.component";
import { InvestmentInformationComponent } from "./investment-information/investment-information.component";
import { InvestmentRoutingModule } from "./investment-routing.module";
import { InvestmentComponent } from "./investment.component";
import { AddInvestmentInRaspberryPesticideComponent } from './add-investment-in-raspberry-pesticide/add-investment-in-raspberry-pesticide.component';
import { AddInvestmentInRaspberryFertilizerComponent } from './add-investment-in-raspberry-fertilizer/add-investment-in-raspberry-fertilizer.component';

@NgModule({
    declarations:[
        InvestmentComponent,
        AddInvestmentComponent,
        InvestmentInformationComponent,
        AddInvestmentInRaspberryPesticideComponent,
        AddInvestmentInRaspberryFertilizerComponent
    ], 
    imports:[
        BrowserAnimationsModule,
        RouterModule,
        NgbModule,
        ReactiveFormsModule,
        BsDropdownModule.forRoot(),
        CommonModule,
        InvestmentRoutingModule
    ]
})
export class InvestmentModule{}