import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { RaspberryInvestment } from 'src/app/models/raspberry-investment';
import { AuthService } from 'src/app/services/auth.service';
import { RaspberryInvestmentService } from 'src/app/services/raspberry-investment.service';
import { RaspberryGrowerService } from 'src/app/services/raspberrygrower.service';

@Component({
  selector: 'app-add-investment',
  templateUrl: './add-investment.component.html',
  styleUrls: ['./add-investment.component.css']
})
export class AddInvestmentComponent implements OnInit {

  investmentForm: FormGroup = new FormGroup({
    'kindOfInvestment': new FormControl(''),
    'date': new FormControl(''),
    'paid': new FormControl(''),
    'idGrower': new FormControl('')
  });
  idGrower: number;

  constructor(
    private growerService: RaspberryGrowerService,
    private investmentService: RaspberryInvestmentService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.idGrower = + this.authService.getGrower();
    if (this.idGrower !== 0) {
      this.investmentForm = new FormGroup({
        'kindOfInvestment': new FormControl(''),
        'date': new FormControl(''),
        'paid': new FormControl(''),
        'idGrower': new FormControl(this.idGrower)
      });
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.onAdd();
    }
  }

  onAdd() {
    this.growerService.getById(this.investmentForm.value['idGrower']).subscribe(
      data => {
        const grower = data;
        const investment = new RaspberryInvestment(
          this.investmentForm.value['kindOfInvestment'],
          new Date(this.investmentForm.value['date']),
          this.investmentForm.value['paid'],
          grower
        );
        this.investmentService.addInvestment(investment).subscribe(
          data => {
            window.location.reload();
          }
        );
      }
    );
  }
}
