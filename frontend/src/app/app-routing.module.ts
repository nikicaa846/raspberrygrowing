import { NgModule } from '@angular/core';
import { Routes, RouterModule,  PreloadAllModules } from '@angular/router';

import { HomepageComponent } from './homepage/homepage.component'

const routes: Routes = [
  { path: '', redirectTo: '/homepage', pathMatch: 'full'},
  { path: 'homepage', component: HomepageComponent},
  { path: 'raspberrygrower',
   loadChildren: () => import("src/app/raspberry-grower/raspberry-grower.module").then(m => m.RaspberryGrowerModule)},
  { path: 'login', 
  loadChildren: () => import("src/app/auth/auth.module").then(m => m.AuthModule)
  },
  { path: 'refrigerator', 
  loadChildren: () => import("src/app/refregerator/refregerator.module").then(m => m.RefregeratorModule)
  },
  { path: 'purchase',
  loadChildren: () => import("src/app/purchase/purchase.module").then(m => m.PurchaseModule)
  }, 
  { path: 'worker',
  loadChildren: () => import("src/app/worker/worker.module").then(m => m.WorkerModule)
  },
  { path: 'processing', 
  loadChildren: () => import("src/app/raspberry-processing/raspberry-procesing.module").then(m => m.RasbperryProcesingModule)
  },
  { path: 'investment',
  loadChildren: () => import("src/app/investment/investment.module").then(m => m.InvestmentModule)
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
