CREATE TABLE raspberry_worker_defrayal
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   date_of_defrayal date NOT NULL,
   salary_for_the_month double(8,2) NOT NULL,
   price_per_hour double(8,2) NOT NULL,
   kind_of_work varchar (45) NOT NULL,
   id_worker bigint (20) NOT NULL,
   id_plantation bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_worker) REFERENCES raspberry_worker (id),
   FOREIGN KEY (id_plantation) REFERENCES raspberry_plantation (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;