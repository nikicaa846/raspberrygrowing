CREATE TABLE refrigerated_removal
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   date_of_removal date NOT NULL,
   kind_of_raspberry varchar (45) NOT NULL,
   weight double (5,2) NOT NULL,
   id_plantation bigint (20) NOT NULL,
   id_refrigerator bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_plantation) REFERENCES raspberry_plantation (id),
   FOREIGN KEY (id_refrigerator) REFERENCES refrigerator (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;