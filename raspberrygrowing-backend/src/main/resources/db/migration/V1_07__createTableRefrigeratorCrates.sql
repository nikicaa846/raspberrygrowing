CREATE TABLE refrigerated_crates
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   number_taken_crates int NOT NULL,
   number_submitted_crates int NOT NULL,
   id_removal bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_removal) REFERENCES refrigerated_removal (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;