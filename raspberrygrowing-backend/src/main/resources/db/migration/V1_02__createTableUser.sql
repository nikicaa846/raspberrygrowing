CREATE TABLE users
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   username varchar (45) NOT NULL,
   password varchar (45) NOT NULL,
   role varchar (45) NOT NULL,
   id_grower bigint,
   PRIMARY KEY (id),
   FOREIGN KEY (id_grower) REFERENCES raspberry_grower (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;