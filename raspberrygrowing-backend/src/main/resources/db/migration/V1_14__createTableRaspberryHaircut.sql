CREATE TABLE raspberry_haircut
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   date_of_haircut date NOT NULL,
   start_hour time NOT NULL,
   end_hour time NOT NULL,
   id_worker bigint (20) NOT NULL,
   id_plantation bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_worker) REFERENCES raspberry_worker (id),
   FOREIGN KEY (id_plantation) REFERENCES raspberry_plantation (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;