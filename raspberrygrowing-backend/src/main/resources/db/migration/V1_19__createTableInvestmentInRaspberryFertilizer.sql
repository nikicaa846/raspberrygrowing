CREATE TABLE raspberry_investment_in_fertilizer
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   date_of_investment date NOT NULL,
   price_per_kg_or_ml double(5,2) NOT NULL,
   paid double(8,2) NOT NULL,
   purchased_quantity double(5,2) NOT NULL,
   id_fertilizer bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_fertilizer) REFERENCES raspberry_fertilizer (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;