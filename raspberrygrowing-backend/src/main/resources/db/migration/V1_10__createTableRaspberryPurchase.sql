CREATE TABLE raspberry_purchase
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   charged varchar (3) NOT NULL,
   date_of_purchase date NOT NULL,
   kind_of_raspberry varchar (45) NOT NULL,
   price double (8,2) NOT NULL,
   weight double (5,2) NOT NULL,
   id_buyer bigint (20) NOT NULL,
   id_plantation bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_plantation) REFERENCES raspberry_plantation (id),
   FOREIGN KEY (id_buyer) REFERENCES raspberry_buyer (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;