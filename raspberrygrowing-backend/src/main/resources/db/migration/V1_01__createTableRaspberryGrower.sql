CREATE TABLE raspberry_grower
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   name varchar (45) NOT NULL,
   surname varchar (45) NOT NULL,
   phone varchar (20) NOT NULL,
   address varchar (100) NOT NULL,
   birthday date NOT NULL,
   PRIMARY KEY (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;