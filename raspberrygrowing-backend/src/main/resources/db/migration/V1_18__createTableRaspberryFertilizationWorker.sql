CREATE TABLE raspberry_fertilization_worker
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   start_hour time NOT NULL,
   end_hour time NOT NULL,
   id_worker bigint (20) NOT NULL,
   id_fertilization bigint(20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_worker) REFERENCES raspberry_worker (id),
   FOREIGN KEY (id_fertilization) REFERENCES raspberry_fertilization(id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;