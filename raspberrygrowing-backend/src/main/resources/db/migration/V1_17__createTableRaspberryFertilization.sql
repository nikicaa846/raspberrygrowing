CREATE TABLE raspberry_fertilization
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   date_of_fertilization date NOT NULL,
   quantity_of_fertilizer double(5,2) NOT NULL,
   id_fertilizer bigint (20) NOT NULL,
   id_plantation bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_fertilizer) REFERENCES raspberry_fertilizer (id),
   FOREIGN KEY (id_plantation) REFERENCES raspberry_plantation (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;