CREATE TABLE raspberry_investment
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   date_of_investment date NOT NULL,
   kind_of_investment varchar(45) NOT NULL,
   paid double(8,2) NOT NULL,
   id_grower bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_grower) REFERENCES raspberry_grower (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;