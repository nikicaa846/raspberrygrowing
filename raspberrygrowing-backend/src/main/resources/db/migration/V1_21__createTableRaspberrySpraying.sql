CREATE TABLE raspberry_spraying
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   date_of_spraying date NOT NULL,
   quantity_of_pesticide double(5,2) NOT NULL,
   id_pesticide bigint (20) NOT NULL,
   id_plantation bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_pesticide) REFERENCES raspberry_pesticide (id),
   FOREIGN KEY (id_plantation) REFERENCES raspberry_plantation (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;