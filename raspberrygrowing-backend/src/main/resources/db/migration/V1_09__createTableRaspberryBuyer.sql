CREATE TABLE raspberry_buyer
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   name varchar (45) NOT NULL,
   surname varchar (45) NOT NULL,
   phone varchar (20) NOT NULL,
   id_grower bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_grower) REFERENCES raspberry_grower (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;