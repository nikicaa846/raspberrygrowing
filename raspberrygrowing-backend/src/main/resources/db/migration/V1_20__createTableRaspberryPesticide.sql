CREATE TABLE raspberry_pesticide
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   description varchar(300) ,
   kind_of_pesticide varchar(45) NOT NULL,
   quantity_in_store double(8,2) NOT NULL,
   unit_of_measurement_of_quantity varchar(2) NOT NULL,
   id_grower bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_grower) REFERENCES raspberry_grower (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;