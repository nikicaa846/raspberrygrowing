CREATE TABLE raspberry_plantation
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   address varchar (100) NOT NULL,
   size double (8,2) NOT NULL,
   id_grower bigint (20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_grower) REFERENCES raspberry_grower (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;