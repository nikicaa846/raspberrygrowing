CREATE TABLE raspberry_spraying_worker
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   start_hour time NOT NULL,
   end_hour time NOT NULL,
   id_worker bigint (20) NOT NULL,
   id_spraying bigint(20) NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_worker) REFERENCES raspberry_worker (id),
   FOREIGN KEY (id_spraying) REFERENCES raspberry_spraying (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;