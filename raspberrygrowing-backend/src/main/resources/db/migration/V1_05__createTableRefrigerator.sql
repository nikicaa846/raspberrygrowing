CREATE TABLE refrigerator
(
   id bigint (20) NOT NULL AUTO_INCREMENT,
   name varchar (45) NOT NULL,
   phone varchar (20),
   address varchar (100) NOT NULL,
   PRIMARY KEY (id)
)
ENGINE= InnoDB DEFAULT CHARSET= utf8;