package net.raspberrygrowingbackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryGrower;
import net.raspberrygrowingbackend.models.RaspberryPlantation;
import net.raspberrygrowingbackend.repository.RaspberryGrowerRepository;

@Service
public class RaspberryGrowerService {

	@Autowired
	private RaspberryGrowerRepository raspberryGrowerRepo;
	
	@Autowired
	private RaspberryPlantationService plantationService;
	
	@Autowired
	private RaspberryPurchaseService purchaseService;
	
	@Autowired
	private RefrigeratedPaymentService paymentService;
	
	@Autowired
	private RaspberryWorkerDefrayalService defrayalService;
	
	@Autowired
	private RaspberryInvestmentService investmentService;
	
	@Autowired
	private InvestmentInRaspberryFertilizerService investmentInFertilizerService;
	
	@Autowired
	private InvestmentInRaspberryPesticideService investmentInPesticideService;
	
	public RaspberryGrower getById(Long id) {
		if ( raspberryGrowerRepo.getById(id) == null ) {
			throw new ResourceNotFoundException("No grower with this id!");
		}
		return raspberryGrowerRepo.getById(id);
	}
	
	public Double getProfit(Long id, Integer year) {
		Double total = 0.0;
		List<RaspberryPlantation> plantations = this.plantationService.findPlantations(id);
		for ( RaspberryPlantation plantation: plantations) {
			total += this.paymentService.getTotalPaymentForPlantation(plantation.getId(), year);
			total += this.purchaseService.getTotalEarnings(year, plantation.getId());
			total -= this.defrayalService.getTotalDefrayalForWorkers(plantation.getId(), year);
		}
		total -= this.investmentInFertilizerService.getTotalInvestmentForYear(year, id);
		total -= this.investmentInPesticideService.getTotalInvestmentForYear(year, id);
		total -= this.investmentService.getTotalInvestmentForYear(year, id);
		
		return total;
	}
	
	public RaspberryGrower addGrower(RaspberryGrower grower) {
		return this.raspberryGrowerRepo.save(grower);
	}
	
	public RaspberryGrower updateGrower(Long id, RaspberryGrower grower) {
		RaspberryGrower growerForUpdate = this.getById(id);
		
		growerForUpdate.setSurname(grower.getSurname());
		growerForUpdate.setAddress(grower.getAddress());
		growerForUpdate.setPhone(grower.getPhone());
		
		return this.raspberryGrowerRepo.save(growerForUpdate);
	}
	
	public void deleteGrower(Long id) {
		this.raspberryGrowerRepo.deleteById(id);
	}
}
