package net.raspberrygrowingbackend.service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryDigging;
import net.raspberrygrowingbackend.repository.RaspberryDigginRepository;

@Service
public class RaspberryDiggingService {

	@Autowired
	private RaspberryDigginRepository diggingRepository;

	public List<RaspberryDigging> findDiggingByDateAndPlantation(LocalDate date, Long idPlantation) {
		if(this.diggingRepository.findByDateOfDiggingAndPlantationId(date, idPlantation).size() == 0)
			throw new ResourceNotFoundException("No digging with this date!");
		return this.diggingRepository.findByDateOfDiggingAndPlantationId(date, idPlantation);
	}

	public Integer getNumberOfMonth(String monthName) {
		switch (monthName) {
		case "January":
			return 1;
		case "February":
			return 2;
		case "March":
			return 3;
		case "April":
			return 4;
		case "May":
			return 5;
		case "Jun":
			return 6;
		case "July":
			return 7;
		case "Avgust":
			return 8;
		case "September":
			return 9;
		case "October":
			return 10;
		case "November":
			return 11;
		case "December":
			return 12;
		}

		return 0;
	}

	public Double getTotalWorkerHoursPerMonth( Long idWorker, String monthName) {
		Double total = 0.0;
		Integer month = this.getNumberOfMonth(monthName);
		List<RaspberryDigging> diggings = this.diggingRepository.findByWorkerId(idWorker);
		for (RaspberryDigging digging : diggings) {
			if (digging.getDateOfDigging().getMonthValue() == month) {
				if (digging.getStartHour() == null && digging.getEndHour() == null) {
					continue;
				}
				Long workingDigging = Duration.between(digging.getStartHour(), digging.getEndHour()).toHours();
				total += workingDigging;
			}
		}
		return total;
	}

	public RaspberryDigging addDigging(RaspberryDigging digging) {
		return this.diggingRepository.save(digging);
	}
}
