package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.models.InvestmentInRaspberryPesticide;
import net.raspberrygrowingbackend.repository.InvestmentInRaspberryPesticideRepository;

@Service
public class InvestmentInRaspberryPesticideService {

	@Autowired
	private InvestmentInRaspberryPesticideRepository investemntPesticideRepo;
	
	@Autowired
	private RaspberryPesticidesService pesticidesService;
	
	public List<InvestmentInRaspberryPesticide> findInvestmentsByDateAndGrower(LocalDate date, Long idGrower){
		return this.investemntPesticideRepo.findInvestmentsByDateOfInvestmentAndPesticideRaspberryGrowerId(date, idGrower);
	}
	
	public Double setPrice(Double paid, Double quantity, String unitOfMeasurment) {
		if ( unitOfMeasurment.equals("ml") ) {
			return Math.round((( paid * 100 ) / quantity) * 100 ) / 100.0;
		} else if ( unitOfMeasurment.equals("kg") ) {
			return Math.round(( paid / quantity ) * 100 ) / 100.0;
		}
		return null;
	}
	
	public InvestmentInRaspberryPesticide addInvestmentInPesticide(InvestmentInRaspberryPesticide investment) {
		this.pesticidesService.investmentInPesticide(investment.getPesticide().getId(), investment.getPurchasedQuantity());
		investment.setPricePerKgOrMl(setPrice(investment.getPaid(), investment.getPurchasedQuantity(), investment.getPesticide().getUnitOfMeasurementOfQuantity()));
		
		return this.investemntPesticideRepo.save(investment);
	}
	
	public Double getTotalInvestmentForYear(Integer year, Long idGrower) {
		Double total = 0.0;
		List<InvestmentInRaspberryPesticide> investments = this.investemntPesticideRepo.findInvestmentsByPesticideRaspberryGrowerId(idGrower);
		for ( InvestmentInRaspberryPesticide investment: investments) {
			if ( investment.getDateOfInvestment().getYear() == year) {
				total += investment.getPaid();
			}
		}
		return total;
	}
}
