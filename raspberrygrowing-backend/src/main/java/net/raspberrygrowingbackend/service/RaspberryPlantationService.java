package net.raspberrygrowingbackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryPlantation;
import net.raspberrygrowingbackend.repository.RaspberryPlantationRepository;

@Service
public class RaspberryPlantationService {

	@Autowired
	private RaspberryPlantationRepository plantationRepo;
	
	public RaspberryPlantation getPlantationById(Long id) {
		return this.plantationRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Plantation doesn't exist with id = " + id));
	}
	
	public List<RaspberryPlantation> findPlantations(Long idGrower) {
		if ( idGrower == 0 ) {
			return this.plantationRepo.findAll();
		} else {
			return this.plantationRepo.findPlantationByRaspberryGrowerId(idGrower);
		}
	}
	
	public RaspberryPlantation addPlantation(RaspberryPlantation plantation) {
		return this.plantationRepo.save(plantation);
	}
	
	public RaspberryPlantation updatePlantation(Long id, RaspberryPlantation plantation) {
		RaspberryPlantation plantationForUpdate = this.getPlantationById(id);
		
		plantationForUpdate.setAddress(plantation.getAddress());
		plantationForUpdate.setRaspberryGrower(plantation.getRaspberryGrower());
		plantationForUpdate.setSize(plantation.getSize());
		
		return this.plantationRepo.save(plantationForUpdate);
	}
	
	public void deletePlantation(Long id) {
		this.plantationRepo.deleteById(id);
	}
}
