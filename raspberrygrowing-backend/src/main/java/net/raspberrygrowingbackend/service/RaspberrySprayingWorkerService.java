package net.raspberrygrowingbackend.service;

import java.time.Duration;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.models.RaspberrySprayingWorker;
import net.raspberrygrowingbackend.repository.RaspberrySprayingWorkerRepository;

@Service
public class RaspberrySprayingWorkerService {

	@Autowired
	private RaspberrySprayingWorkerRepository sprayingWorkerRepo;

	public List<RaspberrySprayingWorker> findBySpraying( Long id) {
		return this.sprayingWorkerRepo.findBySprayingId(id);
	}

	public Integer getNumberOfMonth(String monthName) {
		switch (monthName) {
		case "January":
			return 1;
		case "February":
			return 2;
		case "March":
			return 3;
		case "April":
			return 4;
		case "May":
			return 5;
		case "Jun":
			return 6;
		case "July":
			return 7;
		case "Avgust":
			return 8;
		case "September":
			return 9;
		case "October":
			return 10;
		case "November":
			return 11;
		case "December":
			return 12;
		}

		return 0;
	}

	public Double getTotalWorkerHoursPerMonth( Long idWorker, String monthName) {
		Double total = 0.0;
		Integer month = this.getNumberOfMonth(monthName);
		List<RaspberrySprayingWorker> sprayingWorkers = this.sprayingWorkerRepo.findByWorkerId(idWorker);
		for (RaspberrySprayingWorker sprayingWorker : sprayingWorkers) {
			if (sprayingWorker.getSpraying().getDateOfSpraying().getMonthValue() == month) {
				if (sprayingWorker.getStartHour() == null && sprayingWorker.getEndHour() == null) {
					continue;
				}
				Long workingSpraying = Duration.between(sprayingWorker.getStartHour(), sprayingWorker.getEndHour())
						.toHours();
				total += workingSpraying;
			}
		}
		return total;
	}

	public RaspberrySprayingWorker addSprayingWorker(RaspberrySprayingWorker sprayingWorker) {
		return this.sprayingWorkerRepo.save(sprayingWorker);
	}
}
