package net.raspberrygrowingbackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryPesticides;
import net.raspberrygrowingbackend.repository.RaspberryPesticidesRepository;

@Service
public class RaspberryPesticidesService {

	@Autowired
	private RaspberryPesticidesRepository pesticideRepo;
	
	public RaspberryPesticides getPesticide( Long id) {
		return this.pesticideRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Fertilizer doesn't exist with id = " + id));
	}

	public List<RaspberryPesticides> findPesticides(Long idGrower){
		if( idGrower == 0 ) {
			return this.pesticideRepo.findAll();
		} else {
			return this.pesticideRepo.findPesticidesByRaspberryGrowerId(idGrower);
		}
	}
	
	public RaspberryPesticides addPesticide(RaspberryPesticides pesticide) {
		return this.pesticideRepo.save(pesticide);
	}
	
	public RaspberryPesticides updatePesticide( Long id, RaspberryPesticides pesticide) {
		RaspberryPesticides pesticideForUpdate = this.getPesticide(id);
		
		pesticideForUpdate.setKindOfPesticide(pesticide.getKindOfPesticide());
		pesticideForUpdate.setDescription(pesticide.getDescription());
		pesticideForUpdate.setQuantityInStore(pesticide.getQuantityInStore());
		
		return this.pesticideRepo.save(pesticideForUpdate);
	}
	
	public RaspberryPesticides investmentInPesticide( Long id, Double purchasedQuantity) {
		RaspberryPesticides investmentPesticide = this.getPesticide(id);
		
		investmentPesticide.setQuantityInStore(investmentPesticide.getQuantityInStore() + purchasedQuantity);
		
		return this.pesticideRepo.save(investmentPesticide);
	}
	
	public RaspberryPesticides usePesticideForSpraying( Long id, Double quanityOfPesticide) {
		RaspberryPesticides usingPesticide = this.getPesticide(id);
		
		usingPesticide.setQuantityInStore(usingPesticide.getQuantityInStore() - quanityOfPesticide);
		
		return this.pesticideRepo.save(usingPesticide);
	}
	
	public void deletePesticide( Long id) {
		this.pesticideRepo.deleteById(id);
	}
}
