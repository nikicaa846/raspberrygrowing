package net.raspberrygrowingbackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryBuyer;
import net.raspberrygrowingbackend.repository.RaspberryBuyerRepository;

@Service
public class RaspberryBuyerService {

	@Autowired
	private RaspberryBuyerRepository buyerRepo;
	
	public RaspberryBuyer getBuyer( Long id) {
		return this.buyerRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Buyer doesn't exist with id = " + id));
	}
	
	public List<RaspberryBuyer> findBuyers(Long idGrower){
		if ( idGrower == 0) {
			return this.buyerRepo.findAll();
		} else {
			return this.buyerRepo.findBuyersByRaspberryGrowerId(idGrower);
		}
	}
	
	public RaspberryBuyer addBuyer(RaspberryBuyer buyer) {
		return this.buyerRepo.save(buyer);
	}
	
	public RaspberryBuyer updateBuyer( Long id, RaspberryBuyer buyer) {
		RaspberryBuyer buyerForUpdate = this.buyerRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Buyer doesn't exist with id = " + id));
		
		buyerForUpdate.setName(buyer.getName());
		buyerForUpdate.setSurname(buyer.getSurname());
		buyerForUpdate.setPhone(buyer.getPhone());
		
		return this.buyerRepo.save(buyerForUpdate);
	}
	
	public void deleteBuyer( Long id) {
		this.buyerRepo.deleteById(id);
	}
}
