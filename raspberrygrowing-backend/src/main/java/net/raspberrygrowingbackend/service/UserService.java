package net.raspberrygrowingbackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.DuplicateRequestException;
import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.User;

import net.raspberrygrowingbackend.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepo;

	public User getById(Long id) {
		return this.userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User doesn't exist with id = " + id));
	}

	public User getByUsername(String username) {
		if (this.userRepo.getByUsername(username) == null) {
			throw new ResourceNotFoundException("User doesn't exist with username which you input!");
		}
		return this.userRepo.getByUsername(username);
	}

	public User addUser(User user) {
		if (this.userRepo.getByUsername(user.getUsername()) != null) {
			throw new DuplicateRequestException();
		}
		return this.userRepo.save(user);
	}

	public User updateUser(Long id, User user) {
		User userForUpdate = this.getById(id);

		userForUpdate.setPassword(user.getPassword());
		userForUpdate.setUsername(user.getUsername());
		userForUpdate.setRole(user.getRole());

		return this.userRepo.save(userForUpdate);
	}

	public void deleteUser(Long id) {
		this.userRepo.deleteById(id);
	}
}
