package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RefrigeratedCrates;
import net.raspberrygrowingbackend.repository.RefrigeratedCratesRepository;

@Service
public class RefrigeratedCratesService {

	@Autowired
	private RefrigeratedCratesRepository cratesRepo;

	public RefrigeratedCrates addCrates(RefrigeratedCrates crates) {
		return this.cratesRepo.save(crates);
	}

	public List<RefrigeratedCrates> findCrates(LocalDate date, Long idPlantation) {
		if(this.cratesRepo.findCratesByRemovalDateOfRemovalAndRemovalPlantationId(date, idPlantation).size() == 0 )
			throw new ResourceNotFoundException("No removal with this date!");
		return this.cratesRepo.findCratesByRemovalDateOfRemovalAndRemovalPlantationId(date, idPlantation);
	}
}
