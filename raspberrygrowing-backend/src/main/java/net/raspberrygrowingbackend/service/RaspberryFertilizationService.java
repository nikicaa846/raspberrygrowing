package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryFertilization;
import net.raspberrygrowingbackend.repository.RaspberryFertilizationRepository;

@Service
public class RaspberryFertilizationService {

	@Autowired
	private RaspberryFertilizationRepository fertilizationRepo;
	
	@Autowired
	private RaspberryFertilizerService fertilizerService;
	
	public RaspberryFertilization getById( Long id) {
		return this.fertilizationRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Fertilization doesn't exist with id = " + id));
	}
	
	public RaspberryFertilization findByDateAndPlantationAndFertilizer(LocalDate date, Long idPlantation,  Long idFertilizer){
		if (this.fertilizationRepo.findByDateOfFertilizationAndPlantationIdAndFertilizerId(date, idPlantation, idFertilizer) == null )
			throw new ResourceNotFoundException("No fertilization!");
		return this.fertilizationRepo.findByDateOfFertilizationAndPlantationIdAndFertilizerId(date, idPlantation, idFertilizer);
	}
	
	public List<RaspberryFertilization> findFertilizationByDateAndPlantation(LocalDate date, Long idPlantation){
		return this.fertilizationRepo.findByDateOfFertilizationAndPlantationId(date, idPlantation);
	}
	
	
	public RaspberryFertilization addFertilization(RaspberryFertilization fertilization) {
		this.fertilizerService.useFertilizerInFertilization(fertilization.getFertilizer().getId(), fertilization.getQuantityOfFertilizer());
		
		return this.fertilizationRepo.save(fertilization);
	}
}
