package net.raspberrygrowingbackend.service;

import java.time.Duration;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.models.RaspberryFertilizationWorker;
import net.raspberrygrowingbackend.repository.RaspberryFertilizationWorkerRepository;

@Service
public class RaspberryFertilizationWorkerService {

	@Autowired
	private RaspberryFertilizationWorkerRepository fertilizationWorkerRepo;

	public List<RaspberryFertilizationWorker> findByFertilziation( Long id) {
		return this.fertilizationWorkerRepo.findByFertilizationId(id);
	}

	public Integer getNumberOfMonth(String monthName) {
		switch (monthName) {
		case "January":
			return 1;
		case "February":
			return 2;
		case "March":
			return 3;
		case "April":
			return 4;
		case "May":
			return 5;
		case "Jun":
			return 6;
		case "July":
			return 7;
		case "Avgust":
			return 8;
		case "September":
			return 9;
		case "October":
			return 10;
		case "November":
			return 11;
		case "December":
			return 12;
		}

		return 0;
	}

	public Double getTotalWorkerHoursPerMonth( Long idWorker, String monthName) {
		Double total = 0.0;
		Integer month = this.getNumberOfMonth(monthName);
		List<RaspberryFertilizationWorker> fertilizationWorkers = this.fertilizationWorkerRepo.findByWorkerId(idWorker);
		for (RaspberryFertilizationWorker fertilizationWorker : fertilizationWorkers) {
			if (fertilizationWorker.getFertilization().getDateOfFertilization().getMonthValue() == month) {
				if (fertilizationWorker.getStartHour() == null && fertilizationWorker.getEndHour() == null) {
					continue;
				}
				Long workingFertilization = Duration
						.between(fertilizationWorker.getStartHour(), fertilizationWorker.getEndHour()).toHours();
				total += workingFertilization;
			}
		}
		return total;
	}

	public RaspberryFertilizationWorker addFertilizationWorker(RaspberryFertilizationWorker fertilizationWorker) {
		return this.fertilizationWorkerRepo.save(fertilizationWorker);
	}
}
