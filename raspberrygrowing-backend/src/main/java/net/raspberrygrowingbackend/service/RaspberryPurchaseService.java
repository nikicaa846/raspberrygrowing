package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryPurchase;
import net.raspberrygrowingbackend.repository.RaspberryPurchaseRepository;

@Service
public class RaspberryPurchaseService {

	@Autowired
	private RaspberryPurchaseRepository purchaseRepo;
	
	public List<RaspberryPurchase> findByDateAndPlantation(LocalDate date, Long idPlantation){
		if ( this.purchaseRepo.findByDateOfPurchaseAndPlantationId(date, idPlantation).size() == 0)
			throw new ResourceNotFoundException("No purchase with this date!");
		return this.purchaseRepo.findByDateOfPurchaseAndPlantationId(date, idPlantation);
	}
	
	public double setPrice(Double weight, Double pricePerKg) {
		double price = weight * pricePerKg;
		return Math.round( price * 100.0) / 100.0 ;
	}
	
	public RaspberryPurchase addPurchase(RaspberryPurchase purchase) {
		if ( purchase.getCharged().equals("YES")) {
			purchase.setPrice(setPrice(purchase.getWeight(), purchase.getPrice()));
		} else {
			purchase.setPrice(0.0);
		}
		
		return this.purchaseRepo.save(purchase);
	}
	
	public double getTotalEarnings(Integer year, Long idPlantation) {
		double total = 0;
		List<RaspberryPurchase> purchases = this.purchaseRepo.findByPlantationId(idPlantation);
		for ( RaspberryPurchase purchase : purchases ) {
			if( purchase.getDateOfPurchase().getYear() == year) {
				total += purchase.getPrice();
			}
		}
		return Math.round(total * 100.0) / 100.0;
	}
	
	public double getTotalPurchaseWeight(Integer year, Long idPlantation, String kind) {
		double totalWeight = 0;
		List<RaspberryPurchase> purchases = this.purchaseRepo.findByPlantationId(idPlantation);
		for ( RaspberryPurchase purchase : purchases ) {
			if ( purchase.getDateOfPurchase().getYear() == year && purchase.getKindOfRaspberry().equals(kind)) {
				totalWeight += purchase.getWeight();
			}
		}
		
		return Math.round(totalWeight * 100.0) / 100.0;
	}
}
