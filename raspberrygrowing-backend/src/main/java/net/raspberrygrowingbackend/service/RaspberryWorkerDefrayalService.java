package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryWorkerDefrayal;
import net.raspberrygrowingbackend.repository.RaspberryWorkerDefrayalRepository;

@Service
public class RaspberryWorkerDefrayalService {

	@Autowired
	private RaspberryWorkerDefrayalRepository defrayalRepo;

	@Autowired
	private RaspberryHarvestService harvestService;

	@Autowired
	private RaspberryDiggingService diggingService;

	@Autowired
	private RaspberryHaricutService haircutService;
	
	@Autowired
	private RaspberrySprayingWorkerService sprayingWorkerService;

	@Autowired
	private RaspberryFertilizationWorkerService fertilizationWorkerService;

	public Double getTotalPrice(double price, double hour) {
		return price * hour;
	}

	public Double getTotalHoursForMonth( Long idWorker, String kindOfWork, String monthName) {
		if( kindOfWork.equals("harvest") ) {
			return this.harvestService.getTotalWorkerHoursPerMonth(idWorker, monthName);
		} else if ( kindOfWork.equals("digging") ) {
			return this.diggingService.getTotalWorkerHoursPerMonth(idWorker, monthName);
		} else if( kindOfWork.equals("haircut") ) {
			return this.haircutService.getTotalWorkerHoursPerMonth(idWorker, monthName);
		} else if( kindOfWork.equals("spraying") ) {
			return this.sprayingWorkerService.getTotalWorkerHoursPerMonth(idWorker, monthName);
		} else if( kindOfWork.equals("fertilization") ) {
			return this.fertilizationWorkerService.getTotalWorkerHoursPerMonth(idWorker, monthName);
		}
		
		return null;
	}

	public RaspberryWorkerDefrayal addDefrayal(RaspberryWorkerDefrayal defrayal) {
		defrayal.setTotalHour(
				getTotalHoursForMonth(defrayal.getWorker().getId(), defrayal.getKindOfWork(), defrayal.getDefrayalPerMonth()));
		defrayal.setSalaryForTheMonth(getTotalPrice(defrayal.getPricePerHour(), defrayal.getTotalHour()));

		return this.defrayalRepo.save(defrayal);
	}

	public List<RaspberryWorkerDefrayal> findWorkerDefrayalByDateAndPlantation(LocalDate date, Long idPlantation){
		if ( this.defrayalRepo.findByDateOfDefrayalAndPlantationId(date, idPlantation).size() == 0 ) {
			throw new ResourceNotFoundException("No defroyal with this date!");
		}
		return this.defrayalRepo.findByDateOfDefrayalAndPlantationId(date, idPlantation);
	}
	
	public Double getTotalDefrayalForWorkers(Long idPlantation, Integer year) {
		double total = 0;
		List<RaspberryWorkerDefrayal> defrayals = this.defrayalRepo.findByPlantationId(idPlantation);
		for ( RaspberryWorkerDefrayal defrayal: defrayals) {
			if ( defrayal.getDateOfDefrayal().getYear() == year ) {
				total += defrayal.getSalaryForTheMonth();
			}
		}
		
		return total;
	}
	
	public List<RaspberryWorkerDefrayal> findDefrayalsWorkerPlantationAndYear( Long idWorker, Long idPlantation, Integer year) {
		List<RaspberryWorkerDefrayal> defrayals = new ArrayList<RaspberryWorkerDefrayal>();
		List<RaspberryWorkerDefrayal> defrayalasByWorker = this.defrayalRepo.findDefrayalsByWorkerId(idWorker);
		for ( RaspberryWorkerDefrayal workerDefrayal: defrayalasByWorker) {
			if ( workerDefrayal.getPlantation().getId() == idPlantation && 
					workerDefrayal.getDateOfDefrayal().getYear() == year) {
				defrayals.add(workerDefrayal);
			}
		}
		return defrayals;
	}
}
