package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RefrigeratedRemoval;
import net.raspberrygrowingbackend.repository.RefrigeratedRemovalRepository;

@Service
public class RefrigeratedRemovalService {

	@Autowired
	private RefrigeratedRemovalRepository removalRepo;
	
	public List<RefrigeratedRemoval> findByKindOfRaspberryDateAndPlantation(Long idPlantation, String raspberry, LocalDate date){
		if ( this.removalRepo.findByPlantationIdAndKindOfRaspberryAndDateOfRemoval(idPlantation, raspberry, date).size() == 0 )
			throw new ResourceNotFoundException("No removal with this date!");
		return this.removalRepo.findByPlantationIdAndKindOfRaspberryAndDateOfRemoval(idPlantation, raspberry, date);
	}
	
	public RefrigeratedRemoval addRemoval(RefrigeratedRemoval removal) {
		return this.removalRepo.save(removal);
	}
	
	public Double getTotalWeight(Long idPlantation, Integer year, String kind) {
		double total = 0;
		List<RefrigeratedRemoval> removals = this.removalRepo.findByPlantationId(idPlantation);
		for ( RefrigeratedRemoval removal: removals) {
			if ( removal.getDateOfRemoval().getYear() == year && removal.getKindOfRaspberry().equals(kind)) {
				total += removal.getWeight();
			}
		}
		
		return total;
	}
}
