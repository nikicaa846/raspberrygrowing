package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberrySpraying;
import net.raspberrygrowingbackend.repository.RaspberrySprayingRepository;

@Service
public class RaspberrySprayingService {

	@Autowired
	private RaspberrySprayingRepository sprayingRepo;
	
	@Autowired
	private RaspberryPesticidesService pesticideService;
	
	public RaspberrySpraying getById( Long id) {
		return this.sprayingRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Spraying doesn't exist with id = " + id));
	}
	
	public RaspberrySpraying findByDatePlantationPesticide(LocalDate date, Long idPlantation, Long idPesticide) {
		if(this.sprayingRepo.findByDateOfSprayingAndPlantationIdAndPesticideId(date, idPlantation, idPesticide) == null)
			throw new ResourceNotFoundException("No spraying for this date!");
		return this.sprayingRepo.findByDateOfSprayingAndPlantationIdAndPesticideId(date, idPlantation, idPesticide);
	}
	
	public List<RaspberrySpraying> findSprayingByDateAndPlantation(LocalDate date, Long idPlantation){
		return this.sprayingRepo.findByDateOfSprayingAndPlantationId(date, idPlantation);
	}
	
	public RaspberrySpraying addSpraying(RaspberrySpraying spraying) {
		this.pesticideService.usePesticideForSpraying(spraying.getPesticide().getId(), spraying.getQuantityOfPesticide());
		
		return this.sprayingRepo.save(spraying);
	}
}
