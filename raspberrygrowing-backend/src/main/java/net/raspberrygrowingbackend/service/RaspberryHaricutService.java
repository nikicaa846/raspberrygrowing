package net.raspberrygrowingbackend.service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryHaircut;
import net.raspberrygrowingbackend.repository.RaspberryHaircutRepository;

@Service
public class RaspberryHaricutService {

	@Autowired
	private RaspberryHaircutRepository haircutRepo;

	public List<RaspberryHaircut> findHaircutByDateAndPlantation(LocalDate date, Long idPlantation) {
		if(this.haircutRepo.findByDateOfHaircutAndPlantationId(date, idPlantation).size() == 0)
			throw new ResourceNotFoundException("No haircut with this date!");
		return this.haircutRepo.findByDateOfHaircutAndPlantationId(date, idPlantation);
	}

	public Integer getNumberOfMonth(String monthName) {
		switch (monthName) {
		case "January":
			return 1;
		case "February":
			return 2;
		case "March":
			return 3;
		case "April":
			return 4;
		case "May":
			return 5;
		case "Jun":
			return 6;
		case "July":
			return 7;
		case "Avgust":
			return 8;
		case "September":
			return 9;
		case "October":
			return 10;
		case "November":
			return 11;
		case "December":
			return 12;
		}

		return 0;
	}

	public Double getTotalWorkerHoursPerMonth( Long idWorker, String monthName) {
		Double total = 0.0;
		Integer month = this.getNumberOfMonth(monthName);
		List<RaspberryHaircut> haircuts = this.haircutRepo.findByWorkerId(idWorker);
		for (RaspberryHaircut haircut : haircuts) {
			if (haircut.getDateOfHaircut().getMonthValue() == month) {
				if (haircut.getStartHour() == null && haircut.getEndHour() == null) {
					continue;
				}
				Long workingHaircut = Duration.between(haircut.getStartHour(), haircut.getEndHour()).toHours();
				total += workingHaircut;
			}
		}
		return total;
	}

	public RaspberryHaircut addHaircut(RaspberryHaircut haircut) {
		return this.haircutRepo.save(haircut);
	}
}
