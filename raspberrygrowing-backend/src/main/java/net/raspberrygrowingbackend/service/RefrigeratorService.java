package net.raspberrygrowingbackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.Refrigerator;
import net.raspberrygrowingbackend.repository.RefrigeratorRepository;

@Service
public class RefrigeratorService {

	@Autowired
	private RefrigeratorRepository refrigeratorRepo;
	
	public Refrigerator getById(Long id) {
		return this.refrigeratorRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Refrigerator doesn't exist with id = " + id));
	}
	
	public List<Refrigerator> findAll(){
		return this.refrigeratorRepo.findAll();
	}
	
	public Refrigerator addRefrigerator(Refrigerator refrigerator) {
		return this.refrigeratorRepo.save(refrigerator);
	}
	
	public Refrigerator updateRefrigerator(Long id, Refrigerator refrigerator) {
		Refrigerator refrigeratorForUpdate = this.getById(id);
		
		refrigeratorForUpdate.setAddress(refrigerator.getAddress());
		refrigeratorForUpdate.setName(refrigerator.getName());
		refrigeratorForUpdate.setPhone(refrigerator.getPhone());
		
		return this.refrigeratorRepo.save(refrigeratorForUpdate);
	}
	
	public void deleteRefrigerator(Long id) {
		this.refrigeratorRepo.deleteById(id);
	}
}
