package net.raspberrygrowingbackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryFertilizer;
import net.raspberrygrowingbackend.repository.RaspberryFertilizerRepository;

@Service
public class RaspberryFertilizerService {
	
	@Autowired
	private RaspberryFertilizerRepository fertilizerRepo;
	
	
	public List<RaspberryFertilizer> findFertilizers(Long idGrower){
		if ( idGrower == 0 ) {
			return this.fertilizerRepo.findAll();
		} else {
			return this.fertilizerRepo.findFertilizersByRaspberryGrowerId(idGrower);
		}
	}
	
	public RaspberryFertilizer getFertilizer( Long id) {
		return this.fertilizerRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Fertilizer doesn't exist with id = " + id));
	}
	
	public RaspberryFertilizer addFertilizer(RaspberryFertilizer fertilizer) {
		return this.fertilizerRepo.save(fertilizer);
	}
	
	public RaspberryFertilizer updateFertilizer( Long id, RaspberryFertilizer fertilizer) {
		RaspberryFertilizer fertilizerForUpdate = this.getFertilizer(id);
		
		fertilizerForUpdate.setKindOfFertilizer(fertilizer.getKindOfFertilizer());
		fertilizerForUpdate.setDescription(fertilizer.getDescription());
		fertilizerForUpdate.setQuantityInStore(fertilizer.getQuantityInStore());
		fertilizerForUpdate.setUnitOfMeasurementOfQuantity(fertilizer.getUnitOfMeasurementOfQuantity());
		return this.fertilizerRepo.save(fertilizerForUpdate);
	}
	
	public RaspberryFertilizer investmentInFertilizer( Long id, Double purchasedQuantity) {
		RaspberryFertilizer investmentFertilizer = this.getFertilizer(id);
		
		investmentFertilizer.setQuantityInStore(investmentFertilizer.getQuantityInStore() + purchasedQuantity);
		
		return this.fertilizerRepo.save(investmentFertilizer);
	}
	
	public RaspberryFertilizer useFertilizerInFertilization( Long id, Double usingQuantityOfFertilizer) {
		RaspberryFertilizer usingFertilizer  = this.getFertilizer(id);
		
		usingFertilizer.setQuantityInStore(usingFertilizer.getQuantityInStore() - usingQuantityOfFertilizer);
		
		return this.fertilizerRepo.save(usingFertilizer);
	}
	
	public void deleteFertilizer( Long id) {
		this.fertilizerRepo.deleteById(id);
	}
}
