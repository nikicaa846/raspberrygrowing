package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RefrigeratedRemoval;
import net.raspberrygrowingbackend.models.RefrigeratorPayment;
import net.raspberrygrowingbackend.repository.RefrigeratedPaymentRepository;
import net.raspberrygrowingbackend.repository.RefrigeratedRemovalRepository;

@Service
public class RefrigeratedPaymentService {

	@Autowired
	private RefrigeratedPaymentRepository paymentRepo;
	
	@Autowired
	private RefrigeratedRemovalRepository removalRepo;
	
	public Double getWeight(LocalDate date, String kindOfRaspberry) {
		double weight = 0;
		int year = date.getYear();
		List<RefrigeratedRemoval> removals = this.removalRepo.findAll();
		for ( RefrigeratedRemoval removal: removals) {
			if ( removal.getDateOfRemoval().getYear() == year && kindOfRaspberry.equals(removal.getKindOfRaspberry())) {
				weight += removal.getWeight();
			}
		}	
		return weight;
	}
	
	public Double setPrice(Double weight, Double pricePerKg) {
		double price = weight * pricePerKg;
		return Math.round( price * 100.0) / 100.0 ;
	}
	
	public RefrigeratorPayment addPayment( RefrigeratorPayment payment) {
		payment.setWeight(getWeight(payment.getDateOfPayment(), payment.getKindOfRaspberry()));
		payment.setPrice(setPrice(payment.getWeight(), payment.getPrice()));
		return this.paymentRepo.save(payment);
	}
	
	public List<RefrigeratorPayment> findPayments(Integer year, Long idPlantation) {
		List<RefrigeratorPayment> payments = this.paymentRepo.findByPlantationId(idPlantation);
		ArrayList<RefrigeratorPayment> newPaymentList = new ArrayList<RefrigeratorPayment>();
		for ( RefrigeratorPayment payment: payments ) {
			if ( payment.getDateOfPayment().getYear() == year ) {
				newPaymentList.add(payment);
			}
		}
		if ( newPaymentList.size() == 0 )
			throw new ResourceNotFoundException("No payments for this year!");
		return newPaymentList;
	}
	
	public Double getTotalPaymentForPlantation(Long idPlantation, Integer year) {
		double total = 0;
		List<RefrigeratorPayment>payments = this.paymentRepo.findByPlantationId(idPlantation);
		for ( RefrigeratorPayment payment: payments ) {
			if ( payment.getDateOfPayment().getYear() == year ) {
				total += payment.getPrice();
			}
		}
		
		return total;
	}
}
