package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.models.RaspberryInvestment;
import net.raspberrygrowingbackend.repository.RaspberryInvestmentRepository;

@Service
public class RaspberryInvestmentService {

	@Autowired
	private RaspberryInvestmentRepository investmentRepo;
	
	public RaspberryInvestment addInvestment(RaspberryInvestment investment) {
		return this.investmentRepo.save(investment);
	}
	
	public List<RaspberryInvestment> findInvestmentsByDateAndGrower(LocalDate date, Long idGrower){
		return this.investmentRepo.findInestmentsByDateOfInvestmentAndRaspberryGrowerId(date, idGrower);
	}
	
	public Double getTotalInvestmentForYear(Integer year, Long idGrower) {
		Double total = 0.0;
		List<RaspberryInvestment> investments = this.investmentRepo.findInvestmetsByRaspberryGrowerId(idGrower);
		for ( RaspberryInvestment investment: investments) {
			if ( investment.getDateOfInvestment().getYear() == year) {
				total += investment.getPaid();
			}
		}
		return total;
	}
}
