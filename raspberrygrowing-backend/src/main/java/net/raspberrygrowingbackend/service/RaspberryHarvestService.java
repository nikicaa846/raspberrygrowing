package net.raspberrygrowingbackend.service;	

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryHarvest;
import net.raspberrygrowingbackend.repository.RaspberryHarvestRepository;

@Service
public class RaspberryHarvestService {

	
	@Autowired
	private RaspberryHarvestRepository harvestRepo;

	public Integer getNumberOfMonth(String monthName) {
		switch(monthName) {
		case "January" : return 1;
		case "February" : return 2;
		case "March" : return 3;
		case "April" : return 4;
		case "May" : return 5;
		case "Jun" : return 6;
		case "July" : return 7;
		case "Avgust" : return 8;
		case "September" : return 9;
		case "October" : return 10;
		case "November" : return 11;
		case "December" : return 12;
		}

		return 0;
	}
	
	public List<RaspberryHarvest> findHarvestByDateAndPlantation(LocalDate date, Long idPlantation) {
		if(this.harvestRepo.findHarvestByDateOfHarvestAndPlantationId(date, idPlantation).size() == 0)
			throw new ResourceNotFoundException("No harvest with this date!");
		return this.harvestRepo.findHarvestByDateOfHarvestAndPlantationId(date, idPlantation);
	}
	
	public Double getTotalWorkerHoursPerMonth( Long idWorker, String monthName) {
		Double total = 0.0;
		Integer month = getNumberOfMonth(monthName);
		List<RaspberryHarvest> harvestes = this.harvestRepo.findByWorkerId(idWorker);
		for ( RaspberryHarvest harvest: harvestes) {
			if ( harvest.getDateOfHarvest().getMonthValue() == month ) {
				if (harvest.getStartHour() == null && harvest.getEndHour() == null) {
					continue;
				}
					Long workingHarvest  = Duration.between(harvest.getStartHour(), harvest.getEndHour()).toHours();
					total += workingHarvest;
			}
		}
		return total;
	}
	
	public RaspberryHarvest addHarvest( RaspberryHarvest harvest ) {
		return this.harvestRepo.save(harvest);
	}
}
