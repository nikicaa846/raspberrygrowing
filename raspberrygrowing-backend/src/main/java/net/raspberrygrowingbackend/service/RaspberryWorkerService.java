package net.raspberrygrowingbackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.exception.ResourceNotFoundException;
import net.raspberrygrowingbackend.models.RaspberryWorker;
import net.raspberrygrowingbackend.repository.RaspberryWorkerRepository;

@Service
public class RaspberryWorkerService {

	@Autowired
	private RaspberryWorkerRepository workerRepo;
	
	public RaspberryWorker getById( Long id) {
		return this.workerRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Worker doesn't exist with id = " + id));
	}
	
	public List<RaspberryWorker> findWorkers(Long idGrower){
		if ( idGrower == 0 ) {
			return this.workerRepo.findAll();
		} else {
			return this.workerRepo.findWorkersByRaspberryGrowerId(idGrower);
		}
	}
	
	public RaspberryWorker addWorker( RaspberryWorker worker) {
		return this.workerRepo.save(worker);
	}
	
	public RaspberryWorker updateWorker( Long id, RaspberryWorker worker) {
		RaspberryWorker workerForUpdate = this.getById(id);
		
		workerForUpdate.setName(worker.getName());
		workerForUpdate.setSurname(worker.getSurname());
		workerForUpdate.setPhone(worker.getPhone());
		
		return this.workerRepo.save(workerForUpdate);
	}
	
	public void deleteWorker( Long id) {
		this.workerRepo.deleteById(id);
	}
	
}
