package net.raspberrygrowingbackend.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.raspberrygrowingbackend.models.InvestmentInRaspberryFertilizer;
import net.raspberrygrowingbackend.repository.InvestmentInRaspberryFertilizerRepository;

@Service
public class InvestmentInRaspberryFertilizerService {

	@Autowired
	private InvestmentInRaspberryFertilizerRepository investmentInFertilizerRepo;
	
	@Autowired
	private RaspberryFertilizerService fertilizerService;
	
	public List<InvestmentInRaspberryFertilizer> findInvestmentsByDateAndGrower(LocalDate date, Long idGrower){
		return this.investmentInFertilizerRepo.findInvestmentsByDateOfInvestmentAndFertilizerRaspberryGrowerId(date, idGrower);
	}
	
	public Double setPrice(Double paid, Double quantity, String unitOfMeasurment) {
		if ( unitOfMeasurment.equals("ml") ) {
			return Math.round((( paid * 100 ) / quantity) * 100 ) / 100.0;
		} else if ( unitOfMeasurment.equals("kg") ) {
			return Math.round(( paid / quantity ) * 100 ) / 100.0;
		}
		return null;
	}
	
	public InvestmentInRaspberryFertilizer addInvestmentInFertilizer( InvestmentInRaspberryFertilizer investment) {
		this.fertilizerService.investmentInFertilizer(investment.getFertilizer().getId(), investment.getPurchasedQuantity());
		investment.setPricePerKgOrMl(setPrice(investment.getPaid(), investment.getPurchasedQuantity(), investment.getFertilizer().getUnitOfMeasurementOfQuantity()));
		
		return this.investmentInFertilizerRepo.save(investment);
	}
	
	public Double getTotalInvestmentForYear(Integer year, Long idGrower) {
		Double total = 0.0;
		List<InvestmentInRaspberryFertilizer> investments = this.investmentInFertilizerRepo.findInvestmentsByFertilizerRaspberryGrowerId(idGrower);
		for ( InvestmentInRaspberryFertilizer investment: investments) {
			if ( investment.getDateOfInvestment().getYear() == year) {
				total += investment.getPaid();
			}
		}
		return total;
	}
}
