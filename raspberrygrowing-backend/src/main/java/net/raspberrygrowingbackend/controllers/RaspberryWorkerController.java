package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryWorker;
import net.raspberrygrowingbackend.service.RaspberryWorkerService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/worker")
public class RaspberryWorkerController {

	@Autowired
	private RaspberryWorkerService workerService;
	
	@GetMapping("/{id}")
	public RaspberryWorker getById(@PathVariable Long id) {
		return this.workerService.getById(id);
	}
	
	@GetMapping
	public List<RaspberryWorker> findWorkers(Long idGrower){
		return this.workerService.findWorkers(idGrower);
	}
	
	
	@PostMapping
	public RaspberryWorker addWorker(@RequestBody RaspberryWorker worker) {
		return this.workerService.addWorker(worker);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<RaspberryWorker> updateWorker(@PathVariable Long id, @RequestBody RaspberryWorker worker){
		return ResponseEntity.ok(this.workerService.updateWorker(id, worker));
	}
	
	@DeleteMapping("/{id}")
	public void deleteWorker(@PathVariable Long id) {
		this.workerService.deleteWorker(id);
	}
}
