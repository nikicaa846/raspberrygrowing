package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryHarvest;
import net.raspberrygrowingbackend.service.RaspberryHarvestService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/harvest")
public class RaspberryHarvestController {

	@Autowired
	private RaspberryHarvestService harvestService;
	
	@GetMapping
	public List<RaspberryHarvest> findHarvestByDateAndPlantation(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
			Long idPlantation){
		return this.harvestService.findHarvestByDateAndPlantation(date, idPlantation);
	}
	
	@GetMapping("/worker")
	public Double getTotalWorkerHoursPerMonth(@RequestParam Long idWorker , @RequestParam String monthName) {
		return this.harvestService.getTotalWorkerHoursPerMonth(idWorker, monthName);
	}
	
	@PostMapping
	public RaspberryHarvest addHarvest(@RequestBody RaspberryHarvest harvest) {
		return this.harvestService.addHarvest(harvest);
	}
}
