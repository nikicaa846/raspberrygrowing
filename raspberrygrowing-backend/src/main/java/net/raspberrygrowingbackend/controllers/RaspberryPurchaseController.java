package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryPurchase;
import net.raspberrygrowingbackend.service.RaspberryPurchaseService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/purchase")
public class RaspberryPurchaseController {

	@Autowired
	private RaspberryPurchaseService purchaseService;
	
	@GetMapping
	public List<RaspberryPurchase> findByDateAndPlantation(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date, 
			@RequestParam Long idPlantation){
		return this.purchaseService.findByDateAndPlantation(date, idPlantation);
	}
	
	@GetMapping("/earnings")
	public double getTotalEarnings(@RequestParam Integer year, @RequestParam Long idPlantation) {
		return this.purchaseService.getTotalEarnings(year, idPlantation);
	}
	
	@GetMapping("/weight")
	public double getTotalWeight(@RequestParam Integer year, @RequestParam Long idPlantation, @RequestParam String kind) {
		return this.purchaseService.getTotalPurchaseWeight(year, idPlantation, kind);
	}
	
	@PostMapping
	public RaspberryPurchase addPurchase(@RequestBody RaspberryPurchase purchase) {
		return this.purchaseService.addPurchase(purchase);
	}
}
