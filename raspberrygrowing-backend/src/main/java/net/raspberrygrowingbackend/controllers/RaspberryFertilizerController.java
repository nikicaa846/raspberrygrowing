package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryFertilizer;
import net.raspberrygrowingbackend.service.RaspberryFertilizerService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/fertilizer")
public class RaspberryFertilizerController {

	@Autowired
	private RaspberryFertilizerService fertilizerService;
	
	@GetMapping
	public List<RaspberryFertilizer> findFertilizers(@RequestParam Long idGrower){
		return this.fertilizerService.findFertilizers(idGrower);
	}
	
	@GetMapping("/{id}")
	public RaspberryFertilizer getFertilizer(@PathVariable Long id) {
		return this.fertilizerService.getFertilizer(id);
	}
	
	@PostMapping
	public RaspberryFertilizer addFertilizer(@RequestBody RaspberryFertilizer fertilizer) {
		return this.fertilizerService.addFertilizer(fertilizer);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<RaspberryFertilizer> updateFertilizer(@PathVariable Long id, @RequestBody RaspberryFertilizer fertilizer){
		return ResponseEntity.ok(this.fertilizerService.updateFertilizer(id, fertilizer));
	}
	
	@DeleteMapping("/{id}")
	public void deleteFertilizer(@PathVariable Long id) {
		this.fertilizerService.deleteFertilizer(id);
	}
}
