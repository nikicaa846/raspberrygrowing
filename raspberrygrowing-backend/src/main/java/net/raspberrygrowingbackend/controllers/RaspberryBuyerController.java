package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryBuyer;
import net.raspberrygrowingbackend.service.RaspberryBuyerService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/buyer")
public class RaspberryBuyerController {

	@Autowired
	private RaspberryBuyerService buyerService;
	
	@GetMapping("/{id}")
	public RaspberryBuyer getBuyer(@PathVariable Long id) {
		return this.buyerService.getBuyer(id);
	}
	
	@GetMapping
	public List<RaspberryBuyer> findBuyers(@RequestParam Long idGrower){
		return this.buyerService.findBuyers(idGrower);
	}
	
	@PostMapping
	public RaspberryBuyer addBuyer( @RequestBody RaspberryBuyer buyer ) {
		return this.buyerService.addBuyer(buyer);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<RaspberryBuyer> updateBuyer(@PathVariable Long id, @RequestBody RaspberryBuyer buyer){
		return ResponseEntity.ok(this.buyerService.updateBuyer(id, buyer));
	}
	
	@DeleteMapping("/{id}")
	public void deleteBuyer(@PathVariable Long id) {
		this.buyerService.deleteBuyer(id);
	}
}
