package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RefrigeratorPayment;
import net.raspberrygrowingbackend.service.RefrigeratedPaymentService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/payment")
public class RefrigeratedPaymentController {

	@Autowired
	private RefrigeratedPaymentService paymentService;
	
	@GetMapping
	public List<RefrigeratorPayment> findPayments(@RequestParam Integer year, @RequestParam Long idPlantation){
		return this.paymentService.findPayments(year, idPlantation);
	}
	
	@GetMapping("/plantation")
	public Double getTotalPaymentForPlantation(@RequestParam Long idPlantation, @RequestParam Integer year) {
		return this.paymentService.getTotalPaymentForPlantation(idPlantation, year);
	}
	
	@PostMapping
	public RefrigeratorPayment addPayment(@RequestBody RefrigeratorPayment payment) {
		return this.paymentService.addPayment(payment);
	}
}
