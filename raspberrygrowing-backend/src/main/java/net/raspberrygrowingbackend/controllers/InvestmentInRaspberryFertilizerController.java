package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.InvestmentInRaspberryFertilizer;
import net.raspberrygrowingbackend.service.InvestmentInRaspberryFertilizerService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/raspberryinvestmentinfertilizer")
public class InvestmentInRaspberryFertilizerController {

	
	@Autowired
	private InvestmentInRaspberryFertilizerService investmentInFertilzierService;
	
	@GetMapping
	public List<InvestmentInRaspberryFertilizer> findInvestmentByDateAndGrower(
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate  date, @RequestParam Long idGrower){
		return this.investmentInFertilzierService.findInvestmentsByDateAndGrower(date, idGrower);
	}
	
	@GetMapping("/total")
	public Double getTotalInvestmentForYear(@RequestParam Integer year,@RequestParam Long idGrower) {
		return this.investmentInFertilzierService.getTotalInvestmentForYear(year, idGrower);
	}
	
	@PostMapping
	public InvestmentInRaspberryFertilizer addInvestmentInFertilizer( @RequestBody InvestmentInRaspberryFertilizer investment) {
		return this.investmentInFertilzierService.addInvestmentInFertilizer(investment);
	}
}
