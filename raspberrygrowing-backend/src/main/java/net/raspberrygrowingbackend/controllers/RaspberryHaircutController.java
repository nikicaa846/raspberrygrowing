package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryHaircut;
import net.raspberrygrowingbackend.service.RaspberryHaricutService;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/haircut")
public class RaspberryHaircutController {

	@Autowired
	private RaspberryHaricutService haircutService;
	
	@GetMapping
	public List<RaspberryHaircut> findHaircutsByDateAndPlantation(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
			@RequestParam Long idPlantation){
		return this.haircutService.findHaircutByDateAndPlantation(date, idPlantation);
	}
	
	@GetMapping("/worker")
	public Double getTotalWorkerHoursPerMonth(@RequestParam Long idWorker , @RequestParam String monthName) {
		return this.haircutService.getTotalWorkerHoursPerMonth(idWorker, monthName);
	}
	
	@PostMapping
	public RaspberryHaircut addHaircut(@RequestBody RaspberryHaircut haircut) {
		return this.haircutService.addHaircut(haircut);
	}
}
