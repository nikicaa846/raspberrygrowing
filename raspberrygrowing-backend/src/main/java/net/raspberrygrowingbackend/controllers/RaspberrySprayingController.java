package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberrySpraying;
import net.raspberrygrowingbackend.service.RaspberrySprayingService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/spraying")
public class RaspberrySprayingController {

	@Autowired
	private RaspberrySprayingService sprayingService;
	
	@GetMapping("/{id}")
	public RaspberrySpraying getById(@PathVariable Long id) {
		return this.sprayingService.getById(id);
	}
	
	@GetMapping
	public List<RaspberrySpraying> findSprayingByDateAndPlantation(
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
			@RequestParam Long idPlantation){
		return this.sprayingService.findSprayingByDateAndPlantation(date, idPlantation);
	}
	
	@GetMapping("/sprayingworker")
	RaspberrySpraying findByDatePlantationPesticide( 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date, 
			@RequestParam Long idPlantation, 
			@RequestParam Long idPesticide) {
		return this.sprayingService.findByDatePlantationPesticide(date, idPlantation, idPesticide);
	}
	
	@PostMapping
	public RaspberrySpraying addSpraying(@RequestBody RaspberrySpraying spraying) {
		return this.sprayingService.addSpraying(spraying);
	}
}
