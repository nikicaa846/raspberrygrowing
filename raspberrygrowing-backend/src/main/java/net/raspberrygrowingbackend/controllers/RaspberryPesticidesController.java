package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryPesticides;
import net.raspberrygrowingbackend.service.RaspberryPesticidesService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/pesticide")
public class RaspberryPesticidesController {

	@Autowired
	private RaspberryPesticidesService pesticidesService;
	
	@GetMapping("/{id}")
	public RaspberryPesticides getPesticide(@PathVariable Long id) {
		return this.pesticidesService.getPesticide(id);
	}
	
	@GetMapping
	public List<RaspberryPesticides> findPesticides(@RequestParam Long idGrower){
		return this.pesticidesService.findPesticides(idGrower);
	}
	
	@PostMapping
	public RaspberryPesticides addPesticide(@RequestBody RaspberryPesticides pesticide) {
		return this.pesticidesService.addPesticide(pesticide);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<RaspberryPesticides> updatePesticide(@PathVariable Long id, @RequestBody RaspberryPesticides pesticide){
		return ResponseEntity.ok(pesticidesService.updatePesticide(id, pesticide));
	}
	
	@DeleteMapping("/{id}")
	public void deletePesticide(@PathVariable Long id) {
		this.pesticidesService.deletePesticide(id);
	}
}
