package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryFertilization;
import net.raspberrygrowingbackend.service.RaspberryFertilizationService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/fertilization")
public class RaspberryFertilizationController {

	@Autowired
	private RaspberryFertilizationService fertilizationService;
	
	@GetMapping("/{id}")
	public RaspberryFertilization getById(@PathVariable Long id) {
		return this.fertilizationService.getById(id);
	}
	
	@GetMapping("/fertilizationworker")
	public RaspberryFertilization findByDateAndPlantationAndFertilizer( 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate  date,
			@RequestParam Long idPlantation,
			@RequestParam Long idFertilizer) {
		return this.fertilizationService.findByDateAndPlantationAndFertilizer(date, idPlantation, idFertilizer);
	}
	
	@GetMapping
	public List<RaspberryFertilization> findFertilizationByDateAndPlantation(
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate  date,
			@RequestParam Long idPlantation){
		return this.fertilizationService.findFertilizationByDateAndPlantation(date, idPlantation);
	}
	
	@PostMapping
	public RaspberryFertilization addFertilization(@RequestBody RaspberryFertilization fertilization) {
		return this.fertilizationService.addFertilization(fertilization);
	}
}
