package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryFertilizationWorker;
import net.raspberrygrowingbackend.service.RaspberryFertilizationWorkerService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/fertilizationworker")
public class RaspberryFertilizationWorkerController {
	
	@Autowired
	private RaspberryFertilizationWorkerService fertilizationWorkerService;
	
	@GetMapping
	public List<RaspberryFertilizationWorker> findByFertilization(@RequestParam Long id){
		return this.fertilizationWorkerService.findByFertilziation(id);
	}
	
	@GetMapping("/worker")
	public Double getTotalWorkerHoursPerMonth(@RequestParam Long idWorker , @RequestParam String monthName) {
		return this.fertilizationWorkerService.getTotalWorkerHoursPerMonth(idWorker, monthName);
	}
	
	@PostMapping
	public RaspberryFertilizationWorker addFertilizationWorker( @RequestBody RaspberryFertilizationWorker fertilizationWorker) {
		return this.fertilizationWorkerService.addFertilizationWorker(fertilizationWorker);
	}
}
