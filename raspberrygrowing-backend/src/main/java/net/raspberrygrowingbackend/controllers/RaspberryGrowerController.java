package net.raspberrygrowingbackend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryGrower;
import net.raspberrygrowingbackend.service.RaspberryGrowerService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/grower")
public class RaspberryGrowerController {

	@Autowired
	private RaspberryGrowerService raspberryGrowerService;
	
	@GetMapping("/{id}")
	public RaspberryGrower getById(@PathVariable Long id) {
		return this.raspberryGrowerService.getById(id);
	}
	
	@GetMapping("/profit")
	public Double getProfit(@RequestParam Long id, @RequestParam Integer year) {
		return this.raspberryGrowerService.getProfit(id, year);
	}
	
	@PostMapping
	public RaspberryGrower addGrower(@RequestBody RaspberryGrower grower) {
		return this.raspberryGrowerService.addGrower(grower);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<RaspberryGrower> updateGrower(@PathVariable Long id, @RequestBody RaspberryGrower grower){
		return ResponseEntity.ok(this.raspberryGrowerService.updateGrower(id, grower));
	}
	
	@DeleteMapping("/{id}")
	public void deleteGrower(@PathVariable Long id) {
		this.raspberryGrowerService.deleteGrower(id);
	}
}
