package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryInvestment;
import net.raspberrygrowingbackend.service.RaspberryInvestmentService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/investment")
public class RaspberryInvestmentController {

	@Autowired
	private RaspberryInvestmentService investmentService;
	
	@GetMapping
	public List<RaspberryInvestment> findInvestmentByDateAndGrower(
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate  date,
			@RequestParam Long idGrower){
		return this.investmentService.findInvestmentsByDateAndGrower(date, idGrower);
	}
	
	@GetMapping("/total")
	public Double getTotalInvestmentForYear(@RequestParam Integer year,@RequestParam Long idGrower) {
		return this.investmentService.getTotalInvestmentForYear(year, idGrower);
	}
	
	@PostMapping
	public RaspberryInvestment addInvestment(@RequestBody RaspberryInvestment investment) {
		return this.investmentService.addInvestment(investment);
	}
}
