package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RefrigeratedRemoval;
import net.raspberrygrowingbackend.service.RefrigeratedRemovalService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/removal")
public class RefrigeratedRemovalController {

	@Autowired
	private RefrigeratedRemovalService removalService;
	
	@GetMapping
	public List<RefrigeratedRemoval> 
	findByKindOfRaspberryAndDate(@RequestParam Long idPlantation, @RequestParam String raspberry, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date){
		return this.removalService.findByKindOfRaspberryDateAndPlantation(idPlantation, raspberry, date);
	}
	
	@GetMapping("/weight")
	public Double getTotalWeightOfVilamet(@RequestParam Long idPlantation, @RequestParam Integer year, @RequestParam String kind) {
		return this.removalService.getTotalWeight(idPlantation, year, kind);
	}
	
	@PostMapping
	public RefrigeratedRemoval addRemoval(@RequestBody RefrigeratedRemoval removal) {
		return this.removalService.addRemoval(removal);
	}
}
