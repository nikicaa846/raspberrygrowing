package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RefrigeratedCrates;
import net.raspberrygrowingbackend.service.RefrigeratedCratesService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/crates")
public class RefrigeratedCratesController {

	@Autowired
	private RefrigeratedCratesService cratesService;
	
	@GetMapping
	public List<RefrigeratedCrates> findCrates(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date, 
			@RequestParam Long idPlantation ){
		return this.cratesService.findCrates(date, idPlantation);
	}
	
	@PostMapping
	public RefrigeratedCrates addCrates(@RequestBody RefrigeratedCrates crates) {
		return this.cratesService.addCrates(crates);
	}
}
