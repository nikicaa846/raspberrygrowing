package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryPlantation;
import net.raspberrygrowingbackend.service.RaspberryPlantationService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/plantation")
public class RaspberryPlantationController {

	@Autowired
	private RaspberryPlantationService plantationService;
	
	@GetMapping("/{id}")
	public RaspberryPlantation getPlantation(@PathVariable Long id) {
		return this.plantationService.getPlantationById(id);
	}
	
	@GetMapping
	public List<RaspberryPlantation> findPlantations(@RequestParam Long idGrower){
		return this.plantationService.findPlantations(idGrower);
	}
	
	@PostMapping
	public RaspberryPlantation addPlantation(@RequestBody RaspberryPlantation plantation) {
		return this.plantationService.addPlantation(plantation);
	}
	
	@PutMapping("/{id}")
	public RaspberryPlantation updatePlantation(@PathVariable Long id, @RequestBody RaspberryPlantation plantation) {
		return this.plantationService.updatePlantation(id, plantation);
	}
	
	@DeleteMapping("/{id}")
	public void deletePlantation(@PathVariable Long id) {
		this.plantationService.deletePlantation(id);
	}
}
