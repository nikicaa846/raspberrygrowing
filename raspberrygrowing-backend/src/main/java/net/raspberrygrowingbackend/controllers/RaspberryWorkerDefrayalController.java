package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryWorkerDefrayal;
import net.raspberrygrowingbackend.service.RaspberryWorkerDefrayalService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/workerdefrayal")
public class RaspberryWorkerDefrayalController {

	@Autowired
	private RaspberryWorkerDefrayalService defroyalService;
	
	@GetMapping
	public List<RaspberryWorkerDefrayal> findDefrayalByDateAndPlantation(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate date,
				@RequestParam Long idPlantation){
		return this.defroyalService.findWorkerDefrayalByDateAndPlantation(date, idPlantation);
	}
	
	@GetMapping("/plantation")
	public Double getTotalDefrayalForPlantation(@RequestParam Long idPlantation, @RequestParam Integer year) {
		return this.defroyalService.getTotalDefrayalForWorkers(idPlantation, year);
	}
	
	@GetMapping("/worker")
	public List<RaspberryWorkerDefrayal> findDefroyalsByWorkerId(@RequestParam Long idWorker, 
			@RequestParam Long idPlantation, @RequestParam Integer year){
		return this.defroyalService.findDefrayalsWorkerPlantationAndYear(idWorker, idPlantation, year);
	}
	
	@PostMapping
	public RaspberryWorkerDefrayal addDefrayal(@RequestBody RaspberryWorkerDefrayal defrayal) {
		return this.defroyalService.addDefrayal(defrayal);
	}
}
