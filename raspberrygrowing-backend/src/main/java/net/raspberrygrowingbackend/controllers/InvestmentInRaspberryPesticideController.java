package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.InvestmentInRaspberryPesticide;
import net.raspberrygrowingbackend.service.InvestmentInRaspberryPesticideService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/raspberryinvestmentinpesticide")
public class InvestmentInRaspberryPesticideController {

	@Autowired
	private InvestmentInRaspberryPesticideService investmentInPesticideService;
	
	@GetMapping
	public List<InvestmentInRaspberryPesticide> findInvestmentByDateAndGrower(
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate  date, @RequestParam Long idGrower){
		return this.investmentInPesticideService.findInvestmentsByDateAndGrower(date, idGrower);
	}
	
	@GetMapping("/total")
	public Double getTotalInvestmentForYear(@RequestParam Integer year,@RequestParam Long idGrower) {
		return this.investmentInPesticideService.getTotalInvestmentForYear(year, idGrower);
	}
	
	@PostMapping
	public InvestmentInRaspberryPesticide addInvestmentInPesticide( @RequestBody InvestmentInRaspberryPesticide investment) {
		return this.investmentInPesticideService.addInvestmentInPesticide(investment);
	}
	
}
