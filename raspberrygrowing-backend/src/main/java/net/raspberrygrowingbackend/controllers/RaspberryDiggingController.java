package net.raspberrygrowingbackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberryDigging;
import net.raspberrygrowingbackend.service.RaspberryDiggingService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/digging")
public class RaspberryDiggingController {

	@Autowired
	private RaspberryDiggingService diggingService;
	
	@GetMapping
	public List<RaspberryDigging> findByDateAndPlantation(
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate date, 
			@RequestParam Long idPlantation){
		return this.diggingService.findDiggingByDateAndPlantation(date, idPlantation);
	}
	
	@GetMapping("/worker")
	public Double getTotalWorkerHoursPerMonth(@RequestParam Long idWorker , @RequestParam String monthName) {
		return this.diggingService.getTotalWorkerHoursPerMonth(idWorker, monthName);
	}
	
	@PostMapping
	public RaspberryDigging addDigging(@RequestBody RaspberryDigging digging ) {
		return this.diggingService.addDigging(digging);
	}
}
