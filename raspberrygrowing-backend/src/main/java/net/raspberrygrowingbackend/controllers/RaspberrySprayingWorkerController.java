package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.RaspberrySprayingWorker;
import net.raspberrygrowingbackend.service.RaspberrySprayingWorkerService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/sprayingworker")
public class RaspberrySprayingWorkerController {

	@Autowired
	private RaspberrySprayingWorkerService sprayingWorkerService;
	
	@GetMapping
	public List<RaspberrySprayingWorker> findBySpraying(@RequestParam Long id){
		return this.sprayingWorkerService.findBySpraying(id);
	}
	
	@GetMapping("/worker")
	public Double getTotalWorkerHoursPerMonth(@RequestParam Long idWorker , @RequestParam String monthName) {
		return this.sprayingWorkerService.getTotalWorkerHoursPerMonth(idWorker, monthName);
	}
	
	@PostMapping
	public RaspberrySprayingWorker addSprayingWorker(@RequestBody RaspberrySprayingWorker sprayingWorker) {
		return this.sprayingWorkerService.addSprayingWorker(sprayingWorker);
	}
}
