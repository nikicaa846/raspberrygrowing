package net.raspberrygrowingbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.raspberrygrowingbackend.models.Refrigerator;
import net.raspberrygrowingbackend.service.RefrigeratorService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/refrigerator")
public class RefrigeratorController {

	@Autowired
	private RefrigeratorService refrigeratorService;
	
	@GetMapping("/{id}")
	public Refrigerator getById(@PathVariable Long id) {
		return this.refrigeratorService.getById(id);
	}
	
	@GetMapping
	public List<Refrigerator> findAll(){
		return this.refrigeratorService.findAll();
	}
	
	@PostMapping
	public Refrigerator addRefrigerator(@RequestBody Refrigerator refrigerator) {
		return this.refrigeratorService.addRefrigerator(refrigerator);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Refrigerator> updateRefrigerator(@PathVariable Long id, @RequestBody Refrigerator refrigerator){
		return ResponseEntity.ok(this.refrigeratorService.updateRefrigerator(id, refrigerator));
	}
	
	@DeleteMapping("/{id}")
	public void deleteRefrigerator(@PathVariable Long id) {
		this.refrigeratorService.deleteRefrigerator(id);
	}
}
