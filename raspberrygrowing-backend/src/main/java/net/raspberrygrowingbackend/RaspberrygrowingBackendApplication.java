package net.raspberrygrowingbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RaspberrygrowingBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RaspberrygrowingBackendApplication.class, args);
	}

}
