package net.raspberrygrowingbackend.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "raspberry_spraying")
@Data
public class RaspberrySpraying {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDate dateOfSpraying;
	
	private Double quantityOfPesticide;
	
	@ManyToOne
	@JoinColumn(name = "id_pesticide", referencedColumnName = "id", nullable = false)
	private RaspberryPesticides pesticide;
	
	@ManyToOne
	@JoinColumn(name = "id_plantation", referencedColumnName = "id", nullable = false)
	private RaspberryPlantation plantation;
	
	@OneToMany(mappedBy = "spraying")
	@JsonIgnore
	private Set<RaspberrySprayingWorker> sprayingWorker;
}
