package net.raspberrygrowingbackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "raspberry_pesticide")
@Data
public class RaspberryPesticides {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String kindOfPesticide;
	
	private String description;
	
	private Double quantityInStore;
	
	private String unitOfMeasurementOfQuantity;
	
	@ManyToOne
	@JoinColumn(name = "id_grower", referencedColumnName = "id", nullable = false)
	private RaspberryGrower raspberryGrower;
	
	@OneToMany(mappedBy = "pesticide")
	@JsonIgnore
	private Set<InvestmentInRaspberryPesticide> investment;
	
	@OneToMany(mappedBy = "pesticide")
	@JsonIgnore
	private Set<RaspberrySpraying> spraying;
}
