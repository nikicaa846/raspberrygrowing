package net.raspberrygrowingbackend.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "refrigerated_removal")
@Data
public class RefrigeratedRemoval {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String kindOfRaspberry;
	
	private Double weight;
	
	private LocalDate dateOfRemoval;
	
	@JsonIgnore
	@OneToMany(mappedBy = "removal")
	private Set<RefrigeratedCrates> crates;
	
	@ManyToOne
	@JoinColumn(name = "id_plantation", referencedColumnName = "id", nullable = false)
	private RaspberryPlantation plantation;
	
	@ManyToOne
	@JoinColumn(name = "id_refrigerator", referencedColumnName = "id", nullable = false)
	private Refrigerator refrigerator; 
}
