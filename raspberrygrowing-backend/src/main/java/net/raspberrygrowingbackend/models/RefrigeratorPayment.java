package net.raspberrygrowingbackend.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "refrigerator_payment")
@Data
public class RefrigeratorPayment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Double weight;
	
	private String kindOfRaspberry;
	
	private LocalDate dateOfPayment;
	
	private Double price;
	
	@ManyToOne
	@JoinColumn(name = "id_plantation",referencedColumnName = "id", nullable = false)
	private RaspberryPlantation plantation;
	
	@ManyToOne
	@JoinColumn(name = "id_refrigerator", referencedColumnName = "id", nullable = false)
	private Refrigerator refrigerator; 
}
