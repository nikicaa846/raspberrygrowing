package net.raspberrygrowingbackend.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "raspberry_investment_in_pesticide")
@Data
public class InvestmentInRaspberryPesticide {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDate dateOfInvestment;
	
	private Double pricePerKgOrMl;
	
	private Double purchasedQuantity;
	
	private Double paid;
	
	@ManyToOne
	@JoinColumn(name = "id_pesticide", referencedColumnName = "id", nullable = false)
	private RaspberryPesticides pesticide;

}
