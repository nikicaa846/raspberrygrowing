package net.raspberrygrowingbackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "raspberry_worker")
@Data
public class RaspberryWorker {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String surname;
	
	private String phone;
	
	@ManyToOne
	@JoinColumn(name = "id_grower", referencedColumnName = "id", nullable = false)
	private RaspberryGrower raspberryGrower;
	
	@OneToMany(mappedBy = "worker")
	@JsonIgnore
	private Set<RaspberryHarvest> harvest;
	
	@OneToMany(mappedBy = "worker")
	@JsonIgnore
	private Set<RaspberryDigging> digging;
	
	@OneToMany(mappedBy = "worker")
	@JsonIgnore
	private Set<RaspberryHaircut> haircut;
	
	@OneToMany(mappedBy = "worker")
	@JsonIgnore
	private Set<RaspberrySprayingWorker> sprayingWorker;
	
	@OneToMany(mappedBy = "worker")
	@JsonIgnore
	private Set<RaspberryFertilizationWorker> fertilizationWorker;
	
	@OneToMany(mappedBy = "worker")
	@JsonIgnore
	private Set<RaspberryWorkerDefrayal> workerDefroyal;
}
