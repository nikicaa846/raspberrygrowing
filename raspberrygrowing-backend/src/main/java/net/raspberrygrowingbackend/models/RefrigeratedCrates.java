package net.raspberrygrowingbackend.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "refrigerated_crates")
@Data
public class RefrigeratedCrates {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer numberTakenCrates;
	
	private Integer numberSubmittedCrates;
	
	@ManyToOne
	@JoinColumn(name = "id_removal", referencedColumnName = "id", nullable = false)
	private RefrigeratedRemoval removal;
}
