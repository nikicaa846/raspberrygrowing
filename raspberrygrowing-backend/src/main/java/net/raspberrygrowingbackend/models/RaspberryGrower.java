package net.raspberrygrowingbackend.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


@Entity
@Table(name = "raspberry_grower")
@Data
public class RaspberryGrower {

	@Id
	private Long id;
	
	private String name;
	
	private String surname;
	
	private LocalDate birthday;
	
	private String phone;
	
	private String address;
	
	@JsonIgnore
	@OneToMany(mappedBy = "raspberryGrower")
	private Set<RaspberryPlantation> plantation;
	
	@JsonIgnore
	@OneToMany(mappedBy = "raspberryGrower")
	private Set<RaspberryWorker> worker;
	
	@JsonIgnore
	@OneToMany(mappedBy = "raspberryGrower")
	private Set<RaspberryInvestment> investment;
	
	@JsonIgnore
	@OneToMany(mappedBy = "raspberryGrower")
	private Set<RaspberryBuyer> buyer;

	@JsonIgnore
	@OneToMany(mappedBy = "raspberryGrower")
	private Set<User> user;

}
