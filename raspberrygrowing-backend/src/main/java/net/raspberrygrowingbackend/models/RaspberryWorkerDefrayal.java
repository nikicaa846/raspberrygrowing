package net.raspberrygrowingbackend.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "raspberry_worker_defrayal")
@Data
public class RaspberryWorkerDefrayal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Double totalHour;
	
	private LocalDate dateOfDefrayal;
	
	private Double salaryForTheMonth;
	
	private Double pricePerHour;
	
	private String defrayalPerMonth;
	
	private String kindOfWork;
	
	@ManyToOne
	@JoinColumn(name = "id_plantation", referencedColumnName = "id", nullable = false)
	private RaspberryPlantation plantation;
	
	@ManyToOne
	@JoinColumn(name = "id_worker", referencedColumnName = "id", nullable = false)
	private RaspberryWorker worker;
}
