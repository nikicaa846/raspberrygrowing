package net.raspberrygrowingbackend.models;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "raspberry_haircut")
@Data
public class RaspberryHaircut {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDate dateOfHaircut;
	
	private LocalTime startHour;
	
	private LocalTime endHour;
	
	@ManyToOne
	@JoinColumn(name = "id_plantation", referencedColumnName = "id", nullable = false)
	private RaspberryPlantation plantation;
	
	@ManyToOne
	@JoinColumn(name = "id_worker", referencedColumnName = "id", nullable = false)
	private RaspberryWorker worker;
}
