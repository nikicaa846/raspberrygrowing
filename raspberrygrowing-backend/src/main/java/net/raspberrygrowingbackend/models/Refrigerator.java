package net.raspberrygrowingbackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "refrigerator")
@Data
public class Refrigerator {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String phone;
	
	private String address;
	
	@JsonIgnore
	@OneToMany(mappedBy = "refrigerator")
	private Set<RefrigeratorPayment> payment;
	
	@JsonIgnore
	@OneToMany(mappedBy = "refrigerator")
	private Set<RefrigeratedRemoval> removal;
}
