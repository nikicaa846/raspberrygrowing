package net.raspberrygrowingbackend.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "raspberry_investment")
@Data
public class RaspberryInvestment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String kindOfInvestment;
	
	private LocalDate dateOfInvestment;
	
	private Double paid;
	
	@ManyToOne
	@JoinColumn(name = "id_grower", referencedColumnName = "id", nullable = false)
	private RaspberryGrower raspberryGrower;
}
