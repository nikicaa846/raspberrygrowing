package net.raspberrygrowingbackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "raspberry_plantation")
@Data
public class RaspberryPlantation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Double size;
	
	private String address;
	
	@OneToOne
	@JoinColumn(name = "id_grower", referencedColumnName = "id", nullable = false)
	private RaspberryGrower raspberryGrower;
	
	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RefrigeratedRemoval> removal;
	
	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RefrigeratorPayment> payment;
	
	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RaspberryPurchase> purchase;
	
	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RaspberryHarvest> harvest;
	
	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RaspberryDigging> digging;
	
	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RaspberryHaircut> haircut;

	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RaspberryFertilization> fertilization;
	
	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RaspberrySpraying> spraying;
	
	@OneToMany(mappedBy = "plantation")
	@JsonIgnore
	private Set<RaspberryWorkerDefrayal> defrayal;
}
