package net.raspberrygrowingbackend.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "raspberry_fertilization")
@Data
public class RaspberryFertilization {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalDate dateOfFertilization;
	
	private Double quantityOfFertilizer;
	
	@ManyToOne
	@JoinColumn(name = "id_plantation", referencedColumnName = "id", nullable = false)
	private RaspberryPlantation plantation;
	
	@OneToMany(mappedBy = "fertilization")
	@JsonIgnore
	private Set<RaspberryFertilizationWorker> fertilizationWorker;
	
	@ManyToOne
	@JoinColumn(name = "id_fertilizer", referencedColumnName = "id", nullable = false)
	private RaspberryFertilizer fertilizer;
}
