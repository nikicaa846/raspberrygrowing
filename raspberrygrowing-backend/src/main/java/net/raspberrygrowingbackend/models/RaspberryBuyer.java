package net.raspberrygrowingbackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "raspberry_buyer")
@Data
public class RaspberryBuyer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String surname;
	
	private String phone;
	
	@ManyToOne
	@JoinColumn(name = "id_grower", referencedColumnName = "id", nullable = false)
	private RaspberryGrower raspberryGrower;
	
	@JsonIgnore
	@OneToMany(mappedBy = "buyer")
	private Set<RaspberryPurchase> purchase;
}
