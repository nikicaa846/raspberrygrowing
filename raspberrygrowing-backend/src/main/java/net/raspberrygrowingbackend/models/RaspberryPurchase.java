package net.raspberrygrowingbackend.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "raspberry_purchase")
@Data
public class RaspberryPurchase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Double weight;
	
	private String kindOfRaspberry;
	
	private Double price;
	
	private LocalDate dateOfPurchase;
	
	private String charged;
	
	@ManyToOne
	@JoinColumn(name = "id_buyer", referencedColumnName = "id", nullable = false)
	private RaspberryBuyer buyer;
	
	@ManyToOne
	@JoinColumn(name = "id_plantation", referencedColumnName = "id", nullable = false)
	private RaspberryPlantation plantation;
	
}
