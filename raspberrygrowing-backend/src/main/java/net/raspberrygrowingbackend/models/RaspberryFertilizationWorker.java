package net.raspberrygrowingbackend.models;

import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "raspberry_fertilization_worker")
@Data
public class RaspberryFertilizationWorker {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private LocalTime startHour;
	
	private LocalTime endHour;
	
	@ManyToOne
	@JoinColumn( name = "id_worker", referencedColumnName = "id", nullable = false)
	private RaspberryWorker worker;
	
	@ManyToOne
	@JoinColumn( name = "id_fertilization", referencedColumnName = "id", nullable = false)
	private RaspberryFertilization fertilization;
	
	
}
