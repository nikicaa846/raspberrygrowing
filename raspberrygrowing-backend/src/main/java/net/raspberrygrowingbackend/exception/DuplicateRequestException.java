package net.raspberrygrowingbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseBody
@ResponseStatus(value = HttpStatus.CONFLICT,
reason = "username already in use")
public class DuplicateRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;
}
