package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RefrigeratedCrates;

@Repository
public interface RefrigeratedCratesRepository extends JpaRepository<RefrigeratedCrates, Long> {

	List<RefrigeratedCrates> findCratesByRemovalDateOfRemovalAndRemovalPlantationId(LocalDate date, Long idPlantation);
}
