package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberrySpraying;

@Repository
public interface RaspberrySprayingRepository extends JpaRepository<RaspberrySpraying, Long> {

	List<RaspberrySpraying> findByDateOfSprayingAndPlantationId(LocalDate date, Long idPlantation);
	
	RaspberrySpraying findByDateOfSprayingAndPlantationIdAndPesticideId(LocalDate date, Long idPlantation, Long idPesticide);
}
