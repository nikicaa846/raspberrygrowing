package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryHaircut;

@Repository
public interface RaspberryHaircutRepository extends JpaRepository<RaspberryHaircut,  Long>{

	List<RaspberryHaircut> findByDateOfHaircutAndPlantationId(LocalDate date, Long idPlantation);
	
	List<RaspberryHaircut> findByWorkerId( Long id);
}
