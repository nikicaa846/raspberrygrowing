package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.InvestmentInRaspberryFertilizer;

@Repository
public interface InvestmentInRaspberryFertilizerRepository extends JpaRepository<InvestmentInRaspberryFertilizer, Long> {

	List<InvestmentInRaspberryFertilizer> findInvestmentsByDateOfInvestmentAndFertilizerRaspberryGrowerId(LocalDate date, Long idGrower);
	
	List<InvestmentInRaspberryFertilizer> findInvestmentsByFertilizerRaspberryGrowerId(Long idGrower);
}
