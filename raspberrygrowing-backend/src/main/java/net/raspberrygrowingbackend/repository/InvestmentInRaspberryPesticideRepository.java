package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.InvestmentInRaspberryPesticide;

@Repository
public interface InvestmentInRaspberryPesticideRepository extends JpaRepository<InvestmentInRaspberryPesticide, Long>{

	List<InvestmentInRaspberryPesticide> findInvestmentsByDateOfInvestmentAndPesticideRaspberryGrowerId(LocalDate date, Long idGrower);
	
	List<InvestmentInRaspberryPesticide> findInvestmentsByPesticideRaspberryGrowerId(Long idGrower);
}
