package net.raspberrygrowingbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryFertilizationWorker;

@Repository
public interface RaspberryFertilizationWorkerRepository extends JpaRepository<RaspberryFertilizationWorker, Long>{

	List<RaspberryFertilizationWorker> findByFertilizationId( Long id);
	
	List<RaspberryFertilizationWorker> findByWorkerId( Long id);
}
