package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryPurchase;

@Repository
public interface RaspberryPurchaseRepository extends JpaRepository<RaspberryPurchase, Long> {

	public List<RaspberryPurchase> findByDateOfPurchaseAndPlantationId(LocalDate date, Long idPlantation);
	
	public List<RaspberryPurchase> findByPlantationId(Long idPlantation);
}
