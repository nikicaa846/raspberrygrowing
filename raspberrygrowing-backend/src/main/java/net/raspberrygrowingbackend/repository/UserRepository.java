package net.raspberrygrowingbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	public User getByUsername(String username);
}
