package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryFertilization;

@Repository
public interface RaspberryFertilizationRepository extends JpaRepository<RaspberryFertilization, Long>{

	public List<RaspberryFertilization> findByDateOfFertilizationAndPlantationId(LocalDate date, Long idPlantation);
	
	public RaspberryFertilization findByDateOfFertilizationAndPlantationIdAndFertilizerId(LocalDate date, Long idPlantation,  Long idFertilizer);
}
