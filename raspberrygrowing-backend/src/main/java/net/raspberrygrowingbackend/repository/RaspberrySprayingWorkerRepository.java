package net.raspberrygrowingbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberrySprayingWorker;

@Repository
public interface RaspberrySprayingWorkerRepository extends JpaRepository<RaspberrySprayingWorker, Long> {

	List<RaspberrySprayingWorker> findBySprayingId( Long id);
	
	List<RaspberrySprayingWorker> findByWorkerId( Long id);
}
