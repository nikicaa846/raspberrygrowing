package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryDigging;

@Repository
public interface RaspberryDigginRepository extends JpaRepository<RaspberryDigging, Long> {

	List<RaspberryDigging> findByDateOfDiggingAndPlantationId(LocalDate date, Long idPlantation);
	
	List<RaspberryDigging> findByWorkerId( Long id);
}
