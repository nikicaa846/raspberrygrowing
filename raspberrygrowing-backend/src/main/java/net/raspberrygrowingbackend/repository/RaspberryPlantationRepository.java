package net.raspberrygrowingbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryPlantation;

@Repository
public interface RaspberryPlantationRepository extends JpaRepository<RaspberryPlantation, Long> {

	List<RaspberryPlantation> findPlantationByRaspberryGrowerId(Long idGrower);
}
