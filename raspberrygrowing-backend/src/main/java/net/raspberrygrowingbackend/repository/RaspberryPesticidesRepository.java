package net.raspberrygrowingbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryPesticides;

@Repository
public interface RaspberryPesticidesRepository extends JpaRepository<RaspberryPesticides, Long> {

	List<RaspberryPesticides> findPesticidesByRaspberryGrowerId(Long idGrower);
}
