package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryHarvest;

@Repository
public interface RaspberryHarvestRepository extends JpaRepository<RaspberryHarvest, Long> {
	
	List<RaspberryHarvest> findHarvestByDateOfHarvestAndPlantationId(LocalDate date, Long idPlantation);
	
	List<RaspberryHarvest> findByWorkerId( Long id);

}
