package net.raspberrygrowingbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryFertilizer;

@Repository
public interface RaspberryFertilizerRepository extends JpaRepository<RaspberryFertilizer, Long> {

	List<RaspberryFertilizer> findFertilizersByRaspberryGrowerId(Long idGrower);
}
