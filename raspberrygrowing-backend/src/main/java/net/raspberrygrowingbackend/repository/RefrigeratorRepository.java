package net.raspberrygrowingbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.Refrigerator;

@Repository
public interface RefrigeratorRepository extends JpaRepository<Refrigerator, Long> {
}
