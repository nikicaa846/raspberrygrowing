package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RefrigeratedRemoval;

@Repository
public interface RefrigeratedRemovalRepository extends JpaRepository<RefrigeratedRemoval, Long>{

	List<RefrigeratedRemoval> findByPlantationIdAndKindOfRaspberryAndDateOfRemoval(Long idPlantation, String kindOfRaspberry, LocalDate date);
	
	List<RefrigeratedRemoval> findByPlantationId(Long idPlantation);
}
