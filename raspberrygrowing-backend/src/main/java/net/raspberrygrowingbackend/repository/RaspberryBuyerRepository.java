package net.raspberrygrowingbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryBuyer;

@Repository
public interface RaspberryBuyerRepository extends JpaRepository<RaspberryBuyer, Long> {

	List<RaspberryBuyer> findBuyersByRaspberryGrowerId(Long idGrower);
}
