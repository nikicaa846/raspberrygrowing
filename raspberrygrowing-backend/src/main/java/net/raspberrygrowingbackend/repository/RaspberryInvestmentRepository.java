package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryInvestment;

@Repository
public interface RaspberryInvestmentRepository extends JpaRepository<RaspberryInvestment, Long> {

	List<RaspberryInvestment> findInestmentsByDateOfInvestmentAndRaspberryGrowerId(LocalDate date, Long idGrower);
	
	List<RaspberryInvestment> findInvestmetsByRaspberryGrowerId(Long idGrower);
}
