package net.raspberrygrowingbackend.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryWorkerDefrayal;

@Repository
public interface RaspberryWorkerDefrayalRepository extends JpaRepository<RaspberryWorkerDefrayal,  Long> {

	List<RaspberryWorkerDefrayal> findByDateOfDefrayalAndPlantationId(LocalDate date, Long idPlantation);
	
	List<RaspberryWorkerDefrayal> findByPlantationId(Long idPlantation);
	
	List<RaspberryWorkerDefrayal> findDefrayalsByWorkerId( Long id);
}
