package net.raspberrygrowingbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryWorker;

@Repository
public interface RaspberryWorkerRepository extends JpaRepository<RaspberryWorker, Long> {

	List<RaspberryWorker> findWorkersByRaspberryGrowerId(Long idGrower);
}
