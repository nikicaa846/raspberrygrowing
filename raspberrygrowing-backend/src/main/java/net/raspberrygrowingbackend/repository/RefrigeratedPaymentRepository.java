package net.raspberrygrowingbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RefrigeratorPayment;

@Repository
public interface RefrigeratedPaymentRepository extends JpaRepository<RefrigeratorPayment, Long> {

	List<RefrigeratorPayment> findByPlantationId(Long idPlantation);
}
