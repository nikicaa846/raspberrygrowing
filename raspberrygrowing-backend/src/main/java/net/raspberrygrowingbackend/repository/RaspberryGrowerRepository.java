package net.raspberrygrowingbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.raspberrygrowingbackend.models.RaspberryGrower;

@Repository
public interface RaspberryGrowerRepository extends JpaRepository<RaspberryGrower, Long>{

	public RaspberryGrower getById(Long id);
}
